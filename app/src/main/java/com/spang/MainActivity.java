package com.spang;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.spang.models.User;
import com.spang.ui.sign.SignActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends ApplicationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseMessaging.getInstance().subscribeToTopic("spang");
        FirebaseInstanceId.getInstance().getToken();


        //User user = User.fromSharedPreference(this);

        //if(user != null){
            BottomNavigationView navView = findViewById(R.id.nav_view);
            navView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
            // Passing each menu ID as a set of Ids because each
            // menu should be considered as top level destinations.
            AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(R.id.navigation_todaycard, R.id.navigation_mycard, R.id.navigation_store, R.id.navigation_more).build();
            NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
            // NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
            NavigationUI.setupWithNavController(navView, navController);
       // }else{
        /*
            Intent intent= new Intent(this, SignActivity.class);
            startActivity(intent);
            finish();

         */
       // }
    }
}

package com.spang.Communication;

import org.json.JSONArray;

/**
 * Created by jb on 2016-05-30.
 */
public interface Callback {
    String TAG = "Callback";
    public void callbackString(String str);
    public void callbackInteger(int integer);
    public void callbackJSONArray(JSONArray array);

}

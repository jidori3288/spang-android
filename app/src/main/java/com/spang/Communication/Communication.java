package com.spang.Communication;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.spang.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by jhb on 2016-04-01.
 */

public class Communication {
    String TAG = "Communication";
    public void getJSONArrayDatas(Context context, String url, final Callback callback) {
        System.out.println("Send data!");

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            Log.i("리스펀스", "성공");
                            try {
                                JSONObject obj = new JSONObject(response);
                                JSONArray data = (JSONArray) obj.get("data");
                                Callback callbackInstance = callback;
                                callbackInstance.callbackJSONArray(data);

                            } catch(ClassCastException e){

                            }

                        }
                        catch(JSONException E){
                            Log.e("에러발생", E.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = customAuthorizationHeader(context);
                if(headers != null){
                    return headers;
                }
                return super.getHeaders();
            }
        };

        Volley.newRequestQueue(context).add(postRequest);
        //AppController.getInstance().addToRequestQueue(postRequest);
    }


    public void sendOptions(Context context, String url, final HashMap<String, String> inputdata, final Callback callback) {
        System.out.println("Send data!");
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.i("리스펀스", "성공");
                            try {
                                JSONObject obj = new JSONObject(response);
                                String data_type = (String)obj.get("type");
                                if(data_type.equals("string")){
                                    String data = (String) obj.get("data");
                                    Callback callbackInstance = callback;
                                    callbackInstance.callbackString(data);
                                }

                                if(data_type.equals("integer")){
                                    Integer data = (Integer) obj.get("data");
                                    Callback callbackInstance = callback;
                                    callbackInstance.callbackInteger(data);
                                }

                                else {
                                    JSONArray data = (JSONArray) obj.get("data");
                                    Callback callbackInstance = callback;
                                    callbackInstance.callbackJSONArray(data);
                                }

                            } catch(ClassCastException e){
                                Log.i("이런", "클래스캐스트익셉션");
                            }
                        }
                        catch(JSONException E){
                            Callback callbackInstance = callback;
                            callbackInstance.callbackString("null");
                            Log.e("에러발생", E.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                //HashMap<String, String> obj = new HashMap<>(inputdata);
                Map<String, String> obj = new HashMap<String, String>();

                for (Map.Entry<String, String> entry : inputdata.entrySet() )
                {
                    obj.put(entry.getKey(),  entry.getValue());
                }
                return obj;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = customAuthorizationHeader(context);

                if(headers != null){
                    return headers;
                }
                return super.getHeaders();
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0,0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(context).add(postRequest);
        //AppController.getInstance().addToRequestQueue(postRequest);
    }

    private Map<String, String> customAuthorizationHeader(Context context) {
        User user = User.fromSharedPreference(context);

        if(user != null){
            Map<String, String> headers = new HashMap<String, String>();
            headers.put("Custom-Authorization", user.getAccessToken());

            return headers;
        }
        return null;
    }
}

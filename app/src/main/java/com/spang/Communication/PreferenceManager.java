package com.spang.Communication;


import android.content.Context;

import android.content.SharedPreferences;
import android.view.View;
import android.widget.TextView;

import com.spang.R;
import com.spang.constants.Constant;
import com.spang.constants.SharedPreferencesConstant;
import com.spang.ui.productdb.Product;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static java.security.AccessController.getContext;


/**

 * 데이터 저장 및 로드 클래스

 */

public class PreferenceManager {

    String TAG = "PreferenceManager";

    public static final String PREFERENCES_NAME = SharedPreferencesConstant.NAME;


    private static final String DEFAULT_VALUE_STRING = "";

    private static final boolean DEFAULT_VALUE_BOOLEAN = false;

    private static final int DEFAULT_VALUE_INT = -1;

    private static final long DEFAULT_VALUE_LONG = -1L;

    private static final float DEFAULT_VALUE_FLOAT = -1F;



    private static SharedPreferences getPreferences(Context context) {

        return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);

    }



    /**

     * String 값 저장

     * @param context

     * @param key

     * @param value

     */

    public static void setString(Context context, String key, String value) {

        SharedPreferences prefs = getPreferences(context);

        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(key, value);

        editor.commit();

    }


    //상품갯수만 구하는걸로 변경
    public void confirm(Context context, TextView view) {


        if(view != null) {
            view.setText("로딩중...");
        }
        PreferenceManager pm = new PreferenceManager();
        Communication com = new Communication();
        final HashMap<String,String> hm = pm.getAllPref(context);

        com.sendOptions(context, Constant.BASE_URL + "/app/myproductdb", hm, new Callback() {
            @Override
            public void callbackString(String str) {

            }

            @Override
            public void callbackInteger(int integer) {
                if(view != null) {
                    pm.setString(context, "filter_num", String.valueOf(integer));
                    view.setText(String.valueOf(integer) + "개 상품");
                }
            }

            @Override
            public void callbackJSONArray(JSONArray array) {

                if(view != null) {
                    view.setText(array.length() + "개 상품");
                }
                pm.setString(context, "myproductdb:data", array.toString());

            }
        });


    }


    public void confirmAndAction(Context context, TextView view, final Callback callback) {

        if(view != null) {
            view.setText("로딩중...");
        }
        PreferenceManager pm = new PreferenceManager();
        Communication com = new Communication();
        final HashMap<String,String> hm = pm.getAllPref(context);

        com.sendOptions(context, Constant.BASE_URL + "/app/myproductdb", hm, new Callback() {
            @Override
            public void callbackString(String str) {

            }

            @Override
            public void callbackInteger(int integer) {

            }

            @Override
            public void callbackJSONArray(JSONArray array) {

                if(view != null) {
                    view.setText(array.length() + "개 상품");
                }
                pm.setString(context, "myproductdb:data", array.toString());
                Callback callbackInstance = callback;
                callbackInstance.callbackString("success");

            }
        });


    }





    /**

     * boolean 값 저장

     * @param context

     * @param key

     * @param value

     */

    public static void setBoolean(Context context, String key, boolean value) {

        SharedPreferences prefs = getPreferences(context);

        SharedPreferences.Editor editor = prefs.edit();

        editor.putBoolean(key, value);

        editor.commit();

    }



    /**

     * int 값 저장

     * @param context

     * @param key

     * @param value

     */

    public static void setInt(Context context, String key, int value) {

        SharedPreferences prefs = getPreferences(context);

        SharedPreferences.Editor editor = prefs.edit();

        editor.putInt(key, value);

        editor.commit();

    }



    /**

     * long 값 저장

     * @param context

     * @param key

     * @param value

     */

    public static void setLong(Context context, String key, long value) {

        SharedPreferences prefs = getPreferences(context);

        SharedPreferences.Editor editor = prefs.edit();

        editor.putLong(key, value);

        editor.commit();

    }



    /**

     * float 값 저장

     * @param context

     * @param key

     * @param value

     */

    public static void setFloat(Context context, String key, float value) {

        SharedPreferences prefs = getPreferences(context);

        SharedPreferences.Editor editor = prefs.edit();

        editor.putFloat(key, value);

        editor.commit();

    }



    /**

     * String 값 로드

     * @param context

     * @param key

     * @return

     */

    public static String getString(Context context, String key) {

        SharedPreferences prefs = getPreferences(context);

        String value = prefs.getString(key, DEFAULT_VALUE_STRING);

        return value;

    }


    public HashMap<String,String> getAllPref(Context context){
        PreferenceManager pm = new PreferenceManager();
        HashMap<String,String> hm = new HashMap<>();
        hm.put("opt_category", makeString(pm.getSpecialKeys(context,"category")));
        hm.put("opt_season", makeString(pm.getSpecialKeys(context,"season")));
        hm.put("opt_makecountry", makeString(pm.getSpecialKeys(context,"makecountry")));


        hm.put("opt_sort", pm.getString(context,"sort"));
        hm.put("opt_price_start", pm.getString(context,"price:start"));
        hm.put("opt_price_end", pm.getString(context,"price:end"));
        hm.put("opt_revenue_start", pm.getString(context,"revenue:start"));
        hm.put("opt_revenue_end", pm.getString(context,"revenue:end"));
        hm.put("opt_revenue12m_start", pm.getString(context,"revenue12m:start"));
        hm.put("opt_revenue12m_end", pm.getString(context,"revenue12m:end"));
        hm.put("opt_revenue6m_start", pm.getString(context,"revenue6m:start"));
        hm.put("opt_revenue6m_end", pm.getString(context,"revenue6m:end"));
        hm.put("opt_revenue3m_start", pm.getString(context,"revenue3m:start"));
        hm.put("opt_revenue3m_end", pm.getString(context,"revenue3m:end"));
        hm.put("opt_growth_start", pm.getString(context,"growth:start"));
        hm.put("opt_growth_end", pm.getString(context,"growth:end"));
        hm.put("opt_keyword", pm.getString(context,"keyword"));
        return hm;
    }

    public HashMap<String,String> getTodayFilterPref(Context context){
        PreferenceManager pm = new PreferenceManager();
        HashMap<String,String> hm = new HashMap<>();
        hm.put("today-filter-cat", makeString(pm.getSpecialKeys(context,"today-filter-cat")));
        return hm;
    }

    public void removePrefs(Context context){
        PreferenceManager pm = new PreferenceManager();


        //카테고리 계절 제조국 삭제
        SharedPreferences prefs = getPreferences(context);
        Map<String, ?> allEntries = prefs.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            String key = entry.getKey();
            if( (key.split("category").length > 1) || (key.split("season").length > 1) ||  (key.split("makecountry").length > 1)) {
                pm.removeKey(context, key);
            }
        }

        pm.removeKey(context, "sort");
        pm.removeKey(context, "price:start");
        pm.removeKey(context, "price:end");
        pm.removeKey(context, "revenue:start");
        pm.removeKey(context, "revenue:end");
        pm.removeKey(context, "revenue12m:start");
        pm.removeKey(context, "revenue12m:end");
        pm.removeKey(context, "revenue6m:start");
        pm.removeKey(context, "revenue6m:end");
        pm.removeKey(context, "revenue3m:start");
        pm.removeKey(context, "revenue3m:end");
        pm.removeKey(context, "growth:start");
        pm.removeKey(context, "growth:end");
        pm.removeKey(context, "keyword");
        pm.removeKey(context, "filter_num");


    }
    public void removePastReviewHistory(Context context, String todayDate){
        PreferenceManager pm = new PreferenceManager();


        //카테고리 계절 제조국 삭제
        SharedPreferences prefs = getPreferences(context);
        Map<String, ?> allEntries = prefs.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            String key = entry.getKey();
            if( key.split("view_history").length > 1 ) {
                if(key.split(todayDate).length == 1) {
                    pm.removeKey(context, key);
                }
            }
        }

    }

    //기존 toString은 공백을 만드는 버그가 있다
    public String makeString(ArrayList<String> arr){
        String rs = "";

        for(int i =0; i < arr.size(); i++){
            rs += arr.get(i);
            if(i != (arr.size()-1)){
                rs+= ",";
            }
        }
        return rs;
    }






    /**

     * boolean 값 로드

     * @param context

     * @param key

     * @return

     */

    public static boolean getBoolean(Context context, String key) {

        SharedPreferences prefs = getPreferences(context);

        boolean value = prefs.getBoolean(key, DEFAULT_VALUE_BOOLEAN);

        return value;

    }



    /**

     * int 값 로드

     * @param context

     * @param key

     * @return

     */

    public static int getInt(Context context, String key) {

        SharedPreferences prefs = getPreferences(context);

        int value = prefs.getInt(key, DEFAULT_VALUE_INT);

        return value;

    }



    /**

     * long 값 로드

     * @param context

     * @param key

     * @return

     */

    public static long getLong(Context context, String key) {

        SharedPreferences prefs = getPreferences(context);

        long value = prefs.getLong(key, DEFAULT_VALUE_LONG);

        return value;

    }



    /**

     * float 값 로드

     * @param context

     * @param key

     * @return

     */

    public static float getFloat(Context context, String key) {

        SharedPreferences prefs = getPreferences(context);

        float value = prefs.getFloat(key, DEFAULT_VALUE_FLOAT);

        return value;

    }

    public static ArrayList<String> getSpecialKeys(Context context, String filtername) {

        SharedPreferences prefs = getPreferences(context);
       ArrayList<String> keys = new ArrayList<>();
        Map<String, ?> allEntries = prefs.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            String key = entry.getKey();
            if(key.split(filtername).length > 1) {
                keys.add(key.split(filtername +":")[1]);
            }
        }
        return keys;
    }


    public static int getSpecialKeyLeng(Context context, String filtername) {

        SharedPreferences prefs = getPreferences(context);
        int cnt = 0;
        Map<String, ?> allEntries = prefs.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            String key = entry.getKey();
            if(key.split(filtername).length > 1) {
               cnt++;
            }
        }
        return cnt;
    }


    /**

     * 키 값 삭제

     * @param context

     * @param key

     */

    public static void removeKey(Context context, String key) {

        SharedPreferences prefs = getPreferences(context);

        SharedPreferences.Editor edit = prefs.edit();

        edit.remove(key);

        edit.commit();

    }

    public static void removeSpecialKey(Context context, String filtername) {

        SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        Map<String, ?> allEntries = prefs.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            String key = entry.getKey();
            if(key.split(filtername).length > 1) {
                edit.remove(key);
                edit.commit();
            }
        }
    }



    /**

     * 모든 저장 데이터 삭제

     * @param context

     */

    public static void clear(Context context) {

        SharedPreferences prefs = getPreferences(context);

        SharedPreferences.Editor edit = prefs.edit();

        edit.clear();

        edit.commit();

    }






}


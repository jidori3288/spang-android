package com.spang.ui.filter;

import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.spang.ApplicationActivity;
import com.spang.Communication.PreferenceManager;
import com.spang.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;


public class Filter2_category extends ApplicationActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        String TAG = "FILTER2_CATEGORY";
        setContentView(R.layout.activity_filter2_category);


        RelativeLayout backBtn = (RelativeLayout) findViewById(R.id.back_container);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });





        //선택한 카테고리 리스트
        PreferenceManager pm = new PreferenceManager();
        //pm.clear(getApplicationContext());


        LinkedHashMap<String, ArrayList<String>> expandableListDetail = new LinkedHashMap<String, ArrayList<String>>();

        //패션의류
        ArrayList<String> fashion = new ArrayList<String>();

        fashion.add("여성의류");
        fashion.add("남성의류");
        fashion.add("여성 언더웨어/잠옷");
        fashion.add("남성 언더웨어/잠옷");


        //패션잡화
        ArrayList<String> acc = new ArrayList<String>();

        acc.add("여성신발");
        acc.add("남성신발");
        acc.add("여성가방");
        acc.add("남성가방");
        acc.add("여행용 가방/소품");
        acc.add("지갑");
        acc.add("시계");
        acc.add("주얼리");
        acc.add("모자");
        acc.add("기타잡화");


        //화장품/미용
        ArrayList<String> cosmetic = new ArrayList<String>();

        cosmetic.add("스킨케어");
        cosmetic.add("선케어");
        cosmetic.add("베이스메이크업");
        cosmetic.add("색조메이크업");
        cosmetic.add("클렌징");
        cosmetic.add("마스크/팩");
        cosmetic.add("헤어케어");
        cosmetic.add("헤어스타일링");
        cosmetic.add("바디케어");
        cosmetic.add("네일케어");
        cosmetic.add("남성화장품");
        cosmetic.add("향수");
        cosmetic.add("뷰티소품");


        //디지털/가전
        ArrayList<String> digital = new ArrayList<String>();

        digital.add("휴대폰");
        digital.add("노트북");
        digital.add("태블릿PC");
        digital.add("PC");
        digital.add("모니터");
        digital.add("계절가전");
        digital.add("생활가전");
        digital.add("음향가전");
        digital.add("저장장치");
        digital.add("네트워크 장비");
        digital.add("주방가전");
        digital.add("이미용가전");
        digital.add("게임/타이틀");
        digital.add("자동차기기");



        //가구/인테리어
        ArrayList<String> fun = new ArrayList<String>();

        fun.add("침실가구");
        fun.add("주방가구");
        fun.add("수납가구");
        fun.add("침구단품");
        fun.add("거실가구");
        fun.add("커튼/블라인드");
        fun.add("서재/사무용가구");
        fun.add("DIY자재/용품");
        fun.add("침구세트");
        fun.add("홈데코");
        fun.add("카페트/러그");
        fun.add("아동/주니어가구");
        fun.add("아웃도어가구");
        fun.add("인테리어소품");
        fun.add("솜류");
        fun.add("수예");



        //출산/육아
        ArrayList<String> mom = new ArrayList<String>();

        mom.add("분유");
        mom.add("기저귀");
        mom.add("물티슈");
        mom.add("이유식");
        mom.add("아기간식");
        mom.add("수유용품");
        mom.add("유모차");
        mom.add("카시트");
        mom.add("외출용품");
        mom.add("목욕용품");
        mom.add("스킨/바디용품");
        mom.add("위생/건강용품");
        mom.add("구강/청결용품");
        mom.add("소독/살균용품");
        mom.add("유아동잡화");
        mom.add("유아가구");
        mom.add("이유식용품");
        mom.add("임부복");
        mom.add("유아동의류");
        mom.add("유아동언더웨어/잠옷");



        //식품
        ArrayList<String> food = new ArrayList<>();

        food.add("축산");
        food.add("수산");
        food.add("농산물");
        food.add("반찬");
        food.add("김치");
        food.add("음료");
        food.add("과자");
        food.add("아이스크림/빙수");
        food.add("가공식품");
        food.add("냉동/간편조리식품");
        food.add("건강식품");
        food.add("다이어트식품");
        food.add("전통주");
        food.add("쿠킹박스");



        //스포츠/레저
        ArrayList<String> sports = new ArrayList<>();

        sports.add("등산");
        sports.add("캠핑");
        sports.add("골프");
        sports.add("자전거");
        sports.add("스포츠액세서리");
        sports.add("낚시");
        sports.add("수영");
        sports.add("헬스");
        sports.add("스케이트/보드/롤러");
        sports.add("요가/필라테스");
        sports.add("오토바이/스쿠터");




        //생활/건강
        ArrayList<String> live = new ArrayList<>();

        live.add("자동차용품");
        live.add("주방용품");
        live.add("세탁용품");
        live.add("수납/정리용품");
        live.add("문구/사무용품");
        live.add("생활용품");
        live.add("공구");
        live.add("수집품");
        live.add("악기");
        live.add("반려동물");


        //여가/생활편의
        ArrayList<String> les = new ArrayList<String>();
        les.add("공연/티켓");
        les.add("원데이클래스");
        les.add("지류/카드상품권");
        les.add("국내여행");
        les.add("생활편의");
        les.add("자기계발/취미 레슨");
        les.add("예체능레슨");
        les.add("액티비티");
        les.add("해외여행");
        les.add("홈케어서비스");


        //면세점
        ArrayList<String> myun = new ArrayList<>();
        myun.add("화장품");
        myun.add("주얼리");
        myun.add("시계/기프트");
        myun.add("패션/잡화");
        myun.add("전자제품");
        myun.add("향수");


        expandableListDetail.put("패션의류", fashion);
        expandableListDetail.put("패션잡화", acc);
        expandableListDetail.put("화장품/미용", cosmetic);
        expandableListDetail.put("디지털/가전", digital);
        expandableListDetail.put("가구/인테리어", fun);
        expandableListDetail.put("출산/육아", mom);
        expandableListDetail.put("식품", food);
        expandableListDetail.put("스포츠/레저", sports);
        expandableListDetail.put("생활/건강", live);
        expandableListDetail.put("여가/생활편의", les);
       




        ExpandableListView expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        ArrayList<String> expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());


        Filter2_adapter expandableListAdapter = new Filter2_adapter(this, expandableListTitle, expandableListDetail, "category");
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
              //액션
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
               //액션

            }
        });


        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {



            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                String parentNodeText = expandableListTitle.get(groupPosition);
                String childrenNodeText = expandableListDetail.get(expandableListTitle.get(groupPosition)).get(childPosition);

                ExpandableListAdapter adp=expandableListView.getExpandableListAdapter();
                View vvv = adp.getChildView(groupPosition,childPosition,false,v,parent);

                TextView category_text = (TextView) vvv.findViewById(R.id.expandedListItem);
                ImageView category_check = (ImageView) vvv.findViewById(R.id.expandedListItemCheck);

                String category_fullname = parentNodeText +">" + childrenNodeText;
                final String filter_opt = pm.getString(getApplicationContext(),"category:" +category_fullname);
                //선택
                if(filter_opt.equals("")){

                        category_text.setTextColor(getResources().getColor(R.color.naver));
                        category_check.setVisibility(View.VISIBLE);
                        pm.setString(getApplicationContext(), "category:" + category_fullname, "1");
                        pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));


                }

                //선택해제
                if(!filter_opt.equals("")){

                        category_text.setTextColor(getResources().getColor(R.color.text1));
                        category_check.setVisibility(View.INVISIBLE);
                        pm.removeKey(getApplicationContext(), "category:" + category_fullname);
                        pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));


                }



                return false;
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        PreferenceManager pm = new PreferenceManager();
        TextView filternum = findViewById(R.id.filter_num);
        pm.confirm(getApplicationContext(),filternum);
    }


    @Override
    public void finish(){
        super.finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }



}



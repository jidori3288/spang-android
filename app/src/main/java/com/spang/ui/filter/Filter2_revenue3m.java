package com.spang.ui.filter;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.spang.ApplicationActivity;
import com.spang.Communication.PreferenceManager;
import com.spang.R;


public class Filter2_revenue3m extends ApplicationActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String TAG = "Filter2_revenue3m";
        setContentView(R.layout.activity_filter2_revenue3m);


        RelativeLayout backBtn = (RelativeLayout) findViewById(R.id.back_container);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        //선택한 카테고리 리스트
        PreferenceManager pm = new PreferenceManager();
        //pm.clear(getApplicationContext());


        //선체크
        String price_start = pm.getString(getApplicationContext(), "revenue3m:start");
        String price_end = pm.getString(getApplicationContext(), "revenue3m:end");


        EditText rangeStart = findViewById(R.id.editText1);
        EditText rangeEnd = findViewById(R.id.editText2);

        RelativeLayout p1 = findViewById(R.id.price1);
        RelativeLayout p2 = findViewById(R.id.price2);
        RelativeLayout p3 = findViewById(R.id.price3);
        RelativeLayout p4 = findViewById(R.id.price4);
        RelativeLayout p5 = findViewById(R.id.price5);
        RelativeLayout p6 = findViewById(R.id.price6);

        TextView price_text0 = (TextView)findViewById(R.id.text_price0);
        ImageView price_check0 = (ImageView) findViewById(R.id.expandedListItemCheck0);

        TextView price_text1 = (TextView)findViewById(R.id.text_price1);
        ImageView price_check1 = (ImageView) findViewById(R.id.expandedListItemCheck1);
        TextView price_text2 = (TextView)findViewById(R.id.text_price2);
        ImageView price_check2 = (ImageView) findViewById(R.id.expandedListItemCheck2);
        TextView price_text3 = (TextView)findViewById(R.id.text_price3);
        ImageView price_check3 = (ImageView) findViewById(R.id.expandedListItemCheck3);
        TextView price_text4 = (TextView)findViewById(R.id.text_price4);
        ImageView price_check4 = (ImageView) findViewById(R.id.expandedListItemCheck4);
        TextView price_text5 = (TextView)findViewById(R.id.text_price5);
        ImageView price_check5 = (ImageView) findViewById(R.id.expandedListItemCheck5);
        TextView price_text6 = (TextView)findViewById(R.id.text_price6);
        ImageView price_check6 = (ImageView) findViewById(R.id.expandedListItemCheck6);

        TextView[] tvList = { price_text1, price_text2, price_text3, price_text4, price_text5, price_text6,price_text0};
        ImageView[] ivList = {price_check1, price_check2, price_check3, price_check4, price_check5, price_check6, price_check0};

        if(price_start.equals("0") && price_end.equals("50000")){
            price_text1.setTextColor(getResources().getColor(R.color.naver));
            price_check1.setVisibility(View.VISIBLE);
            rangeStart.setText("0");
            rangeEnd.setText("50000");
        }
        if(price_start.equals("50000") && price_end.equals("100000")){
            price_text2.setTextColor(getResources().getColor(R.color.naver));
            price_check2.setVisibility(View.VISIBLE);
            rangeStart.setText("50000");
            rangeEnd.setText("100000");
        }
        if(price_start.equals("100000") && price_end.equals("500000")){
            price_text3.setTextColor(getResources().getColor(R.color.naver));
            price_check3.setVisibility(View.VISIBLE);
            rangeStart.setText("100000");
            rangeEnd.setText("500000");
        }
        if(price_start.equals("500000") && price_end.equals("1000000")){
            price_text4.setTextColor(getResources().getColor(R.color.naver));
            price_check4.setVisibility(View.VISIBLE);
            rangeStart.setText("500000");
            rangeEnd.setText("1000000");
        }
        if(price_start.equals("1000000") && price_end.equals("2000000")){
            price_text5.setTextColor(getResources().getColor(R.color.naver));
            price_check5.setVisibility(View.VISIBLE);
            rangeStart.setText("1000000");
            rangeEnd.setText("2000000");
        }
        if(price_start.equals("2000000") && price_end.equals("")){
            price_text6.setTextColor(getResources().getColor(R.color.naver));
            price_check6.setVisibility(View.VISIBLE);
            rangeStart.setText("2000000");
            rangeEnd.setText("");
        }


        rangeStart.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                for(int s=0; s < tvList.length; s++) {
                    tvList[s].setTextColor(getResources().getColor(R.color.text1));
                    ivList[s].setVisibility(View.INVISIBLE);
                }
                rangeStart.setText("");
                price_text0.setTextColor(getResources().getColor(R.color.naver));
                price_check0.setVisibility(View.VISIBLE);
                pm.setString(getApplicationContext(),"revenue3m:start", rangeStart.getText().toString() );
                pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                return false;
            }
        });

        rangeEnd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                for(int s=0; s < tvList.length; s++) {
                    tvList[s].setTextColor(getResources().getColor(R.color.text1));
                    ivList[s].setVisibility(View.INVISIBLE);
                }
                rangeEnd.setText("");
                price_text0.setTextColor(getResources().getColor(R.color.naver));
                price_check0.setVisibility(View.VISIBLE);
                pm.setString(getApplicationContext(),"revenue3m:end", rangeEnd.getText().toString());
                pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                return false;
            }
        });

        rangeStart.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String num = rangeStart.getText().toString().replaceAll(",","");
                pm.setString(getApplicationContext(),"revenue3m:start",num );
                pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        rangeEnd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                String num = rangeEnd.getText().toString().replaceAll(",","");
                pm.setString(getApplicationContext(),"revenue3m:end", num);
                pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        p1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                for(int s=0; s < tvList.length; s++) {
                    tvList[s].setTextColor(getResources().getColor(R.color.text1));
                    ivList[s].setVisibility(View.INVISIBLE);
                }

                final String price_start = pm.getString(getApplicationContext(),"revenue3m:start" );
                final String price_end = pm.getString(getApplicationContext(),"revenue3m:end");
                //선택
                if(!(price_start.equals("0") && price_end.equals("50000"))){
                    price_text1.setTextColor(getResources().getColor(R.color.naver));
                    price_check1.setVisibility(View.VISIBLE);
                    pm.setString(getApplicationContext(), "revenue3m:start", "0");
                    pm.setString(getApplicationContext(), "revenue3m:end","50000" );
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                    rangeStart.setText("0");
                    rangeEnd.setText("50,000");
                }

                //선택해제
                else{
                    price_text1.setTextColor(getResources().getColor(R.color.text1));
                    price_check1.setVisibility(View.INVISIBLE);
                    pm.removeKey(getApplicationContext(), "revenue3m:start");
                    pm.removeKey(getApplicationContext(), "revenue3m:end");
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                    rangeStart.setText("");
                    rangeEnd.setText("");

                }


            }
        });
        p2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                for(int s=0; s < tvList.length; s++) {
                    tvList[s].setTextColor(getResources().getColor(R.color.text1));
                    ivList[s].setVisibility(View.INVISIBLE);
                }

                final String price_start = pm.getString(getApplicationContext(),"revenue3m:start" );
                final String price_end = pm.getString(getApplicationContext(),"revenue3m:end");
                //선택
                if(!(price_start.equals("50000") && price_end.equals("100000"))){
                    price_text2.setTextColor(getResources().getColor(R.color.naver));
                    price_check2.setVisibility(View.VISIBLE);
                    pm.setString(getApplicationContext(), "revenue3m:start", "50000" );
                    pm.setString(getApplicationContext(), "revenue3m:end","100000" );
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                    rangeStart.setText("50,000");
                    rangeEnd.setText("100,000");
                }

                //선택해제
                else{
                    price_text2.setTextColor(getResources().getColor(R.color.text1));
                    price_check2.setVisibility(View.INVISIBLE);
                    pm.removeKey(getApplicationContext(), "revenue3m:start");
                    pm.removeKey(getApplicationContext(), "revenue3m:end");
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                    rangeStart.setText("");
                    rangeEnd.setText("");

                }
            }
        });
        p3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(int s=0; s < tvList.length; s++) {
                    tvList[s].setTextColor(getResources().getColor(R.color.text1));
                    ivList[s].setVisibility(View.INVISIBLE);
                }

                final String price_start = pm.getString(getApplicationContext(),"revenue3m:start" );
                final String price_end = pm.getString(getApplicationContext(),"revenue3m:end");
                //선택
                if(!(price_start.equals("100000") && price_end.equals("500000"))){
                    price_text3.setTextColor(getResources().getColor(R.color.naver));
                    price_check3.setVisibility(View.VISIBLE);
                    pm.setString(getApplicationContext(), "revenue3m:start", "100000");
                    pm.setString(getApplicationContext(), "revenue3m:end","500000" );
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                    rangeStart.setText("100,000");
                    rangeEnd.setText("500,000");
                }

                //선택해제
                else{
                    price_text3.setTextColor(getResources().getColor(R.color.text1));
                    price_check3.setVisibility(View.INVISIBLE);
                    pm.removeKey(getApplicationContext(), "revenue3m:start");;
                    pm.removeKey(getApplicationContext(), "revenue3m:end");
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                    rangeStart.setText("");
                    rangeEnd.setText("");

                }
            }
        });
        p4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(int s=0; s < tvList.length; s++) {
                    tvList[s].setTextColor(getResources().getColor(R.color.text1));
                    ivList[s].setVisibility(View.INVISIBLE);
                }

                final String price_start = pm.getString(getApplicationContext(),"revenue3m:start" );
                final String price_end = pm.getString(getApplicationContext(),"revenue3m:end");
                //선택
                if(!(price_start.equals("500000") && price_end.equals("1000000"))){
                    price_text4.setTextColor(getResources().getColor(R.color.naver));
                    price_check4.setVisibility(View.VISIBLE);
                    pm.setString(getApplicationContext(), "revenue3m:start", "500000");
                    pm.setString(getApplicationContext(), "revenue3m:end","1000000" );
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                    rangeStart.setText("500,000");
                    rangeEnd.setText("1,000,000");
                }

                //선택해제
                else{
                    price_text4.setTextColor(getResources().getColor(R.color.text1));
                    price_check4.setVisibility(View.INVISIBLE);
                    pm.removeKey(getApplicationContext(), "revenue3m:start");
                    pm.removeKey(getApplicationContext(), "revenue3m:end");
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                    rangeStart.setText("");
                    rangeEnd.setText("");

                }
            }
        });
        p5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(int s=0; s < tvList.length; s++) {
                    tvList[s].setTextColor(getResources().getColor(R.color.text1));
                    ivList[s].setVisibility(View.INVISIBLE);
                }

                final String price_start = pm.getString(getApplicationContext(),"revenue3m:start" );
                final String price_end = pm.getString(getApplicationContext(),"revenue3m:end");
                //선택
                if(!(price_start.equals("1000000") && price_end.equals("2000000"))){
                    price_text5.setTextColor(getResources().getColor(R.color.naver));
                    price_check5.setVisibility(View.VISIBLE);
                    pm.setString(getApplicationContext(), "revenue3m:start", "1000000");
                    pm.setString(getApplicationContext(), "revenue3m:end","2000000");
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                    rangeStart.setText("1,000,000");
                    rangeEnd.setText("2,000,000");
                }

                //선택해제
                else{
                    price_text5.setTextColor(getResources().getColor(R.color.text1));
                    price_check5.setVisibility(View.INVISIBLE);
                    pm.removeKey(getApplicationContext(), "revenue3m:start");
                    pm.removeKey(getApplicationContext(), "revenue3m:end");
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                    rangeStart.setText("");
                    rangeEnd.setText("");

                }
            }
        });
        p6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(int s=0; s < tvList.length; s++) {
                    tvList[s].setTextColor(getResources().getColor(R.color.text1));
                    ivList[s].setVisibility(View.INVISIBLE);
                }

                final String price_start = pm.getString(getApplicationContext(),"revenue3m:start" );
                final String price_end = pm.getString(getApplicationContext(),"revenue3m:end");
                //선택
                if(!(price_start.equals("2000000") && price_end.equals(""))){
                    price_text6.setTextColor(getResources().getColor(R.color.naver));
                    price_check6.setVisibility(View.VISIBLE);
                    pm.setString(getApplicationContext(), "revenue3m:start", "2000000");
                    pm.setString(getApplicationContext(), "revenue3m:end","");
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                    rangeStart.setText("2,000,000");
                    rangeEnd.setText("");
                }

                //선택해제
                else{
                    price_text6.setTextColor(getResources().getColor(R.color.text1));
                    price_check6.setVisibility(View.INVISIBLE);
                    pm.removeKey(getApplicationContext(), "revenue3m:start");
                    pm.removeKey(getApplicationContext(), "revenue3m:end");
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                    rangeStart.setText("");
                    rangeEnd.setText("");

                }
            }
        });



    }




    @Override
    public void finish(){
        super.finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    @Override
    protected void onResume() {
        super.onResume();
        PreferenceManager pm = new PreferenceManager();
        TextView filternum = findViewById(R.id.filter_num);
        pm.confirm(getApplicationContext(),filternum);
    }


}



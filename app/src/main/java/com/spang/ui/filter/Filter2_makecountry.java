package com.spang.ui.filter;

import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.spang.ApplicationActivity;
import com.spang.Communication.PreferenceManager;
import com.spang.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;


public class Filter2_makecountry extends ApplicationActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        String TAG = "Filter2_makecountry";
        setContentView(R.layout.activity_filter2_makecountry);


        RelativeLayout backBtn = (RelativeLayout) findViewById(R.id.back_container);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });





        //선택한 카테고리 리스트
        PreferenceManager pm = new PreferenceManager();
        //pm.clear(getApplicationContext());


        LinkedHashMap<String, ArrayList<String>> expandableListDetail = new LinkedHashMap<String, ArrayList<String>>();

        //국산
        ArrayList<String> country1 = new ArrayList<String>();

        country1.add("국산");



        //아시아
        ArrayList<String> country2 = new ArrayList<String>();

        country2.add("중국");
        country2.add("일본");
        country2.add("베트남");
        country2.add("인도네시아");
        country2.add("방글라데시");
        country2.add("스리랑카");
        country2.add("싱가포르");
        country2.add("필리핀");
        country2.add("태국");
        country2.add("홍콩");
        country2.add("대만");
        country2.add("몽골");
        country2.add("그루지야");
        country2.add("네팔");
        country2.add("동티모르");
        country2.add("라오스");
        country2.add("레바논");
        country2.add("리비아");
        country2.add("마카오");
        country2.add("말레이시아");
        country2.add("몰디브");
        country2.add("미얀마");
        country2.add("바레인");
        country2.add("부탄");
        country2.add("북한");
        country2.add("브루나이");
        country2.add("사우디아라비아");
        country2.add("시리아");
        country2.add("아랍에미리트");
        country2.add("아르메니아");
        country2.add("아프가니스탄");
        country2.add("예멘");
        country2.add("오만");
        country2.add("요르단");
        country2.add("우즈베키스탄");
        country2.add("이라크");
        country2.add("이란");
        country2.add("인도");
        country2.add("인도양식민지");
        country2.add("카자흐스탄");
        country2.add("카타르");
        country2.add("캄보디아");
        country2.add("쿠웨이트");
        country2.add("키르기스스탄");
        country2.add("타지키스탄");
        country2.add("투르크메니스탄");
        country2.add("티베트");
        country2.add("파키스탄");


        //유럽
        ArrayList<String> country3 = new ArrayList<String>();
        country3.add("독일");
        country3.add("영국");
        country3.add("프랑스");
        country3.add("이탈리아");
        country3.add("스위스");
        country3.add("스페인");
        country3.add("그리스");
        country3.add("노르웨이");
        country3.add("네덜란드");
        country3.add("덴마크");
        country3.add("스웨덴");
        country3.add("오스트리아");
        country3.add("체코");
        country3.add("터키");
        country3.add("핀란드");
        country3.add("그린란드");
        country3.add("라트비아");
        country3.add("러시아연방");
        country3.add("루마니아");
        country3.add("룩셈부르크");
        country3.add("리투아니아");
        country3.add("리히텐슈타인");
        country3.add("마케도니아");
        country3.add("말타");
        country3.add("모나코");
        country3.add("몰도바공화국");
        country3.add("몰타");
        country3.add("바티칸");
        country3.add("벨기에");
        country3.add("벨라루스");
        country3.add("벨로루시");
        country3.add("보스니아-헤르체고비나");
        country3.add("불가리아");
        country3.add("사이프러스");
        country3.add("세르비아");
        country3.add("슬로바키아");
        country3.add("슬로베니아");
        country3.add("아이슬란드");
        country3.add("아일랜드공화국");
        country3.add("아제르바이잔");
        country3.add("안도라");
        country3.add("알메니아");
        country3.add("알바니아");
        country3.add("에스토니아");
        country3.add("우크라이나");
        country3.add("유고");
        country3.add("조지아");
        country3.add("크로아티아");
        country3.add("페로스제도");
        country3.add("포르투갈");
        country3.add("폴란드");
        country3.add("헝가리");


        //북아메리카(북미)
        ArrayList<String> country4 = new ArrayList<String>();
        country4.add("미국");
        country4.add("캐나다");

        //라틴아메리카(남미)
        ArrayList<String> country5 = new ArrayList<String>();
        country5.add("멕시코");
        country5.add("브라질");
        country5.add("아르헨티나");
        country5.add("칠레");
        country5.add("과테말라");
        country5.add("콜롬비아");
        country5.add("우루과이");
        country5.add("파라과이");
        country5.add("페루");



        //오세아니아
        ArrayList<String> country6 = new ArrayList<String>();
        country6.add("뉴질랜드");
        country6.add("호주");


        expandableListDetail.put("국산", country1);
        expandableListDetail.put("아시아", country2);
        expandableListDetail.put("유럽", country3);
        expandableListDetail.put("북아메리카(북미)", country4);
        expandableListDetail.put("라틴아메리카(남미)", country5);
        expandableListDetail.put("오세아니아", country6);




        ExpandableListView expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        ArrayList<String> expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());


        Filter2_adapter expandableListAdapter = new Filter2_adapter(this, expandableListTitle, expandableListDetail, "makecountry");
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
              //액션
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
               //액션

            }
        });


        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {



            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                String parentNodeText = expandableListTitle.get(groupPosition);
                String childrenNodeText = expandableListDetail.get(expandableListTitle.get(groupPosition)).get(childPosition);

                ExpandableListAdapter adp=expandableListView.getExpandableListAdapter();
                View vvv = adp.getChildView(groupPosition,childPosition,false,v,parent);

                TextView category_text = (TextView) vvv.findViewById(R.id.expandedListItem);
                ImageView category_check = (ImageView) vvv.findViewById(R.id.expandedListItemCheck);

                String category_fullname = parentNodeText +">" + childrenNodeText;
                final String filter_opt = pm.getString(getApplicationContext(),"makecountry:" +category_fullname);
                //선택
                if(filter_opt.equals("")){

                        category_text.setTextColor(getResources().getColor(R.color.naver));
                        category_check.setVisibility(View.VISIBLE);
                        pm.setString(getApplicationContext(), "makecountry:" + category_fullname, "1" );
                        pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));

                }

                //선택해제
                if(!filter_opt.equals("")){

                        category_text.setTextColor(getResources().getColor(R.color.text1));
                        category_check.setVisibility(View.INVISIBLE);
                        pm.removeKey(getApplicationContext(), "makecountry:" + category_fullname);
                        pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));


                }



                return false;
            }
        });
    }




    @Override
    public void finish(){
        super.finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    @Override
    protected void onResume() {
        super.onResume();
        PreferenceManager pm = new PreferenceManager();
        TextView filternum = findViewById(R.id.filter_num);
        pm.confirm(getApplicationContext(),filternum);
    }


}



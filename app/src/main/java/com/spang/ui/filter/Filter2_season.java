package com.spang.ui.filter;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.spang.ApplicationActivity;
import com.spang.Communication.PreferenceManager;
import com.spang.R;


public class Filter2_season extends ApplicationActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String TAG = "Filter2_season";
        setContentView(R.layout.activity_filter2_season);


        RelativeLayout backBtn = (RelativeLayout) findViewById(R.id.back_container);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        //선택한 카테고리 리스트
        PreferenceManager pm = new PreferenceManager();




        RelativeLayout p1 = findViewById(R.id.season4);
        RelativeLayout p2 = findViewById(R.id.ss);
        RelativeLayout p3 = findViewById(R.id.fw);

        TextView price_text1 = (TextView)findViewById(R.id.text_4);
        ImageView price_check1 = (ImageView) findViewById(R.id.expandedListItemCheck1);
        TextView price_text2 = (TextView)findViewById(R.id.text_ss);
        ImageView price_check2 = (ImageView) findViewById(R.id.expandedListItemCheck2);
        TextView price_text3 = (TextView)findViewById(R.id.text_fw);
        ImageView price_check3 = (ImageView) findViewById(R.id.expandedListItemCheck3);

        final String season4 = pm.getString(getApplicationContext(),"season:4" );
        final String seasonss = pm.getString(getApplicationContext(),"season:ss" );
        final String seasonfw = pm.getString(getApplicationContext(),"season:fw" );
        if(!season4.equals("")){
            price_text1.setTextColor(getResources().getColor(R.color.naver));
            price_check1.setVisibility(View.VISIBLE);
        }
        if(!seasonss.equals("")){
            price_text2.setTextColor(getResources().getColor(R.color.naver));
            price_check2.setVisibility(View.VISIBLE);
        }
        if(!seasonfw.equals("")){
            price_text3.setTextColor(getResources().getColor(R.color.naver));
            price_check3.setVisibility(View.VISIBLE);
        }


        p1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String season = pm.getString(getApplicationContext(),"season:4" );
                //선택
                if(season.equals("")){
                    price_text1.setTextColor(getResources().getColor(R.color.naver));
                    price_check1.setVisibility(View.VISIBLE);
                    pm.setString(getApplicationContext(), "season:4", "1" );
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                }

                //선택해제
                else{
                    price_text1.setTextColor(getResources().getColor(R.color.text1));
                    price_check1.setVisibility(View.INVISIBLE);
                    pm.removeKey(getApplicationContext(), "season:4");
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                }


            }
        });
        p2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String season = pm.getString(getApplicationContext(),"season:ss" );
                //선택
                if(season.equals("")){
                    price_text2.setTextColor(getResources().getColor(R.color.naver));
                    price_check2.setVisibility(View.VISIBLE);
                    pm.setString(getApplicationContext(), "season:ss", "1" );
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                }

                //선택해제
                else{
                    price_text2.setTextColor(getResources().getColor(R.color.text1));
                    price_check2.setVisibility(View.INVISIBLE);
                    pm.removeKey(getApplicationContext(), "season:ss");
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                }
            }
        });


        p3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final String season = pm.getString(getApplicationContext(),"season:fw" );
                //선택
                if(season.equals("")){
                    price_text3.setTextColor(getResources().getColor(R.color.naver));
                    price_check3.setVisibility(View.VISIBLE);
                    pm.setString(getApplicationContext(), "season:fw", "1");
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                }

                //선택해제
                else{
                    price_text3.setTextColor(getResources().getColor(R.color.text1));
                    price_check3.setVisibility(View.INVISIBLE);
                    pm.removeKey(getApplicationContext(), "season:fw");
                    pm.confirm(getApplicationContext(),findViewById(R.id.filter_num));
                }


            }
        });


    }




    @Override
    public void finish(){
        super.finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    @Override
    protected void onResume() {
        super.onResume();
        PreferenceManager pm = new PreferenceManager();
        TextView filternum = findViewById(R.id.filter_num);
        pm.confirm(getApplicationContext(),filternum);
    }


}



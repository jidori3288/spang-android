package com.spang.ui.filter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.spang.ApplicationActivity;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.Communication.PreferenceManager;
import com.spang.R;
import com.spang.ui.cardbuy.CardBuyActivity;
import com.spang.ui.productdb.ProductdbActivity;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

public class Filter1 extends ApplicationActivity {

    PreferenceManager pm = new PreferenceManager();
    Communication com = new Communication();

    @Override
    protected void onCreate(Bundle savedInstanceState){
        String TAG = "FILTER1";

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter1);


        //카테고리 클릭시
        RelativeLayout category_layout = findViewById(R.id.filter_category);
        category_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Filter2_category.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        //가격
        RelativeLayout price_layout = findViewById(R.id.filter_price);
        price_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Filter2_price.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        //계절
        RelativeLayout season_layout = findViewById(R.id.filter_season);
        season_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Filter2_season.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        //누적매출액
        RelativeLayout revenue_layout = findViewById(R.id.filter_revenue);
        revenue_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Filter2_revenue.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        //12매출액
        RelativeLayout revenue12m_layout = findViewById(R.id.filter_revenue12m);
        revenue12m_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Filter2_revenue12m.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        //6매출액
        RelativeLayout revenue6m_layout = findViewById(R.id.filter_revenue6m);
        revenue6m_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Filter2_revenue6m.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        //3매출액
        RelativeLayout revenue3m_layout = findViewById(R.id.filter_revenue3m);
        revenue3m_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Filter2_revenue3m.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        //성장률
        RelativeLayout growth_layout = findViewById(R.id.filter_growth);
        growth_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Filter2_growth.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        //성장률
        RelativeLayout makecountry_layout = findViewById(R.id.filter_makecountry);
        makecountry_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Filter2_makecountry.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });


        ImageView filter_out  =findViewById(R.id.filter_out);
        filter_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Button adjust_btn = findViewById(R.id.adjust);
        adjust_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //필터전송
                //Intent intent = new Intent(getApplicationContext(), ProductdbActivity.class);
                //intent.putExtra("fromFilter","fromFilter");
                //startActivity(intent);
                pm.setString(getApplicationContext(),"myproductdb:adjust", "1");
                finish();
            }
        });



        Button initialize_btn = findViewById(R.id.initialize);
        initialize_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pm.removeSpecialKey(getApplicationContext(), "category");
                pm.removeSpecialKey(getApplicationContext(), "price");
                pm.removeSpecialKey(getApplicationContext(), "season");
                pm.removeSpecialKey(getApplicationContext(), "revenue");
                pm.removeSpecialKey(getApplicationContext(), "revenue12m");
                pm.removeSpecialKey(getApplicationContext(), "revenue6m");
                pm.removeSpecialKey(getApplicationContext(), "revenue3m");
                pm.removeSpecialKey(getApplicationContext(), "growth");
                pm.removeSpecialKey(getApplicationContext(), "makecountry");

                lengNumDisplay("category", R.id.f1_category_num);
                lengNumDisplay("price:star", R.id.f1_price_num);
                lengNumDisplay("season", R.id.f1_season_num);
                lengNumDisplay("revenue:star", R.id.f1_revenue_num);
                lengNumDisplay("revenue12m:star", R.id.f1_revenue12m_num);
                lengNumDisplay("revenue6m:star", R.id.f1_revenue6m_num);
                lengNumDisplay("revenue3m:star", R.id.f1_revenue3m_num);
                lengNumDisplay("growth:star", R.id.f1_growth_num);
                lengNumDisplay("makecountry", R.id.f1_makecountry_num);

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        lengNumDisplay("category", R.id.f1_category_num);
        lengNumDisplay("price:star", R.id.f1_price_num);
        lengNumDisplay("season", R.id.f1_season_num);
        lengNumDisplay("revenue:star", R.id.f1_revenue_num);
        lengNumDisplay("revenue12m:star", R.id.f1_revenue12m_num);
        lengNumDisplay("revenue6m:star", R.id.f1_revenue6m_num);
        lengNumDisplay("revenue3m:star", R.id.f1_revenue3m_num);
        lengNumDisplay("growth:star", R.id.f1_growth_num);
        lengNumDisplay("makecountry", R.id.f1_makecountry_num);


        TextView filternum = findViewById(R.id.filter_num);
        pm.confirm(getApplicationContext(),filternum);
    }

    @Override
    public void finish(){
        super.finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }


    public void lengNumDisplay(String filtername, int id) {
        int selectedCategory = pm.getSpecialKeyLeng(getApplicationContext(),filtername);
        TextView catNum = findViewById(id);
        if(selectedCategory == 0){
            catNum.setVisibility(View.INVISIBLE);
        }else {
            catNum.setText(String.valueOf(selectedCategory));
            catNum.setVisibility(View.VISIBLE);
        }
    }




}

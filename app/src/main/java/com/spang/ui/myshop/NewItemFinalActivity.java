package com.spang.ui.myshop;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.spang.ApplicationActivity;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.R;
import com.spang.constants.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class NewItemFinalActivity extends ApplicationActivity {

    private Timer timer = new Timer();
    private final long DELAY = 500; // in ms
    EditText editText;

    int stop_repeat = 0;
    int data_counter = 0;
    int card_leng = 10;
    int visibleItem_lv = 0;
    int getVisibleItem_gv = 0;
    ArrayList productList = new ArrayList<>();
    Communication com = new Communication();
    RelativeLayout loading_layout;
    TextView error_msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String TAG = "hell";
        setContentView(R.layout.activity_newitem_final);



        //초기화면

        TextView textview = findViewById(R.id.newitem_title);
        ImageView imageView = findViewById(R.id.newitem_image);

        String title = getIntent().getStringExtra("title");
        String image = getIntent().getStringExtra("image");

        textview.setText(title);
        Glide.with(getApplicationContext()).load(image).diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH).into(imageView);
        slideUp(imageView);


        //네 맞습니다
        TextView yes_text = findViewById(R.id.yes);
        yes_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView wow = findViewById(R.id.wow);
                slideUp(wow);
                TextView wowow = findViewById(R.id.wowow);
                slideUp(wowow);

                RelativeLayout main_layout = findViewById(R.id.main_layout);
                RelativeLayout final_layout = findViewById(R.id.final_layout);
                main_layout.setVisibility(GONE);
                final_layout.setVisibility(VISIBLE);


                Handler handler = new Handler() {
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        finish();
                    }
                };
                handler.sendEmptyMessageDelayed(0, 2000);
            }
        });


        TextView no_text = findViewById(R.id.no);
        no_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });




    }


    public void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight() ,  // fromYDelta
                0);                // toYDelta
        animate.setDuration(3000);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }


}

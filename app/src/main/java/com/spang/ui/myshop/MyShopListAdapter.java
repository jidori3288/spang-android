package com.spang.ui.myshop;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.spang.Communication.PreferenceManager;
import com.spang.R;
import com.spang.ui.analysis.CardDetailActivity;
import com.spang.ui.productdb.Product;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MyShopListAdapter extends ArrayAdapter<JSONObject> {
    String TAG = "hell";
    String place = "";
    ArrayList  listObj = null;
    String extra_info = "";
    DecimalFormat formatter = new DecimalFormat("###,###");
    PreferenceManager pm = new PreferenceManager();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    Calendar c1 = Calendar.getInstance();

    public MyShopListAdapter(Context context, int resource, ArrayList object, String listPlace, String extraInfo) {
        super(context, resource, object);

        this.place = listPlace;
        this.listObj = object;
        this.extra_info = extraInfo;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        //오늘날짜
        String strToday = sdf.format(c1.getTime());

        //오늘 날짜가 아닌 읽은 리뷰 프리퍼런스는 전부 삭제한다
        pm.removePastReviewHistory(getContext(),strToday);


        //매출부분
        if(place.equals("revenue")) {
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_item_shop_revenue, null);

            ImageView item_thumbnail = (ImageView) v.findViewById(R.id.item_thumbnail);
            TextView item_name = (TextView) v.findViewById(R.id.item_name);
            //item_name.setSelected(true);
            TextView item_revenue = (TextView) v.findViewById(R.id.item_revenue);


            try {
                JSONObject obj = (JSONObject)listObj.get(position);
                Glide.with(getContext()).load(obj.get("image_url")).diskCacheStrategy(DiskCacheStrategy.ALL).into(item_thumbnail);
                item_name.setText((String)obj.get("타이틀"));
                item_revenue.setText((String)obj.get(extra_info));

                item_thumbnail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        /*
                        Intent itn = new Intent(getContext(), CardDetailActivity.class);
                        itn.putExtra("data", ((String) obj.toString()));
                        getContext().startActivity(itn);

                         */
                        try {
                            pm.setString(getContext(), "large-data", obj.toString());
                            Intent intent = new Intent(getContext(), CardDetailActivity.class);
                            getContext().startActivity(intent);
                        }catch(Exception ex2) {
                            //
                        }
                    }
                });

                item_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        /*
                        Intent itn = new Intent(getContext(), CardDetailActivity.class);
                        itn.putExtra("data", ((String) obj.toString()));
                        getContext().startActivity(itn);

                         */
                        try {
                            pm.setString(getContext(), "large-data", obj.toString());
                            Intent intent = new Intent(getContext(), CardDetailActivity.class);
                            getContext().startActivity(intent);
                        }catch(Exception ex2) {
                            //
                        }
                    }
                });
            }catch(Exception ex){
                    ex.printStackTrace();
            }
        }

        //리뷰부분
        if(place.equals("review")){


            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_item_shop_review, null);

            ImageView item_thumbnail = (ImageView) v.findViewById(R.id.item_thumbnail);
            TextView item_name = (TextView) v.findViewById(R.id.item_name);
            TextView item_review = (TextView) v.findViewById(R.id.item_review);
            item_name.setSelected(true);

            try {
                JSONObject obj = (JSONObject) listObj.get(position);
                Glide.with(getContext()).load(obj.get("image_url")).diskCacheStrategy(DiskCacheStrategy.ALL).into(item_thumbnail);
                item_name.setText((String)obj.get("타이틀"));
                String todayReview = (String)obj.get("오늘리뷰갯수");

                try {
                    item_review.setText(todayReview);
                    if(!todayReview.equals("집계중")) {
                        if(Integer.parseInt(todayReview) > 0){
                            item_review.setTypeface(null, Typeface.BOLD);
                            item_review.setTextColor(getContext().getResources().getColor(R.color.red));
                        }
                        String already_read = pm.getString(getContext(), "review_history_"+ strToday + "_" + item_name.getText() +"_"+ item_review.getText());
                        if(already_read.equals("1")) {
                            item_review.setTextColor(getContext().getResources().getColor(R.color.black_normal));
                        }

                    }
                }catch(Exception ex) {
                    //집계중
                }

                item_thumbnail.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        /*
                        Intent itn = new Intent(getContext(), CardDetailActivity.class);
                        itn.putExtra("data", ((String) obj.toString()));
                        getContext().startActivity(itn);

                         */
                        try {
                            pm.setString(getContext(), "large-data", obj.toString());
                            Intent intent = new Intent(getContext(), CardDetailActivity.class);
                            getContext().startActivity(intent);
                        }catch(Exception ex2) {
                            //
                        }
                    }
                });

                item_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        /*
                        Intent itn = new Intent(getContext(), CardDetailActivity.class);
                        itn.putExtra("data", ((String) obj.toString()));
                        getContext().startActivity(itn);

                         */
                        try {
                            pm.setString(getContext(), "large-data", obj.toString());
                            Intent intent = new Intent(getContext(), CardDetailActivity.class);
                            getContext().startActivity(intent);
                        }catch(Exception ex2) {
                            //
                        }
                    }
                });



                item_review.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {

                            item_review.setTextColor(getContext().getResources().getColor(R.color.black_normal));

                            pm.setString(getContext(), "review_history_"+ strToday + "_" + item_name.getText() +"_"+ item_review.getText(), "1");

                            Intent itn = new Intent(getContext(), ReviewActivity.class);
                            itn.putExtra("data", ((JSONArray) obj.get("오늘리뷰")).toString());
                            getContext().startActivity(itn);





                        }catch(Exception Ex){
                            Ex.printStackTrace();
                        }

                    }
                });
            }catch(Exception ex){

            }
        }


        return v;
    }
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);

        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + 50;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }





}

package com.spang.ui.myshop;


import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;


import com.airbnb.lottie.LottieAnimationView;
import com.spang.ApplicationFragment;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.R;
import com.spang.constants.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import static android.view.View.GONE;
import static com.spang.ui.myshop.MyShopListAdapter.setListViewHeightBasedOnChildren;

public  class MyShopReviewFragment extends ApplicationFragment {
    String TAG = "hell";
    private final static String FRAGMENT_TAG = "REVIEW";
    public static final String COLOR = "color";
    public static final String TEXT = "text";
    RelativeLayout loading_layout;
    LottieAnimationView animationView;
    Communication com = new Communication();
    private int stop_repeat2 = 0;
    NumberFormat Format = NumberFormat.getNumberInstance(Locale.UK);

    public static Fragment newInstanceReview(ArrayList<JSONObject> item_list) {
        MyShopReviewFragment f = new MyShopReviewFragment();
        Bundle args = new Bundle();
        String item_list_str = item_list.toString();
        args.putString("item_list", item_list_str); // list 넘기기
        f.setArguments(args);
        return f;
    }

    public ArrayList<JSONObject> sorter(ArrayList<JSONObject> arr2, String sort_name){

        ArrayList<JSONObject> arr2_new = new ArrayList<>();
        ArrayList<JSONObject> sides = new ArrayList<>();
        for(int i =0; i <arr2.size(); i++){
            try {
                String val = (String)((JSONObject) arr2.get(i)).get(sort_name);
                if (val.equals("집계중")) {
                    sides.add(arr2.get(i));
                } else {
                    arr2_new.add(arr2.get(i));
                }
            }catch(Exception ex){

            }
        }


        Comparator<JSONObject> orderByToday = new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject item1, JSONObject item2) {

                int ret = 0;
                try {
                    String item_1 = (String)item1.get(sort_name);
                    String item_2 = (String) item2.get(sort_name);

                    if (Format.parse(item_1).intValue() > Format.parse(item_2).intValue())
                        ret = -1;
                    else if (Format.parse(item_1).intValue() < Format.parse(item_2).intValue())
                        ret = 1;
                    else
                        ret = 0;


                }catch(Exception ex){

                }

                return ret;
            }
        } ;
        Collections.sort(arr2_new, orderByToday);
        arr2_new.addAll(sides);
        return arr2_new;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop_review, container, false);
        RelativeLayout frameLayout = (RelativeLayout) view.findViewById(R.id.frameLayout);

        //로딩 시작
        loading_layout= view.findViewById(R.id.lottie_loading_layout2);
        loading_layout.setVisibility(View.VISIBLE);
        animationView = (LottieAnimationView) view.findViewById(R.id.lottie_loading2);
        animationView.setVisibility(animationView.VISIBLE);
        animationView.setAnimation("19451-blue-preloader.json");
        animationView.loop(true);
        animationView.playAnimation();


        RelativeLayout infoQ = view.findViewById(R.id.review_text_holder);
        infoQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInfo();
            }
        });

        //데이터 불러오기(임시)
        callMyShopData2(view);


        view.scrollTo(0,0);


        return view;
    }

    public void callMyShopData2(View view){
        if(stop_repeat2 == 0 ) {

            stop_repeat2= 1;

            String db_url = Constant.BASE_URL + "/app/myshop";
            com.getJSONArrayDatas(getContext(), db_url, new Callback() {
                @Override
                public void callbackString(String str) {

                }

                @Override
                public void callbackInteger(int integer) {

                }

                @Override
                public void callbackJSONArray(JSONArray array) {


                    if (array.length() == 0) {

                        TextView lottie_text = (TextView) view.findViewById(R.id.lottie_text);
                        lottie_text.setVisibility(View.VISIBLE);
                        animationView.setVisibility(GONE);


                    }

                    if (array.length() > 0) {

                        try {

                            Log.v("test","Test");
                            loading_layout.setVisibility(GONE);


                            JSONObject item_obj = new JSONObject((String) array.get(0));
                            JSONArray arr = (JSONArray) item_obj.get("개별상품정보");
                            ArrayList<JSONObject> arr2 = new ArrayList<>();
                            int sum = 0;
                            for (int k = 0; k < arr.length(); k++) {
                                JSONObject obj = (JSONObject) arr.get(k);
                                try {

                                    //날짜 체크 한번 더


                                    JSONArray arrS = (JSONArray) obj.get("오늘리뷰");
                                    int leng = arrS.length();
                                    sum += leng;
                                    ((JSONObject) arr.get(k)).put("오늘리뷰갯수", String.valueOf(leng));
                                    arr2.add((JSONObject) arr.get(k));
                                } catch (Exception ex) {
                                    ((JSONObject) arr.get(k)).put("오늘리뷰갯수", "집계중");
                                    arr2.add((JSONObject) arr.get(k));
                                }
                            }

                            arr2 = sorter(arr2, "오늘리뷰갯수");

                            TextView mainText = ((TextView) view.findViewById(R.id.item_today_review));
                            mainText.setText(String.valueOf(sum));
                            ListView listView = view.findViewById(R.id.itemlist);
                            listView.setScrollContainer(false);
                            MyShopListAdapter listViewAdapter = new MyShopListAdapter(getContext(), R.layout.list_item_shop_review, arr2, "review", "리뷰는 오늘껏만 보여줌");
                            listView.setAdapter(listViewAdapter);
                            setListViewHeightBasedOnChildren(listView);


                            stop_repeat2 = 0;
                            Log.v("test","Test3");

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }


                    } else {
                        //데이터없음
                    }

                }
            });
        }

    }



    public void showInfo(){

        //다이얼로그 객체 생성
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        //속성 지정
        builder.setTitle("실시간리뷰");
        builder.setMessage("등록된 상품들의 오늘 리뷰 수 입니다. 리뷰 숫자 부분을 누르면, 고객들의 리뷰를 확인할 수 있습니다. 매일 밤 12시에 리셋됩니다");
        //아이콘
        builder.setIcon(android.R.drawable.ic_menu_help);


        //예 버튼 눌렀을 때
        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //텍스트 뷰 객체를 넣어줌..
                // Snackbar.make(textView ,"확인되었습니다",Snackbar.LENGTH_SHORT).show();
            }
        });

        //만들어주기
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView msg = (TextView)dialog.findViewById(android.R.id.message);
        msg.setTextSize(12);
    }
}

package com.spang.ui.myshop;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;
import com.spang.ApplicationFragment;
import com.spang.Communication.Communication;
import com.spang.R;


import org.json.JSONObject;

import java.util.ArrayList;


public class MyShopMainFragment extends ApplicationFragment {

    String TAG = "hell";

    Communication com = new Communication();

    //아이템목록
    ArrayList<JSONObject> item_list_revenue = new ArrayList<>();


    //리뷰목록
    ArrayList<JSONObject> item_list_review = new ArrayList<>();
    TabLayout mTabLayout;
    ViewPager mViewPager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = inflater.inflate(R.layout.activity_myshop, container, false);

        mTabLayout = (TabLayout) root.findViewById(R.id.tabLayout);
        mViewPager = (ViewPager) root.findViewById(R.id.viewPager);
        BottomNavigationView navView = root.findViewById(R.id.nav_view);


        //아이템 오브젝트
        try{

            setupViewPager(mViewPager);
            mTabLayout.setupWithViewPager(mViewPager);


        }catch(Exception ee){

        }


        return root;
    }

    @Override
    public void onPause(){
        super.onPause();

    }




    private void setupViewPager(ViewPager viewPager) {
        MyShopAdapter adapter = new MyShopAdapter(getFragmentManager());
        adapter.addRevenueFragment(item_list_revenue);
        adapter.addReviewFragment(item_list_review);
        viewPager.setAdapter(adapter);
    }
}
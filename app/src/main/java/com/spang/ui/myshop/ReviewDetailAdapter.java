package com.spang.ui.myshop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.spang.R;

import org.json.JSONObject;

import java.util.List;

public class ReviewDetailAdapter  extends ArrayAdapter<JSONObject> {
    String TAG = "hell";
    public ReviewDetailAdapter(Context context, int resource, List<JSONObject> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(null == v) {
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_review, null);
        }

        TextView review = (TextView) v.findViewById(R.id.txtReview);
        TextView reviwer = (TextView) v.findViewById(R.id.txtReviewer);
        TextView star =(TextView) v.findViewById(R.id.txtStar);
        TextView date = (TextView) v.findViewById(R.id.txtDate);


        JSONObject obj = getItem(position);
        try {
            review.setText((String) obj.get("review_text"));
            reviwer.setText((String) obj.get("user_id"));
            star.setText((String) obj.get("star"));
            date.setText((String) obj.get("review_date"));
        }catch(Exception ex) {

        }

        return v;
    }



}

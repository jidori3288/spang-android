package com.spang.ui.myshop;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.spang.ApplicationActivity;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.R;
import com.spang.constants.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class NewItemPreviewActivity extends ApplicationActivity {

    private Timer timer = new Timer();
    private final long DELAY = 500; // in ms
    EditText editText;

    int stop_repeat = 0;
    int data_counter = 0;
    int card_leng = 10;
    int visibleItem_lv = 0;
    int getVisibleItem_gv = 0;
    ArrayList productList = new ArrayList<>();
    Communication com = new Communication();
    RelativeLayout loading_layout;
    TextView error_msg;
    RelativeLayout backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String TAG = "hell";
        setContentView(R.layout.activity_newitem_preview);

        editText = (EditText) findViewById(R.id.search_url);
        editText.requestFocus();

        loadingImage(0);

        error_msg = findViewById(R.id.error_msg);

        backBtn = findViewById(R.id.back_container);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        keywordListener();

    }


    public void loadingImage( int onoff){
        final LottieAnimationView animationView = (LottieAnimationView) findViewById(R.id.lottie_loading);
        RelativeLayout animation_layout = findViewById(R.id.lottie_loading_layout);
        if(onoff == 1) {
            //로딩 시작
            animation_layout.setVisibility(VISIBLE);
            animationView.setVisibility(VISIBLE);
            animationView.setAnimation("19451-blue-preloader.json");
            animationView.loop(true);
            animationView.playAnimation();
        }
        else {
            animation_layout.setVisibility(GONE);
            animationView.setVisibility(GONE);
        }
    }


    public void keywordListener(){


        EditText editText = findViewById(R.id.search_url);
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // 입력되는 텍스트에 변화가 있을 때

                if(timer != null) {
                    timer.cancel();
                }

                error_msg.setText("");
                error_msg.setVisibility(GONE);
            }

            @Override
            public void afterTextChanged(Editable arg0) {

                //avoid triggering event when text is too short
                String url = editText.getText().toString();
                if (url.length() >= 1) {

                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //초기화
                                    getData("0", url);
                                }
                            });

                        }
                    }, DELAY);

                }
            }




            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

        });

    }



    public void getData(String is_open, String url) {


            loadingImage(1);

            Log.v("volley","request");

            //데이터 불러오기(임시)
            String db_url = Constant.BASE_URL + "/app/register/preview";
            HashMap<String, String> hm = new HashMap<>();
            hm.put("url", url);


            RelativeLayout noitem_layout = findViewById(R.id.noitem_text);

            com.sendOptions(getApplicationContext(), db_url, hm, new Callback() {
                @Override
                public void callbackString(String str) {

                }

                @Override
                public void callbackInteger(int integer) {

                }

                @Override
                public void callbackJSONArray(JSONArray array) {


                    loadingImage(0);


                    if (array.length() > 0) {

                        Log.v("volley", "response");

                        try {
                            JSONObject obj = (JSONObject) array.get(0);
                            Boolean isValid = (Boolean)obj.get("valid");
                            String message = (String) obj.get("message");

                            //성공적일때
                            if(isValid == true) {
                                String title = (String) obj.get("title");
                                String image = (String) obj.get("image");
                                Intent itn = new Intent(getApplicationContext(), NewItemFinalActivity.class);
                                itn.putExtra("title", title);
                                itn.putExtra("image", image);
                                startActivity(itn);

                            }

                            //문제가 있을때
                            if(isValid == false) {
                                error_msg.setText(message);
                                error_msg.setVisibility(VISIBLE);
                            }


                        }catch(Exception ex){

                        }


                    } else {
                        noitem_layout.setVisibility(GONE);
                    }
                }
            });

    }
}

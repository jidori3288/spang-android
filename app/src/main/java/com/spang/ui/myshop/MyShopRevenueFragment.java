package com.spang.ui.myshop;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.spang.ApplicationFragment;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.Communication.PreferenceManager;
import com.spang.R;
import com.spang.constants.Constant;
import com.spang.ui.analysis.CardDetailActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.ArrayList;

import static android.view.View.GONE;
import static com.spang.ui.myshop.MyShopListAdapter.setListViewHeightBasedOnChildren;

public  class MyShopRevenueFragment extends ApplicationFragment {

    PreferenceManager pm = new PreferenceManager();
    String TAG = "hell";
    public static final String COLOR = "color";
    public static final String TEXT = "text";
    private final static String FRAGMENT_TAG = "REVENUE";
    Communication com = new Communication();
    NumberFormat Format = NumberFormat.getNumberInstance(Locale.UK);
    RelativeLayout loading_layout;
    LottieAnimationView animationView;
    private int stop_repeat = 0;

    public static Fragment newInstanceRevenue(ArrayList<JSONObject> item_list) {
        MyShopRevenueFragment f = new MyShopRevenueFragment();
        Bundle args = new Bundle();
        String arr_str = item_list.toString();
        args.putString("item_list", arr_str); // list 넘기기
        f.setArguments(args);
        return f;
    }

    public ArrayList<JSONObject> sorter(ArrayList<JSONObject> arr2, String sort_name){

        ArrayList<JSONObject> arr2_new = new ArrayList<>();
        ArrayList<JSONObject> sides = new ArrayList<>();
        for(int i =0; i <arr2.size(); i++){
            try {
                String val = (String)((JSONObject) arr2.get(i)).get(sort_name);
                if (val.equals("집계중")) {
                    sides.add(arr2.get(i));
                } else {
                    arr2_new.add(arr2.get(i));
                }
            }catch(Exception ex){

            }
        }


        Comparator<JSONObject> orderByToday = new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject item1, JSONObject item2) {

                int ret = 0;
                try {
                    String item_1 = (String)item1.get(sort_name);
                    String item_2 = (String) item2.get(sort_name);

                        if (Format.parse(item_1).intValue() > Format.parse(item_2).intValue())
                            ret = -1;
                        else if (Format.parse(item_1).intValue() < Format.parse(item_2).intValue())
                            ret = 1;
                        else
                            ret = 0;


                }catch(Exception ex){

                }

                return ret;
            }
        } ;
        Collections.sort(arr2_new, orderByToday);
        arr2_new.addAll(sides);
        return arr2_new;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop_revenue, container, false);
        RelativeLayout frameLayout = (RelativeLayout) view.findViewById(R.id.frameLayout);

        //로딩 시작
        loading_layout= view.findViewById(R.id.lottie_loading_layout);
        loading_layout.setVisibility(View.VISIBLE);
        animationView = (LottieAnimationView) view.findViewById(R.id.lottie_loading);
        animationView.setVisibility(animationView.VISIBLE);
        animationView.setAnimation("19451-blue-preloader.json");
        animationView.loop(true);
        animationView.playAnimation();


        callMyshopData(view);


        FloatingActionButton fab = view.findViewById(R.id.add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), NewItemPreviewActivity.class);
                startActivity(intent);


            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }




    public void callMyshopData(View view){
        if(stop_repeat == 0 ) {
            stop_repeat =1;
            String db_url = Constant.BASE_URL + "/app/myshop";
            com.getJSONArrayDatas(getContext(), db_url, new Callback() {
                @Override
                public void callbackString(String str) {

                }

                @Override
                public void callbackInteger(int integer) {

                }

                @Override
                public void callbackJSONArray(JSONArray array) {
                    Log.v("t","T");

                    if(array.length() == 0) {

                        TextView lottie_text= (TextView) view.findViewById(R.id.lottie_text);
                        lottie_text.setVisibility(View.VISIBLE);
                        animationView.setVisibility(GONE);


                    }


                    if(array.length() > 0){

                        try {


                            JSONObject item_obj = new JSONObject((String)array.get(0));



                            String revenue_today =(String)item_obj.get("오늘_누적");
                            String revenue_7day = (String)item_obj.get("7일_누적");
                            String revenue_30day = (String)item_obj.get("30일_누적");
                            String revenue_total =(String)item_obj.get("전체_누적");
                            // String update_date = (String) item_obj.get("insert_time");

                            // String update_time = update_date.split("T")[0] +" "+ update_date.split("T")[1].split("000Z")[0];

                            ((TextView)view.findViewById(R.id.revenue_today)).setText(revenue_today + "원");
                            ((TextView)view.findViewById(R.id.revenue_7day)).setText(revenue_7day+ "원");
                            ((TextView)view.findViewById(R.id.revenue_30day)).setText(revenue_30day+ "원");
                            ((TextView)view.findViewById(R.id.revenue_total)).setText(revenue_total+ "원");
                            //((TextView)view.findViewById(R.id.update_date)).setText(update_time);

                            RelativeLayout infoQ = view.findViewById(R.id.revenue_text_holder);
                            infoQ.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    showInfo();
                                }
                            });

                            JSONArray arr = (JSONArray) item_obj.get("개별상품정보");
                            ArrayList<JSONObject> arr2 = new ArrayList<>();
                            for(int k =0; k < arr.length(); k++){
                                arr2.add((JSONObject) arr.get(k));
                            }
                            arr2 = sorter(arr2, "누적매출액오늘");


                            ListView listView = view.findViewById(R.id.itemlist);
                            listView.setScrollContainer(false);

                            MyShopListAdapter listViewAdapter = new MyShopListAdapter(getContext(), R.layout.list_item_shop_revenue, arr2, "revenue", "누적매출액오늘");
                            listView.setAdapter(listViewAdapter);
                            setListViewHeightBasedOnChildren(listView);
                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    try {
                                        JSONObject data = (JSONObject) ((JSONObject) ((MyShopListAdapter) listView.getAdapter()).listObj.get(position)).get("data");
                                        pm.setString(getContext(), "large-data", data.toString());
                                        Intent intent = new Intent(getContext(), CardDetailActivity.class);
                                        getContext().startActivity(intent);
                                    }catch(Exception ex2){
                                        //
                                    }

                                }
                            });


                            //라디오버튼
                            RadioButton buttonToday = view.findViewById(R.id.button_today);
                            RadioButton button7day = view.findViewById(R.id.button_7day);
                            RadioButton button30day = view.findViewById(R.id.button_30day);
                            RadioButton buttonTotal = view.findViewById(R.id.button_total);

                            buttonToday.setChecked(true);

                            loading_layout.setVisibility(GONE);

                            buttonToday.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    MyShopListAdapter adp = (MyShopListAdapter)listView.getAdapter();
                                    adp.extra_info = "누적매출액오늘";
                                    adp.listObj = sorter(adp.listObj, "누적매출액오늘");
                                    adp.notifyDataSetChanged();

                                }
                            });
                            button7day.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    MyShopListAdapter adp = (MyShopListAdapter)listView.getAdapter();
                                    adp.extra_info = "누적매출액7일";
                                    adp.listObj = sorter(adp.listObj, "누적매출액7일");
                                    adp.notifyDataSetChanged();
                                }
                            });
                            button30day.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    MyShopListAdapter adp = (MyShopListAdapter)listView.getAdapter();
                                    adp.extra_info = "누적매출액1개월";
                                    adp.listObj = sorter(adp.listObj, "누적매출액1개월");
                                    adp.notifyDataSetChanged();
                                }
                            });
                            buttonTotal.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    MyShopListAdapter adp = (MyShopListAdapter)listView.getAdapter();
                                    adp.extra_info = "누적매출액";
                                    adp.listObj = sorter(adp.listObj, "누적매출액");
                                    adp.notifyDataSetChanged();
                                }
                            });


                            loading_layout.setVisibility(GONE);
                            stop_repeat = 0;





                        }catch(Exception ex){

                        }


                    }
                    else {
                        //데이터없음
                    }

                }
            });



        }
    }



    public void showInfo(){

        //다이얼로그 객체 생성
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        //속성 지정
        builder.setTitle("실시간매출");
        builder.setMessage("등록된 상품들의 오늘 추정 매출입니다. '집계중'의 경우 집계중이거나 상품이 판매중지인 상태입니다. 실시간으로 매출이 변동되므로 주기적으로 모니터링 해주세요. 모든 데이터는 매일 밤 12시 리셋됩니다");
        //아이콘
        builder.setIcon(android.R.drawable.ic_menu_help);


        //예 버튼 눌렀을 때
        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //텍스트 뷰 객체를 넣어줌..
                // Snackbar.make(textView ,"확인되었습니다",Snackbar.LENGTH_SHORT).show();
            }
        });

        //만들어주기
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView msg = (TextView)dialog.findViewById(android.R.id.message);
        msg.setTextSize(12);
    }


}

package com.spang.ui.myshop;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.spang.ApplicationActivity;
import com.spang.Communication.Callback;
import com.spang.R;
import com.spang.constants.Constant;
import com.spang.ui.productdb.ListViewAdapter;
import com.spang.ui.productdb.Product;
import com.spang.ui.search.SearchListAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import static android.view.View.GONE;

public class ReviewActivity extends ApplicationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String TAG = "hell";
        setContentView(R.layout.activity_review);


        RelativeLayout back_container = findViewById(R.id.back_container);
        back_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        String data_str = getIntent().getStringExtra("data");
        try {
            JSONArray data_obj = new JSONArray(data_str);
            ArrayList<JSONObject> list = new ArrayList<>();
            for(int i=0; i < data_obj.length(); i++){
                list.add((JSONObject) data_obj.get(i));
            }

            ListView review_recyclerview = findViewById(R.id.review_listview);
            ReviewDetailAdapter reviewDetailAdapter = new ReviewDetailAdapter(this, R.layout.item_review, list);
            review_recyclerview.setAdapter(reviewDetailAdapter);
            reviewDetailAdapter.notifyDataSetChanged();
        }catch(Exception ex){
            //ex
        }
    }




}

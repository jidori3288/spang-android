package com.spang.ui.myshop;


import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

class MyShopAdapter extends FragmentStatePagerAdapter {
    String TAG = "hell";
    private List<Fragment> mFragmentList = new ArrayList<>();
    private List<String> mFragmentTitleList = new ArrayList<>();
    public MyShopAdapter(FragmentManager manager) {
        super(manager);
    }
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }
    @Override
    public int getCount() {
        return mFragmentList.size();
    }


    public void addRevenueFragment(ArrayList<JSONObject> item_list_revenue) {
        Fragment fragment = MyShopRevenueFragment.newInstanceRevenue(item_list_revenue);
        mFragmentList.add(fragment);
        mFragmentTitleList.add("성과");
    }


    public void addReviewFragment(ArrayList<JSONObject> item_list_review) {
        Fragment fragment = MyShopReviewFragment.newInstanceReview(item_list_review);
        mFragmentList.add(fragment);
        mFragmentTitleList.add("리뷰");
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        FragmentManager manager = ((Fragment) object).getFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.remove((Fragment) object);
        trans.commit();
    }

}
package com.spang.ui.cardbuy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.spang.R;

import java.util.ArrayList;
import java.util.List;

public class CardBuyActivityAdapter extends RecyclerView.Adapter<CardBuyActivityAdapter.ViewHolder> {
    String TAG = "hell";
    private List<CardBuyActivityModel> cardBuyActivityModelList;
    private Context context;

    public CardBuyActivityAdapter(Context context) {
        this.context = context;
        cardBuyActivityModelList = new ArrayList<>();
    }

    public void updateList(List<CardBuyActivityModel> cardBuyActivityModelList) {
        this.cardBuyActivityModelList = cardBuyActivityModelList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_buy, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return cardBuyActivityModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageViewIcon;
        TextView textViewName;

        public ViewHolder(View itemView) {
            super(itemView);
            imageViewIcon = itemView.findViewById(R.id.imageViewIcon);
            textViewName = itemView.findViewById(R.id.textViewName);


        }

        public void onBind(final int position) {
            final CardBuyActivityModel cardBuyActivityModel = cardBuyActivityModelList.get(position);
            textViewName.setText(cardBuyActivityModel.name);
            Glide.with(context)
                    .load(cardBuyActivityModel.drawable)
                    .into(imageViewIcon);
        }
    }

}

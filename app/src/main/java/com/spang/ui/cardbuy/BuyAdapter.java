package com.spang.ui.cardbuy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.spang.R;

import java.util.List;

public class BuyAdapter extends PagerAdapter {
    String TAG = "hell";
    List<Integer> lstImages;
    Context context;
    LayoutInflater layoutInflater;

    public BuyAdapter(List<Integer> lstImages, Context context) {
        this.lstImages = lstImages;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return  lstImages.size();
    }
    @Override
    public boolean isViewFromObject(View view, Object object){
        return  view.equals(object);
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object){
        container.removeView((View)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position){
        View view = layoutInflater.inflate(R.layout.item_buycard,container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.buycardView);
        imageView.setImageResource(lstImages.get(position));
        container.addView(view);
        return view;
    }
}

package com.spang.ui.cardbuy;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;


import com.spang.ApplicationActivity;
import com.spang.R;
import com.spang.ui.analysis.StartSnapHelper;

import java.util.ArrayList;
import java.util.List;


public class CardBuyActivity extends ApplicationActivity {
    String TAG = "hell";
    private CardBuyActivityAdapter recommendAdapter;
    private CardBuyActivityAdapter categoryAdapter;
    private CardBuyActivityAdapter overseasAdapter;
    private CardBuyActivityAdapter growthAdapter;
    private CardBuyActivityAdapter enemyAdapter;
    private CardBuyActivityAdapter priceAdapter;

    private RecyclerView recommendView;
    private RecyclerView categoryView;
    private RecyclerView overseasView;
    private RecyclerView growthView;
    private RecyclerView enemyView;
    private RecyclerView priceView;

    List<CardBuyActivityModel> RecommendList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_itembuy);

        recommendView =  findViewById(R.id.itembuy_recommend);
        categoryView = findViewById(R.id.itembuy_category);
        overseasView = findViewById(R.id.itembuy_overseas);
        growthView =  findViewById(R.id.itembuy_growth);
        enemyView = findViewById(R.id.itembuy_enemy);
        priceView = findViewById(R.id.itembuy_price);

        setUpRecyclerView();
        recommendAdapter.updateList(getItemList("recommend"));
        categoryAdapter.updateList(getItemList("category"));
        overseasAdapter.updateList(getItemList("overseas"));
        growthAdapter.updateList(getItemList("growth"));
        enemyAdapter.updateList(getItemList("enemy"));
        priceAdapter.updateList(getItemList("price"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



    private void setUpRecyclerView() {


        //랜덤추천카드
        LinearLayoutManager layoutRecommend = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recommendView.setLayoutManager(layoutRecommend);
        recommendAdapter = new CardBuyActivityAdapter(this);
        recommendView.setAdapter(recommendAdapter);
        SnapHelper snapHelper1 = new LinearSnapHelper();
        snapHelper1.attachToRecyclerView(recommendView);


        //카테고리추천카드
        LinearLayoutManager layoutCategory = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        categoryView.setLayoutManager(layoutCategory);
        categoryAdapter = new CardBuyActivityAdapter(this);
        categoryView.setAdapter(categoryAdapter);
        SnapHelper snapHelper2 = new StartSnapHelper();
        snapHelper2.attachToRecyclerView(categoryView);


        //구매대행카드
        LinearLayoutManager layoutOverseas = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        overseasView.setLayoutManager(layoutOverseas);
        overseasAdapter = new CardBuyActivityAdapter(this);
        overseasView.setAdapter(overseasAdapter);
        SnapHelper snapHelper3 = new StartSnapHelper();
        snapHelper3.attachToRecyclerView(overseasView);

        //성장률
        LinearLayoutManager layoutGrowth = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        growthView.setLayoutManager(layoutGrowth);
        growthAdapter = new CardBuyActivityAdapter(this);
        growthView.setAdapter(growthAdapter);
        SnapHelper snapHelper4 = new StartSnapHelper();
        snapHelper4.attachToRecyclerView(growthView);


        //경쟁자
        LinearLayoutManager layoutEnemy = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        enemyView.setLayoutManager(layoutEnemy);
        enemyAdapter = new CardBuyActivityAdapter(this);
        enemyView.setAdapter(enemyAdapter);
        SnapHelper snapHelper5 = new StartSnapHelper();
        snapHelper5.attachToRecyclerView(enemyView);


        //최저가
        LinearLayoutManager layoutPrice = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        priceView.setLayoutManager(layoutPrice);
        priceAdapter = new CardBuyActivityAdapter(this);
        priceView.setAdapter(priceAdapter);
        SnapHelper snapHelper6 = new StartSnapHelper();
        snapHelper6.attachToRecyclerView(priceView);




    }



    public List<CardBuyActivityModel> getItemList(String kind) {

        List<CardBuyActivityModel> appList = new ArrayList<>();

        if(kind.equals("recommend")) {

            appList.add(new CardBuyActivityModel("새싹카드", R.drawable.saessak, "월매출60만"));
            appList.add(new CardBuyActivityModel("파워카드", R.drawable.bronzemedal, "월매출30만"));
            appList.add(new CardBuyActivityModel("빅파워카드", R.drawable.silvermdeal, "월매출30만"));
            appList.add(new CardBuyActivityModel("프리미엄카드", R.drawable.goldmedal, "월매출30만"));
        }
        if(kind.equals("category")){
            appList.add(new CardBuyActivityModel("패션의류", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("패션잡화", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("화장품/미용", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("디지털/가전", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("가구/인테리어", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("출산/육아", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("식품", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("생활/건강", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("여가/생활편의", R.drawable.brochure, ""));
        }
        if(kind.equals("overseas")){
            appList.add(new CardBuyActivityModel("중국", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("일본", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("동남아", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("미국", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("유럽", R.drawable.brochure, ""));
        }
        if(kind.equals("growth")){
            appList.add(new CardBuyActivityModel("이번주", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("이번달", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("올해", R.drawable.brochure, ""));
        }
        if(kind.equals("enemy")){
            appList.add(new CardBuyActivityModel("10명 이하", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("10~30명", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("30~100명", R.drawable.brochure, ""));
        }

        if(kind.equals("price")){
            appList.add(new CardBuyActivityModel("1,000원이하", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("1,000~5,000원", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("5,000~10,000원", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("10,000~30,000원", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("30,000~50,000원", R.drawable.brochure, ""));
            appList.add(new CardBuyActivityModel("50,000~70,000원", R.drawable.brochure, ""));
        }

        return appList;
    }
}

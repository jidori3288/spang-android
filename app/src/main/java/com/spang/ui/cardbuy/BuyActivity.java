package com.spang.ui.cardbuy;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.gigamole.infinitecycleviewpager.HorizontalInfiniteCycleViewPager;
import com.spang.ApplicationActivity;
import com.spang.R;

import java.util.ArrayList;
import java.util.List;

public class BuyActivity extends ApplicationActivity {
    List<Integer> lstImages = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String TAG = "hell";
        setContentView(R.layout.activity_buy);

        initData();
        HorizontalInfiniteCycleViewPager pager = findViewById(R.id.horizontal_cycle);
        BuyAdapter adapter = new BuyAdapter(lstImages, getBaseContext());
        pager.setAdapter(adapter);

    }
    private void initData(){
        lstImages.add(R.drawable.joker);
        //lstImages.add(R.drawable.buycard3);
        //lstImages.add(R.drawable.buycard4);
        //lstImages.add(R.drawable.buycard5);
    }
}

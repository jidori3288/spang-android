package com.spang.ui.payment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.anjlab.android.iab.v3.PurchaseData;
import com.google.gson.Gson;
import com.spang.ApplicationActivity;

import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.spang.R;
import com.spang.models.PaymentProduct;
import com.spang.models.PaymentTransaction;
import com.spang.models.User;
import com.spang.models.UserPoint;
import com.spang.utils.DateFormatConverter;
import com.spang.utils.HttpClient;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class PaymentActivity extends ApplicationActivity implements BillingProcessor.IBillingHandler {
    String TAG = "hell";
    String license_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApQVmTosfdUanUsN3WSdu2YM0jyWEifJzsx/7u+LnLWvVX26Ebo077IJMCrN2hbJITJ7gh7wAQpbGgKjRIyisRi3fnkvXx/+iQqGiuJS1VETDcD4E4xVGwRLj0QBewOlWcK+SCUxwwk1JltEIQh6x2JQCxzkzfyQRiQepXNeYT/FGVNCvfNawYtfOyxe8CFBCF6l/q90om0R33jtWalrl68fKG9UVMuH+FLw+cfa+Dn3NgKWEXwwBmHH3OQ6pLEzOxRcQcMpc0O6fH2zi1Bb5Z7oCk1HzOjOt6fZBj8LaifVpE5mYnVAl8Xd3s7Q+ew5orIe7tgrm6M8WUAymf8KR9wIDAQAB";

    private ListView paymentProductListView;
    private BillingProcessor bp;
    private TextView pointText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        bp = new BillingProcessor(this, license_key, this);
        bp.initialize();

        paymentProductListView = (ListView) findViewById(R.id.payment_product_list);

        pointText = findViewById(R.id.point_line_2);
        fetchUserPoint();

        //백버튼
        ImageView backbtn = findViewById(R.id.backBtn);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        if (bp != null) {
            bp.release();
        }
        super.onDestroy();
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        // * 구매 완료시 호출
        Toast.makeText(this, productId + " 구매 완료", Toast.LENGTH_LONG).show();

        bp.consumePurchase(productId);

        createPaymentTransaction(details);

//        details.purchaseInfo.purchaseData.purchaseToken
        // productId: 구매한 sku (ex) no_ads)
        // details: 결제 관련 정보
    }

    @Override
    public void onPurchaseHistoryRestored() {
        // * 구매 정보가 복원되었을때 호출
        // bp.loadOwnedPurchasesFromGoogle() 하면 호출 가능
    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        Toast.makeText(this, "에러코드:"+errorCode, Toast.LENGTH_LONG).show();
        Log.d("!!!!!", "" + errorCode);
        switch (errorCode) {
            case 0:
                Log.d("onBillingError", "> Success - BILLING_RESPONSE_RESULT_OK");
                break;
            case 1:
                Log.d("onBillingError", "> User pressed back or canceled a dialog - BILLING_RESPONSE_RESULT_USER_CANCELED");
                break;
            case 3:
                Log.d("onBillingError", "> Billing API version is not supported for the type requested - BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE");
                break;
            case 4:
                Log.d("onBillingError", "> Requested product is not available for purchase - BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE");
                break;
            case 5:
                Log.d("onBillingError", "> Invalid arguments provided to the API. This error can also indicate that the application was not correctly signed or properly set up for In-app Billing in Google Play, or does not have the necessary permissions in its manifest - BILLING_RESPONSE_RESULT_DEVELOPER_ERROR");
                break;
            case 6:
                Log.d("onBillingError", "> Fatal error during the API action - BILLING_RESPONSE_RESULT_ERROR");
                break;
            case 7:
                Log.d("onBillingError", "> Failure to purchase since item is already owned - BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED");
                break;
            case 8:
                Log.d("onBillingError", "> Failure to consume since item is not owned - BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED");
                break;
        }
    }

    @Override
    public void onBillingInitialized() {
        fetchPaymentProducts();
    }

    private void fetchPaymentProducts(){
        HttpClient.getWithArrayResponse(this, "/payment_products", new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Gson gson = new Gson();
                PaymentProduct[] paymentProductArray = gson.fromJson(response.toString(), PaymentProduct[].class);
                ArrayList<PaymentProduct> paymentProducts = new ArrayList<>(Arrays.asList(paymentProductArray));
                setPaymentProductAdapter(paymentProducts);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
            }
        });
    }

    private void setPaymentProductAdapter(ArrayList<PaymentProduct> paymentProducts){
        View.OnClickListener onPaymentProductClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PaymentProduct selectedPaymentProduct = null;
                Integer id = (Integer) view.getTag();
                for(PaymentProduct paymentProduct : paymentProducts){
                    if(id == paymentProduct.getId()){
                        selectedPaymentProduct = paymentProduct;

                        break;
                    }
                }
                User user = User.fromSharedPreference(PaymentActivity.this);

                bp.purchase((Activity) PaymentActivity.this, selectedPaymentProduct.getProductId(), "{\"user_id\": " + user.getId() + "}");
            }
        };

        PaymentProductAdapter paymentProductAdapter =
                new PaymentProductAdapter(PaymentActivity.this, paymentProducts, onPaymentProductClickListener);

        paymentProductListView.setAdapter(paymentProductAdapter);
    }
    private void createPaymentTransaction(@Nullable TransactionDetails details){
        // TODO 서버에서 해야할 것
        // https://developer.android.com/google/play/billing/billing_library_overview?hl=ko#java
        if(details != null){
            PurchaseData purchaseData = details.purchaseInfo.purchaseData;

            HashMap<String, String> data = new HashMap<>();
            data.put("product_id", purchaseData.productId);
            data.put("purchase_token", purchaseData.purchaseToken);
            data.put("purchase_time", DateFormatConverter.format(purchaseData.purchaseTime));
            data.put("developer_payload", purchaseData.developerPayload);

            HttpClient.post(this, "/payment_transactions", data, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Gson gson = new Gson();
                    PaymentTransaction paymentTransaction = gson.fromJson(response.toString(), PaymentTransaction.class);
                    String productName = paymentTransaction.getPaymentProduct().getName();

                    fetchUserPoint();

                    Toast.makeText(PaymentActivity.this, productName + "을 구매 완료 했습니다.", Toast.LENGTH_SHORT).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, error.toString());
                }
            });
        }
    }

    private void fetchUserPoint() {
        HttpClient.get(this, "/user_points/me", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                UserPoint userPoint = gson.fromJson(response.toString(), UserPoint.class);
                String point = toNumFormat(userPoint.getPoint()) + " P";
                pointText.setText(point);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
            }
        });
    }

    public static String toNumFormat(int num) {
        DecimalFormat df = new DecimalFormat("#,###");
        return df.format(num);
    }
}

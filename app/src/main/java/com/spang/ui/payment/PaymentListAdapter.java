package com.spang.ui.payment;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.spang.R;
import com.spang.models.PaymentTransaction;
import com.spang.utils.DateFormatConverter;

import java.util.ArrayList;

public class PaymentListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<PaymentTransaction> list;

    private ViewHolder viewHolder;

    public PaymentListAdapter(Context context, ArrayList<PaymentTransaction> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_payment_list_item, parent, false);

            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        PaymentTransaction paymentTransaction = (PaymentTransaction) getItem(position);
        viewHolder.nameText.setText(paymentTransaction.getPaymentProduct().getName());
        if(paymentTransaction.getPurchaseTime() != null)
            viewHolder.dateText.setText(DateFormatConverter.format(paymentTransaction.getPurchaseTime()));

        return convertView;
    }

    public class ViewHolder {

        private LinearLayout container;
        private TextView nameText;
        private TextView dateText;

        public ViewHolder(View convertView) {
            nameText = (TextView) convertView.findViewById(R.id.txtPayName);
            dateText = (TextView) convertView.findViewById(R.id.txtPayDate);
        }
    }
}

package com.spang.ui.payment;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.spang.R;
import com.spang.models.PaymentProduct;

import java.util.ArrayList;

public class PaymentProductAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<PaymentProduct> list;
    private View.OnClickListener onClickListener;

    private ViewHolder viewHolder;

    public PaymentProductAdapter(Context context,
                                 ArrayList<PaymentProduct> list,
                                 View.OnClickListener onClickListener) {

        this.context = context;
        this.list = list;
        this.onClickListener = onClickListener;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if(null == v) {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_payment_product_item, null);
        }

        RelativeLayout container = v.findViewById(R.id.container);
       TextView name_text =  v.findViewById(R.id.name_text);
        TextView name_desc =  v.findViewById(R.id.name_desc);
        TextView name_desc2 =  v.findViewById(R.id.name_desc2);
        TextView price_btn =  v.findViewById(R.id.price_btn);
        ImageView point_img =  v.findViewById(R.id.point_img);


        PaymentProduct paymentProduct = (PaymentProduct) getItem(position);
        Glide.with(context).load(paymentProduct.getImg()).diskCacheStrategy(DiskCacheStrategy.ALL).into(point_img);
        name_text.setText(paymentProduct.getName());
        name_desc.setText(paymentProduct.getNameDesc());
        name_desc2.setText(paymentProduct.getNameDesc2());
        price_btn.setText(paymentProduct.getPriceBtn());
        price_btn.setTag(paymentProduct.getId());

        price_btn.setOnClickListener(onClickListener);

        return v;
    }

    public class ViewHolder {

        private RelativeLayout container;


        private ImageView point_img;
        private TextView name_text;
        private TextView name_desc;
        private TextView name_desc2;
        private TextView price_btn;


        public ViewHolder(View convertView) {
            container = (RelativeLayout) convertView.findViewById(R.id.container);
            point_img = (ImageView) convertView.findViewById(R.id.point_img);
            name_text = (TextView) convertView.findViewById(R.id.name_text);
            name_desc = (TextView) convertView.findViewById(R.id.name_desc);
            name_desc2 = (TextView) convertView.findViewById(R.id.name_desc2);
            price_btn = (TextView) convertView.findViewById(R.id.price_btn);

        }
    }
}

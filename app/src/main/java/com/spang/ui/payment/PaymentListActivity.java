package com.spang.ui.payment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.spang.ApplicationActivity;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.R;
import com.spang.constants.Constant;
import com.spang.models.PaymentProduct;
import com.spang.models.PaymentTransaction;
import com.spang.ui.myshop.ReviewDetailAdapter;
import com.spang.utils.HttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static android.view.View.GONE;

public class PaymentListActivity  extends ApplicationActivity {
    private final String TAG = "PaymentListActivity";
    private ListView listView;
    private TextView noItemText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paylist);
        noItemText = (TextView) findViewById(R.id.payment_no_item);
        listView = (ListView) findViewById(R.id.payment_listview);

        fetchPaymentTransactions();

        ImageView backbtn = findViewById(R.id.backBtn);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void fetchPaymentTransactions(){
        HttpClient.getWithArrayResponse(this, "/payment_transactions", new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Gson gson = new Gson();
                PaymentTransaction[] paymentTransactionArray = gson.fromJson(response.toString(), PaymentTransaction[].class);
                ArrayList<PaymentTransaction> paymentTransactions = new ArrayList<>(Arrays.asList(paymentTransactionArray));
                if(paymentTransactions.size() > 0){
                    PaymentListAdapter paymentListAdapter = new PaymentListAdapter(PaymentListActivity.this, paymentTransactions);
                    listView.setAdapter(paymentListAdapter);
                }else{
                    listView.setVisibility(View.GONE);
                    noItemText.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
            }
        });
    }

}

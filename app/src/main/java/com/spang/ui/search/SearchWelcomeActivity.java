package com.spang.ui.search;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.airbnb.lottie.LottieAnimationView;
import com.spang.ApplicationFragment;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.Communication.PreferenceManager;
import com.spang.R;
import com.spang.constants.Constant;
import com.spang.ui.productdb.Product;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class SearchWelcomeActivity extends ApplicationFragment {
    Communication com = new Communication();
    PreferenceManager pm = new PreferenceManager();

    int data_counter =0;
    int stop_repeat = 0;
    int card_leng = 30;
    int repeat_count = 0;

    Context context;
    SearchWelcomeAdapter searchWelcomeAdapter;
    RecyclerView themeRecyclerView;
    ArrayList<Product> productList;

    boolean loading = true;

    int first = 0;

    View root;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = inflater.inflate(R.layout.activity_searchwelcome, container, false);
        context = getContext();
        productList = new ArrayList<>();
        themeRecyclerView = root.findViewById(R.id.themeRecyclerView);

        //레이아웃 설정
        setupGrid();

        //초기
        getData(root , "1", "intro");

        //테마별 버튼
        themeSetting(root);

        RelativeLayout search_container = root.findViewById(R.id.search_container);
        search_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SearchActivity.class);
                startActivity(intent);
            }
        });


        return root;
    }


    public void themeSetting(View root) {
        Button theme_popular = root.findViewById(R.id.theme_popular);
        Button theme_recommend = root.findViewById(R.id.theme_recommend);
        Button theme_revenue = root.findViewById(R.id.theme_revenue);
        Button theme_china = root.findViewById(R.id.theme_china);
        Button theme_overseas = root.findViewById(R.id.theme_overseas);
        Button theme_comparison = root.findViewById(R.id.theme_comparison);
        theme_popular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ThemeActivity.class);
                intent.putExtra("theme-name", "popular");
                intent.putExtra("theme-name-kr", "트렌드");
                startActivity(intent);
            }
        });
        theme_recommend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ThemeActivity.class);
                intent.putExtra("theme-name", "recommend");
                intent.putExtra("theme-name-kr", "추천");
                startActivity(intent);
            }
        });
        theme_revenue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ThemeActivity.class);
                intent.putExtra("theme-name", "revenue");
                intent.putExtra("theme-name-kr", "고매출");
                startActivity(intent);
            }
        });

        theme_china.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ThemeActivity.class);
                intent.putExtra("theme-name", "china");
                intent.putExtra("theme-name-kr", "중국산");
                startActivity(intent);
            }
        });
        theme_overseas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ThemeActivity.class);
                intent.putExtra("theme-name", "overseas");
                intent.putExtra("theme-name-kr", "구매대행");
                startActivity(intent);
            }
        });
        theme_comparison.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ThemeActivity.class);
                intent.putExtra("theme-name", "comparison");
                intent.putExtra("theme-name-kr", "가격비교");
                startActivity(intent);
            }
        });
    }


    private void setupGrid() {
        final StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        themeRecyclerView.setLayoutManager(layoutManager);
        themeRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                // userPhotosAdapter.setLockedAnimations(true);
            }
        });
    }


    public void loadingImage(View root, int onoff){
        final LottieAnimationView animationView = (LottieAnimationView) root.findViewById(R.id.lottie_loading);
        RelativeLayout animation_layout = root.findViewById(R.id.lottie_loading_layout);
        if(onoff == 1) {
            //로딩 시작
            animation_layout.setVisibility(VISIBLE);
            animationView.setVisibility(VISIBLE);
            animationView.setAnimation("19451-blue-preloader.json");
            animationView.loop(true);
            animationView.playAnimation();
        }
        else {
            animation_layout.setVisibility(GONE);
            animationView.setVisibility(GONE);
        }
    }

    public void getData(View roots,String is_open, String theme) {

        if(stop_repeat == (productList.size()) ) {

            root = roots;

            stop_repeat+= card_leng;

            repeat_count++;

            loadingImage(root,1);

            Log.v("volley","request");

            //데이터 불러오기(임시)
           // String db_url = Constant.BASE_URL + "/app/theme/" + theme;
            String db_url = Constant.BASE_URL + "/app/theme";
            HashMap<String, String> hm = pm.getTodayFilterPref(getContext());
            hm.put("is_open", is_open);
            hm.put("data_counter", String.valueOf(data_counter));
            hm.put("card_leng", String.valueOf(card_leng));
            hm.put("theme", theme);


            com.sendOptions(getContext(), db_url, hm, new Callback() {
                @Override
                public void callbackString(String str) {

                }

                @Override
                public void callbackInteger(int integer) {

                }

                @Override
                public void callbackJSONArray(JSONArray array) {
                    if (array.length() > 0) {

                        Log.v("volley","response");

                        loadingImage(root,0);

                        data_counter += card_leng;

                        List<Product> temp = new ArrayList<>();
                        //리스츠 투가하기
                        for (int i = 0; i < array.length(); i++) {
                            try {
                                String obj = (String) array.get(i);
                                temp.add(new Product(new JSONObject(obj)));
                                productList.add(new Product(new JSONObject(obj)));
                            } catch (Exception ex) {

                            }

                        } //


                        //어댑터연결
                        if(first == 0) {
                            if(getActivity() != null) {
                                searchWelcomeAdapter = new SearchWelcomeAdapter(getActivity() , productList);
                                themeRecyclerView.setAdapter(searchWelcomeAdapter);
                                first++;
                            }
                        }
                        else {
                            SearchWelcomeAdapter adp = (SearchWelcomeAdapter)themeRecyclerView.getAdapter();
                            adp.notifyDataSetChanged();
                        }


                        loading = false;


                        //그리드뷰 셋팅
                        themeRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

                            private int previousTotal = 0;

                            @Override
                            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                                super.onScrollStateChanged(recyclerView, newState);
                            }

                            @Override
                            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);

                                if (!themeRecyclerView.canScrollVertically(-1)) {
                                    Log.i("hh", "Top of list");
                                } else if (!themeRecyclerView.canScrollVertically(1)) {
                                    Log.i("hh", "End of list");
                                    if(!loading) {
                                        getData(root, "0",theme);
                                        loading = true;
                                    }
                                } else {
                                    Log.i("hh", "idle");
                                }
                            }

                        });


                    } else {
                        loadingImage(root, 0);
                    }
                }
            });
        }
    }







    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //setPoint();
    }
    //상세보기에서 다시 돌아온경우
    @Override
    public void onResume() {

        super.onResume();

    }


    private class BackgroundTask extends AsyncTask<Void, Void, Void> {

        //일단 이게 시작됨. 푸터를 끝에 넣는다.
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
             return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }

    }

}

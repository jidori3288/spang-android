package com.spang.ui.search;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.airbnb.lottie.LottieAnimationView;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.spang.ApplicationActivity;
import com.spang.ApplicationFragment;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.Communication.PreferenceManager;
import com.spang.R;
import com.spang.constants.Constant;
import com.spang.ui.analysis.CardDetailActivity;
import com.spang.ui.productdb.Product;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.MODE_PRIVATE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class SearchActivity   extends ApplicationActivity {
    String TAG = "hell";
    private ListView listView;
    private SearchListAdapter listViewAdapter;
    private List<Product> productList;
    public LottieAnimationView animationView;

    Communication com = new Communication();
    PreferenceManager pm = new PreferenceManager();

    int stop_repeat = 0;
    int data_counter = 0;
    int card_leng = 10;
    int visibleItem_lv = 0;
    int getVisibleItem_gv = 0;
    RelativeLayout loading_layout;
    int repeat_count = 0;
    private ViewStub stubList;


    String keyword = "";


    RelativeLayout noitem_layout;
    private Timer timer = new Timer();
    private final long DELAY = 1000; // in ms

    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        noitem_layout = findViewById(R.id.noitem_text);

        stubList = (ViewStub) findViewById(R.id.stub_list);
        stubList.inflate();

        listView = (ListView) findViewById(R.id.mylistview2);

        productList = new ArrayList<>();


        //로딩 시작
        loading_layout = findViewById(R.id.lottie_loading_layout);


        keywordListener();


        //검색창
        editText = (EditText) findViewById(R.id.search_text);
        editText.requestFocus();
        //InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);


        ImageView back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });




    }

    public void keywordListener(){


        EditText editText = findViewById(R.id.search_text);

        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // 입력되는 텍스트에 변화가 있을 때
                if(timer != null)
                    timer.cancel();
            }

            @Override
            public void afterTextChanged(Editable arg0) {

                //avoid triggering event when text is too short
                keyword = editText.getText().toString();
                if (keyword.length() >= 1) {

                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {


                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //초기화
                                    stop_repeat = 0;
                                    data_counter = 0;
                                    card_leng = 10;
                                    visibleItem_lv = 0;
                                    getVisibleItem_gv = 0;
                                    productList = new ArrayList<>();
                                    getData("0", keyword);
                                }
                            });

                        }
                    }, DELAY);



                    }
                }




            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

        });

    }



    public void getData(String is_open, String keyword) {

        noitem_layout.setVisibility(GONE);

        if(stop_repeat == productList.size()) {

            stop_repeat+= card_leng;

            repeat_count++;

            loadingImage(1);

            Log.v("volley","request");

            //데이터 불러오기(임시)
            String db_url = Constant.BASE_URL + "/app/keyword";
            HashMap<String, String> hm = pm.getTodayFilterPref(getApplicationContext());
            hm.put("is_open", is_open);
            hm.put("data_counter", String.valueOf(data_counter));
            hm.put("card_leng", String.valueOf(card_leng));
            hm.put("opt_keyword", keyword);


            RelativeLayout noitem_layout = findViewById(R.id.noitem_text);

            com.sendOptions(getApplicationContext(), db_url, hm, new Callback() {
                @Override
                public void callbackString(String str) {

                }

                @Override
                public void callbackInteger(int integer) {

                }

                @Override
                public void callbackJSONArray(JSONArray array) {


                    if (array.length() > 0) {

                        noitem_layout.setVisibility(GONE);

                        Log.v("volley", "response");

                        loadingImage(0);

                        data_counter += card_leng;

                        //리스츠 투가하기
                        for (int i = 0; i < array.length(); i++) {
                            try {
                                String obj = (String) array.get(i);
                                productList.add(new Product(new JSONObject(obj)));
                            } catch (Exception ex) {

                            }

                        }
                        //어댑터연결
                        listViewAdapter = new SearchListAdapter(getApplicationContext(), R.layout.list_item, productList);
                        listView.setAdapter(listViewAdapter);
                        listViewAdapter.notifyDataSetChanged();


                        //뷰모드 세팅
                        listView.setOnItemClickListener(onItemClick);


                        listView.setSelection(productList.size() - card_leng - visibleItem_lv + 1);

                        //스크롤 리스너
                        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

                            private int visibleThreshold = 0;
                            private int currentPage = 0;
                            private int previousTotal = 0;
                            private boolean loading = true;

                            @Override
                            public void onScrollStateChanged(AbsListView view, int scrollState) {

                            }

                            @Override
                            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                                if (loading) {
                                    if (totalItemCount > previousTotal) {
                                        loading = false;
                                        previousTotal = totalItemCount;
                                        currentPage++;
                                    }
                                }
                                if (!loading && ((firstVisibleItem + visibleItemCount) == totalItemCount)) {
                                    visibleItem_lv = visibleItemCount;
                                    getData("0", keyword);
                                    loading = true;
                                }
                            }
                        });


                    } else {
                        noitem_layout.setVisibility(GONE);
                    }
                }
            });
        }
    }

    public void loadingImage(int onoff){
        final LottieAnimationView animationView = (LottieAnimationView) findViewById(R.id.lottie_loading);
        RelativeLayout animation_layout = findViewById(R.id.lottie_loading_layout);
        if(onoff == 1) {
            //로딩 시작
            animation_layout.setVisibility(VISIBLE);
            animationView.setVisibility(VISIBLE);
            animationView.setAnimation("19451-blue-preloader.json");
            animationView.loop(true);
            animationView.playAnimation();
        }
        else {
            animation_layout.setVisibility(GONE);
            animationView.setVisibility(GONE);
        }
    }


    AdapterView.OnItemClickListener onItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            try {
                String data =  productList.get(position).getData().toString();
                pm.setString(getApplicationContext(), "large-data", data);
                Intent intent = new Intent(getApplicationContext(), CardDetailActivity.class);
                startActivity(intent);
            }catch(Exception ex2) {
                //
            }

        }
    };



}
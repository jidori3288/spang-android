package com.spang.ui.search;


import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bumptech.glide.Glide;
import com.spang.Communication.PreferenceManager;
import com.spang.R;
import com.spang.ui.analysis.CardDetailActivity;
import com.spang.ui.productdb.Product;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchWelcomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int PHOTO_ANIMATION_DELAY = 600;
    private static final Interpolator INTERPOLATOR = new DecelerateInterpolator();
    private final Context context;
    private final int cellSize;
    private List<Product> product_list;
    private boolean lockedAnimations = false;
    private int lastAnimatedItem = -1;
    private static int screenWidth = 0;
    private static int screenHeight = 0;
    PreferenceManager pm = new PreferenceManager();

    public SearchWelcomeAdapter(Context context, @NonNull ArrayList<Product> product_list) {
        this.context = context;
        this.cellSize = getScreenWidth(context) / 3;
        this.product_list = product_list;

    }
    public static int getScreenWidth(Context c) {
        if (screenWidth == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenWidth = size.x;
        }

        return screenWidth;
    }
    public static int getScreenHeight(Context c) {
        if (screenHeight == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenHeight = size.y;
        }

        return screenHeight;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Log.v("검색onCreateViewHolder", "onCreateViewHolder");
        final View view = LayoutInflater.from(context).inflate(R.layout.item_photo, parent, false);
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
        layoutParams.height = cellSize;
        layoutParams.width = cellSize;
        layoutParams.setFullSpan(false);
        view.setLayoutParams(layoutParams);
        return new PhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.v("검색", "onBindViewHolder");
        bindPhoto((PhotoViewHolder) holder, position);
    }



    private void bindPhoto(final PhotoViewHolder holder, int position) {

        Log.v("검색", String.valueOf(position));

        Product product = (Product)product_list.get(position);
        JSONObject obj = product.getData();

        try {
            Glide.with(context)
                    .load((String) obj.get("image_url"))
                    .override(cellSize, cellSize)
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .into(holder.ivPhoto);


            holder.ivPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = holder.getAdapterPosition();

                    try {
                        String data = product_list.get(pos).getData().toString();
                        pm.setString(v.getContext(), "large-data", data);
                        Intent intent = new Intent(v.getContext(), CardDetailActivity.class);
                        context.startActivity(intent);
                    } catch (Exception ex2) {
                        //
                    }
                }

                ;
            });
        }catch(Exception ex){
            //
        }


        if (lastAnimatedItem < position) lastAnimatedItem = position;
    }

    private void animatePhoto(PhotoViewHolder viewHolder) {
        if (!lockedAnimations) {
            if (lastAnimatedItem == viewHolder.getPosition()) {
                setLockedAnimations(true);
            }

            long animationDelay = PHOTO_ANIMATION_DELAY + viewHolder.getPosition() * 30;

            viewHolder.flRoot.setScaleY(0);
            viewHolder.flRoot.setScaleX(0);

            viewHolder.flRoot.animate()
                    .scaleY(1)
                    .scaleX(1)
                    .setDuration(200)
                    .setInterpolator(INTERPOLATOR)
                    .setStartDelay(animationDelay)
                    .start();
        }
    }

    @Override
    public int getItemCount() {
        return product_list.size();
    }

    static class PhotoViewHolder extends RecyclerView.ViewHolder {
        FrameLayout flRoot;
        ImageView ivPhoto;

        public PhotoViewHolder(View view) {
            super(view);
            flRoot = view.findViewById(R.id.flRoot);
            ivPhoto = view.findViewById(R.id.ivPhoto);
        }
    }

    public void setLockedAnimations(boolean lockedAnimations) {
        this.lockedAnimations = lockedAnimations;
    }
}
package com.spang.ui.search;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.airbnb.lottie.LottieAnimationView;
import com.spang.ApplicationActivity;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.Communication.PreferenceManager;
import com.spang.R;
import com.spang.constants.Constant;
import com.spang.ui.productdb.Product;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ThemeActivity extends ApplicationActivity {
    Communication com = new Communication();
    PreferenceManager pm = new PreferenceManager();

    int data_counter =0;
    int stop_repeat = 0;
    int card_leng = 30;
    int repeat_count = 0;

    Context context;
    SearchWelcomeAdapter searchWelcomeAdapter;
    RecyclerView themeRecyclerView;
    ArrayList<Product> productList;

    boolean loading = true;

    int first = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme);
        productList = new ArrayList<>();
        themeRecyclerView = findViewById(R.id.themeRecyclerView);

        //레이아웃 설정
        setupGrid();


        String theme_name = getIntent().getStringExtra("theme-name");
        String theme_name_kr = getIntent().getStringExtra("theme-name-kr");

        //초기
        getData(null , "1", theme_name);

        RelativeLayout search_container = findViewById(R.id.search_container);
        search_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView theme_text = findViewById(R.id.theme_text);
        theme_text.setText(theme_name_kr);

    }




    private void setupGrid() {
        final StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        themeRecyclerView.setLayoutManager(layoutManager);
        themeRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                // userPhotosAdapter.setLockedAnimations(true);
            }
        });
    }


    public void loadingImage(View root, int onoff){
        final LottieAnimationView animationView = (LottieAnimationView) findViewById(R.id.lottie_loading);
        RelativeLayout animation_layout = findViewById(R.id.lottie_loading_layout);
        if(onoff == 1) {
            //로딩 시작
            animation_layout.setVisibility(VISIBLE);
            animationView.setVisibility(VISIBLE);
            animationView.setAnimation("19451-blue-preloader.json");
            animationView.loop(true);
            animationView.playAnimation();
        }
        else {
            animation_layout.setVisibility(GONE);
            animationView.setVisibility(GONE);
        }
    }

    public void getData(View roots,String is_open, String theme) {

        if(stop_repeat == (productList.size()) ) {


            stop_repeat+= card_leng;


            repeat_count++;

            loadingImage(null,1);

            Log.v("volley","request");

            //데이터 불러오기(임시)
           // String db_url = Constant.BASE_URL + "/app/theme/" + theme;
            String db_url = Constant.BASE_URL + "/app/theme";
            HashMap<String, String> hm = pm.getTodayFilterPref(getApplicationContext());
            hm.put("is_open", is_open);
            hm.put("data_counter", String.valueOf(data_counter));
            hm.put("card_leng", String.valueOf(card_leng));
            hm.put("theme", theme);


            com.sendOptions(getApplicationContext(), db_url, hm, new Callback() {
                @Override
                public void callbackString(String str) {

                }

                @Override
                public void callbackInteger(int integer) {

                }

                @Override
                public void callbackJSONArray(JSONArray array) {
                    if (array.length() > 0) {

                        Log.v("volley","response");

                        loadingImage(null,0);

                        data_counter += card_leng;

                        List<Product> temp = new ArrayList<>();
                        //리스츠 투가하기
                        for (int i = 0; i < array.length(); i++) {
                            try {
                                String obj = (String) array.get(i);
                                temp.add(new Product(new JSONObject(obj)));
                                productList.add(new Product(new JSONObject(obj)));
                            } catch (Exception ex) {

                            }

                        } //


                        //어댑터연결
                        if(first == 0) {
                            searchWelcomeAdapter = new SearchWelcomeAdapter(getApplicationContext(), productList);
                            themeRecyclerView.setAdapter(searchWelcomeAdapter);
                            first++;
                        }
                        else {
                            SearchWelcomeAdapter adp = (SearchWelcomeAdapter)themeRecyclerView.getAdapter();
                            adp.notifyDataSetChanged();
                        }


                        loading = false;


                        //그리드뷰 셋팅
                        themeRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

                            private int previousTotal = 0;

                            @Override
                            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                                super.onScrollStateChanged(recyclerView, newState);
                            }

                            @Override
                            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);

                                if (!themeRecyclerView.canScrollVertically(-1)) {
                                    Log.i("hh", "Top of list");
                                } else if (!themeRecyclerView.canScrollVertically(1)) {
                                    Log.i("hh", "End of list");
                                    if(!loading) {
                                        getData(null, "0",theme);
                                        loading = true;
                                    }
                                } else {
                                    Log.i("hh", "idle");
                                }
                            }

                        });


                    } else {
                        loadingImage(null, 0);
                    }
                }
            });
        }
    }







    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //setPoint();
    }
    //상세보기에서 다시 돌아온경우
    @Override
    public void onResume() {

        super.onResume();

    }


    private class BackgroundTask extends AsyncTask<Void, Void, Void> {

        //일단 이게 시작됨. 푸터를 끝에 넣는다.
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
             return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }

    }

}

package com.spang.ui.sign;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.kakao.auth.AuthType;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.KakaoSDK;
import com.kakao.auth.Session;
import com.kakao.util.exception.KakaoException;
import com.kakao.util.helper.log.Logger;
import com.spang.ApplicationActivity;
import com.spang.MainActivity;
import com.spang.R;
import com.spang.constants.Constant;
import com.spang.models.User;
import com.spang.ui.analysis.WebsiteViewActivity;
import com.spang.utils.HashKey;
import com.spang.utils.HttpClient;
import com.spang.utils.KakaoSdkAdapter;

import org.json.JSONObject;

import java.util.HashMap;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.view.View.VISIBLE;

public class SignActivity extends ApplicationActivity {
    String TAG = "SIGN";

    private SessionCallback callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
//        TODO Kakao 로그인 등 HashKey가 필요시에 사용할 것
        Log.d("HashKey", HashKey.get(this));

        try {
            KakaoSdkAdapter kakaoSdkAdapter= new KakaoSdkAdapter(this.getApplicationContext());
            KakaoSDK.init(kakaoSdkAdapter);
        }catch(KakaoSDK.AlreadyInitializedException ex){
            ex.printStackTrace();
        }

        callback = new SessionCallback();
        Session session = Session.getCurrentSession();
        session.addCallback(callback);

        TextView textView = (TextView) findViewById(R.id.categoryText);
        String category = User.getCategoryFromSharedPreference(this);
        if(category != null){
            textView.setText(category + " 카테고리");
        }

        TextView agree = findViewById(R.id.agree);
        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), WebsiteViewActivity.class);
                intent.putExtra("url",  Constant.BASE_URL +"/app/yakwan");
                startActivity(intent.addFlags(FLAG_ACTIVITY_NEW_TASK));

            }
        });
   }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Session.getCurrentSession().removeCallback(callback);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data)) {
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void signIn(String accessToken){
        HashMap<String, String> data = new HashMap<>();
        data.put("access_token", accessToken);

        HttpClient.post(this, "/users", data, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                afterSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
            }
        });
    }

    private void afterSuccess(JSONObject response){
        Gson gson = new Gson();
        User user = gson.fromJson(response.toString(), User.class);
        user.toSharedPreference(this);

        updateCategory();
    }

    private void updateCategory(){
        String selectedCategory = User.getCategoryFromSharedPreference(this);

        if(selectedCategory != null){
            HashMap<String, String> data = new HashMap<>();
            data.put("category", selectedCategory);

            HttpClient.put(this, "/users/me", data, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    startMainActivity();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, error.toString());
                }
            });
        }else{
            startMainActivity();
        }
    }

    private void startMainActivity(){
        Intent intent = new Intent(SignActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private class SessionCallback implements ISessionCallback {
        @Override
        public void onSessionOpened() {
            //로딩바
            ImageView signinImg = findViewById(R.id.signin_img);
            signinImg.setVisibility(View.GONE);
            LottieAnimationView animationView = findViewById(R.id.lottie_loading);
            animationView.setAnimation("19451-blue-preloader.json");
            animationView.loop(true);
            animationView.playAnimation();
            RelativeLayout loading_layout = findViewById(R.id.lottie_loading_layout3);
            loading_layout.setVisibility(VISIBLE);

            LinearLayout sub_text = findViewById(R.id.signin_subtext);
            sub_text.setVisibility(View.INVISIBLE);
            TextView main_text = findViewById(R.id.signin_mainext);
            main_text.setText("인증 하고 있습니다\n잠시만 기다려 주세요");


            String accessToken = Session.getCurrentSession().getTokenInfo().getAccessToken();
            signIn(accessToken);
        }

        @Override
        public void onSessionOpenFailed(KakaoException exception) {
            if(exception != null) {
                Logger.e(exception);
            }
        }
    }
}

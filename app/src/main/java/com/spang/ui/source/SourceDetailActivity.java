package com.spang.ui.source;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.nex3z.notificationbadge.NotificationBadge;
import com.spang.ApplicationActivity;
import com.spang.R;

public class SourceDetailActivity extends ApplicationActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_source);

        //바스켓
        ImageView basket_icon = findViewById(R.id.basket_icon);
        NotificationBadge mBadge = (NotificationBadge) findViewById(R.id.badge);
        mBadge.setNumber(3);
        mBadge.setTextSize(28);



        //초스피드 구매대행 버튼
        Button buttonShow = findViewById(R.id.BuyButton);
        buttonShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final BottomSheetDialog buttomSheetDialog = new BottomSheetDialog(SourceDetailActivity.this, R.style.BottomSheetDialogTheme);
                View bottomSheetView = getLayoutInflater().from(getApplicationContext()).inflate(R.layout.bottom_sheet, (LinearLayout) findViewById(R.id.bottom_sheet));
                //닫기버튼
                bottomSheetView.findViewById(R.id.closeBtn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        buttomSheetDialog.dismiss();
                    }
                });


                buttomSheetDialog.setContentView(bottomSheetView);
                buttomSheetDialog.show();
            }
        });




    }
}

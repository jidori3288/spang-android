package com.spang.ui.today;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.spang.ApplicationActivity;
import com.spang.Communication.Communication;
import com.spang.Communication.PreferenceManager;
import com.spang.R;
import com.spang.ui.filter.Filter2_category;
import com.spang.ui.filter.Filter2_growth;
import com.spang.ui.filter.Filter2_makecountry;
import com.spang.ui.filter.Filter2_price;
import com.spang.ui.filter.Filter2_revenue;
import com.spang.ui.filter.Filter2_revenue12m;
import com.spang.ui.filter.Filter2_revenue3m;
import com.spang.ui.filter.Filter2_revenue6m;
import com.spang.ui.filter.Filter2_season;

public class Filter1_today extends ApplicationActivity {

    PreferenceManager pm = new PreferenceManager();
    Communication com = new Communication();

    @Override
    protected void onCreate(Bundle savedInstanceState){
        String TAG = "FILTER1";

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter1_today);


        //카테고리 클릭시
        RelativeLayout category_layout = findViewById(R.id.filter_category);
        category_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Filter2_category_today.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

        ImageView filter_out  =findViewById(R.id.filter_out);
        filter_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Button adjust_btn = findViewById(R.id.adjust);
        adjust_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //필터전송
                //Intent intent = new Intent(getApplicationContext(), ProductdbActivity.class);
                //intent.putExtra("fromFilter","fromFilter");
                //startActivity(intent);
                //pm.setString(getApplicationContext(),"myproductdb:adjust", "1");

                pm.setString(getApplicationContext(),"today:adjust", "1");
                finish();
            }
        });



        Button initialize_btn = findViewById(R.id.initialize);
        initialize_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pm.removeSpecialKey(getApplicationContext(), "today-filter-cat");
                lengNumDisplay("today-filter-cat", R.id.f1_category_num);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        lengNumDisplay("today-filter-cat", R.id.f1_category_num);

    }

    @Override
    public void finish(){
        super.finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }


    public void lengNumDisplay(String filtername, int id) {
        int selectedCategory = pm.getSpecialKeyLeng(getApplicationContext(),filtername);
        TextView catNum = findViewById(id);
        if(selectedCategory == 0){
            catNum.setVisibility(View.INVISIBLE);
        }else {
            catNum.setText(String.valueOf(selectedCategory));
            catNum.setVisibility(View.VISIBLE);
        }
    }




}

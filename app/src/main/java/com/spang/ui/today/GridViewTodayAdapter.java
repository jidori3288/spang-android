package com.spang.ui.today;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.R;
import com.spang.constants.Constant;
import com.spang.ui.productdb.Product;
import com.spang.ui.today2.CardActivityModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class GridViewTodayAdapter extends ArrayAdapter<Product> {
    String TAG = "hell";
    Communication com = new Communication();

    public List<Product> product_list;


    public GridViewTodayAdapter(Context context, int resource, List<Product> objects) {
        super(context, resource, objects);
        this.product_list = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(null == v) {
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.grid_item_today, null);
        }
        Product product = getItem(position);
        ImageView img = (ImageView) v.findViewById(R.id.image);
        TextView title = (TextView) v.findViewById(R.id.title);
        TextView price =(TextView) v.findViewById(R.id.price);
        TextView category  = (TextView) v.findViewById(R.id.category);

        TextView revenue = (TextView) v.findViewById(R.id.revenueTotal);
        //TextView growth = (TextView) v.findViewById(R.id.growth);
        TextView season = (TextView) v.findViewById(R.id.season);
        TextView card_point = (TextView)v.findViewById(R.id.card_point);
        TextView hashtag = (TextView) v.findViewById(R.id.card_hashtag);
        ImageView card_save = (ImageView) v.findViewById(R.id.save_btn);
        TextView save_txt = (TextView) v.findViewById(R.id.save_txt);
        ImageView card_store = (ImageView) v.findViewById(R.id.store_btn);
        TextView store_txt = (TextView) v.findViewById(R.id.card_point);

        //JSONObject obj = product.getData();
        JSONObject obj =product_list.get(position).getData();



        //저장버튼 클릭시 이벤트처리
        //saveClickListener(getContext(), v, position);

        //스토어 버튼 클릭시 이벤트 처리
        //storeClickListener(getContext(), v, position);

        try {

            if(position < 50) {
                Glide.with(getContext()).load(obj.get("image_url")).thumbnail(/*sizeMultiplier=*/ 0.5f).diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH).into(img);
            }
            else {
                Glide.with(getContext()).load(obj.get("image_url")).thumbnail(/*sizeMultiplier=*/ 0.5f).diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.LOW).into(img);
            }
            title.setText((String)obj.get("타이틀"));
            price.setText((String)obj.get("price_short"));
            category.setText((String)obj.get("category"));
            revenue.setText("약 " + (String)obj.get("누적매출액_쇼트2"));
            //growth.setText((String)obj.get("월평균성장률"));

            //카드포인트
            String c_v = String.valueOf(obj.get("card_value"));
            if((int)obj.get("card_value") <= 0) {
                card_point.setText("무료");
            }
            else {
                card_point.setText(c_v + "P");
            }

            season.setText((String)obj.get("계절"));
            //해시태그
            String hashtag_str = (String)obj.get("hashtag");
            String[] hashtag_arr = hashtag_str.split(",");
            String hashtag_pick = "";
            for(int t=0; t <hashtag_arr.length; t++){
                if(t < 3){
                    hashtag_pick += hashtag_arr[t];
                    hashtag_pick += " ";
                }
            }
            hashtag.setText(hashtag_pick);



            try {

                /*
                //저장된 아이템일 경우
                String is_save = (String) obj.get("is_save");
                if (is_save.equals("1")) {
                    card_save.setColorFilter(ContextCompat.getColor(getContext(), R.color.naver), android.graphics.PorterDuff.Mode.MULTIPLY);
                    save_txt.setVisibility(View.VISIBLE);
                    save_txt.setText("저장됨");
                }


                //모의판매 중인 아이템일 경우
                String is_store = (String) obj.get("is_store");
                if (is_store.equals("1")) {
                    card_store.setColorFilter(ContextCompat.getColor(getContext(), R.color.naver), android.graphics.PorterDuff.Mode.MULTIPLY);
                    store_txt.setTextColor(ContextCompat.getColor(getContext(), R.color.naver));

                }

                 */






            }catch(Exception ex){

            }

        }catch(Exception ex){

        }
        return v;
    }


    public void saveClickListener (Context context, View view,  int position) {


        try {

            JSONObject obj = product_list.get(position).getData();
            //Product product = getItem(position);
            //JSONObject obj = product.getData();

            String is_save = (String) obj.get("is_save");


            ImageView card_save = view.findViewById(R.id.save_btn);
            TextView save_txt = view.findViewById(R.id.save_txt);


            card_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {


                        if (is_save.equals("0")) {
                            animateHeart(card_save);
                            card_save.setColorFilter(ContextCompat.getColor(context, R.color.naver), android.graphics.PorterDuff.Mode.MULTIPLY);
                            try {
                                obj.put("is_save", "1");
                                save_txt.setVisibility(View.VISIBLE);

                                //해당 포지션의 데이터에 저장 시켜주기
                                Product pd = product_list.get(position);
                                pd.setData(obj);
                                product_list.set(position, pd);
                                notifyDataSetChanged();

                            } catch (Exception ex) {
                                //
                            }
                            HashMap hm = new HashMap();
                            try {
                                hm.put("product_no", obj.get("상품번호"));
                                com.sendOptions(context, Constant.BASE_URL + "/app/save", hm, new Callback() {
                                    @Override
                                    public void callbackString(String str) {

                                    }

                                    @Override
                                    public void callbackInteger(int integer) {

                                    }

                                    @Override
                                    public void callbackJSONArray(JSONArray array) {


                                    }
                                });
                            } catch (Exception ex) {

                            }
                        }

                        if (is_save.equals("1")) {

                            animateHeart(card_save);
                            card_save.setColorFilter(ContextCompat.getColor(context, R.color.cardview_shadow_start_color), android.graphics.PorterDuff.Mode.MULTIPLY);
                            try {
                                obj.put("is_save", "0");
                                save_txt.setVisibility(View.INVISIBLE);

                                //해당 포지션의 데이터에 저장 시켜주기
                                Product pd = product_list.get(position);
                                pd.setData(obj);
                                product_list.set(position, pd);
                                notifyDataSetChanged();

                            } catch (Exception ex) {
                                //
                            }
                            HashMap hm = new HashMap();
                            try {
                                hm.put("product_no", obj.get("상품번호"));
                                com.sendOptions(context, Constant.BASE_URL + "/app/save-remove", hm, new Callback() {
                                    @Override
                                    public void callbackString(String str) {

                                    }

                                    @Override
                                    public void callbackInteger(int integer) {

                                    }

                                    @Override
                                    public void callbackJSONArray(JSONArray array) {


                                    }
                                });
                            } catch (Exception ex) {

                            }
                        }


                    } catch (Exception ex) {

                    }
                }


            });
        }catch(Exception ee){

        }

    }

    public void storeClickListener (Context context, View view,  int position) {


        ImageView card_store = view.findViewById(R.id.store_btn);
        TextView card_point = view.findViewById(R.id.card_point);

        ImageView card_save = view.findViewById(R.id.save_btn);
        TextView save_txt = view.findViewById(R.id.save_txt);


        card_store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                 Product product = getItem(position);
                JSONObject obj = product.getData();
                String is_store = (String) obj.get("is_store");


                if(is_store.equals("0")) {

                    //스토어
                    animateHeart(card_store);
                    card_store.setColorFilter(ContextCompat.getColor(context, R.color.naver), android.graphics.PorterDuff.Mode.MULTIPLY);
                    card_point.setTextColor(ContextCompat.getColor(context, R.color.naver));

                    //세이브
                    animateHeart(card_save);
                    card_save.setColorFilter(ContextCompat.getColor(context, R.color.naver), android.graphics.PorterDuff.Mode.MULTIPLY);
                    save_txt.setVisibility(View.VISIBLE);


                    try {
                        obj.put("is_save", "1");
                        obj.put("is_store", "1");
                        Product pd = product_list.get(position);
                        pd.setData(obj);
                        product_list.set(position, pd);
                        notifyDataSetChanged();
                    }catch(Exception eee){

                    }
                    HashMap hm = new HashMap();
                    try {
                        hm.put("product_no", obj.get("상품번호"));
                        com.sendOptions(context, Constant.BASE_URL + "/app/store", hm, new Callback() {
                            @Override
                            public void callbackString(String str) {

                            }

                            @Override
                            public void callbackInteger(int integer) {

                            }

                            @Override
                            public void callbackJSONArray(JSONArray array) {




                            }
                        });
                    } catch (Exception ex) {

                    }
                }


                    if(is_store.equals("1")) {

                        animateHeart(card_store);
                        card_store.setColorFilter(ContextCompat.getColor(context, R.color.cardview_shadow_start_color), android.graphics.PorterDuff.Mode.MULTIPLY);
                        card_point.setTextColor(ContextCompat.getColor(context, R.color.cardview_shadow_start_color));
                        try {
                            obj.put("is_store", "0");
                            Product pd = product_list.get(position);
                            pd.setData(obj);
                            product_list.set(position, pd);
                            notifyDataSetChanged();
                        }catch(Exception ee){

                        }
                        //스토어
                        HashMap hm = new HashMap();
                        try {
                            hm.put("product_no", obj.get("상품번호"));
                            com.sendOptions(context, Constant.BASE_URL + "/app/store-remove", hm, new Callback() {
                                @Override
                                public void callbackString(String str) {

                                }

                                @Override
                                public void callbackInteger(int integer) {
                                    }

                                @Override
                                public void callbackJSONArray(JSONArray array) {


                                }
                            });
                        } catch (Exception ex) {

                        }
                    }

                }catch (Exception ee){

                }
            }

        });


    }

    public void animateHeart(final ImageView view) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        prepareAnimation(scaleAnimation);

        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        //prepareAnimation(alphaAnimation);

        AnimationSet animation = new AnimationSet(true);
        animation.addAnimation(alphaAnimation);
        //animation.addAnimation(scaleAnimation);
        animation.setDuration(600);

        view.startAnimation(animation);

    }

    private Animation prepareAnimation(Animation animation){
        animation.setRepeatCount(1);
        animation.setRepeatMode(Animation.REVERSE);
        return animation;
    }
}

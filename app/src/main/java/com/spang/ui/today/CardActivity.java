package com.spang.ui.today;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.nex3z.notificationbadge.NotificationBadge;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.spang.ApplicationFragment;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.Communication.PreferenceManager;
import com.spang.MainActivity;
import com.spang.R;
import com.spang.constants.Constant;
import com.spang.constants.SharedPreferencesConstant;
import com.spang.models.UserPoint;
import com.spang.ui.analysis.CardDetailActivity;
import com.spang.ui.filter.Filter1;
import com.spang.ui.joker.JokerActivity;
import com.spang.ui.payment.PaymentActivity;
import com.spang.ui.productdb.GridViewAdapter;
import com.spang.ui.productdb.ListViewAdapter;
import com.spang.ui.productdb.Product;
import com.spang.ui.setting.CategoryResetActivity;
import com.spang.ui.sign.SignActivity;
import com.spang.utils.HttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.spang.constants.SharedPreferencesConstant.SELECTED_CATEGORY;

public class CardActivity extends ApplicationFragment {
    String TAG = "hell";

    Communication com = new Communication();
    PreferenceManager pm = new PreferenceManager();

    int data_counter =0;
    int stop_repeat = 0;
    int card_leng = 5;
    int repeat_count = 0;

    Context context;
    private ViewStub stubGrid;
    private GridView gridView;

    private List<Product> productList;
    private GridViewTodayAdapter gridViewAdapter;
    int getVisibleItem_gv = 0;
    private SlidingUpPanelLayout mLayout;
    TextView mBadge;
    View root;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        root = inflater.inflate(R.layout.fragment_today, container, false);

        context = getContext();
        stubGrid = (ViewStub) root.findViewById(R.id.stub_grid_today);
        stubGrid.inflate();

        gridView = (GridView) root.findViewById(R.id.mygridview);
        productList = new ArrayList<>();

        //필터뱃지
         mBadge = (TextView) root.findViewById(R.id.badge);

        //로딩 시작
        loadingImage(root, 1);

        //카테고리 갯수
        setCategory();

        //초기
        getData(root , "1");


        mLayout = (SlidingUpPanelLayout) root.findViewById(R.id.sliding_layout);
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

        //필터 초기화
        filterClickListener(root);


        return root;
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //setPoint();
    }


    /*
    private void setInformation(){
        if(PreferenceManager.getBoolean(getContext(), SharedPreferencesConstant.RANDOM_ITEM_INFORMATION) != true){
            showInfo();
            PreferenceManager.setBoolean(getContext(), SharedPreferencesConstant.RANDOM_ITEM_INFORMATION, true);
        }

        layoutInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInfo();
            }
        });
    }

     */

    private void setCategory(){
        int category_selected_num = pm.getSpecialKeyLeng(context, "today-filter");
        if (category_selected_num != 0){
            mBadge.setVisibility(VISIBLE);
        } else {
            mBadge.setVisibility(INVISIBLE);
        }
    }

    public void filterClickListener(View root){
        ImageView filter_btn = root.findViewById(R.id.filter_btn);
        filter_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Filter1_today.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

    }


    private void setPoint(){
        HttpClient.get(getContext(), "/user_points/me", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                UserPoint userPoint = gson.fromJson(response.toString(), UserPoint.class);
                Integer point = userPoint.getPoint();
                PreferenceManager.setInt(getContext(), SharedPreferencesConstant.REMAIN_POINT, point);

                //포인트 미노출로 주석처리
                //pointText.setText(point.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
            }
        });
    }


    public void loadingImage(View root, int onoff){
        final LottieAnimationView animationView = (LottieAnimationView) root.findViewById(R.id.lottie_loading);
        RelativeLayout animation_layout = root.findViewById(R.id.lottie_loading_layout);
        if(onoff == 1) {
            //로딩 시작
            animation_layout.setVisibility(VISIBLE);
            animationView.setVisibility(VISIBLE);
            animationView.setAnimation("19451-blue-preloader.json");
            animationView.loop(true);
            animationView.playAnimation();
        }
        else {
            animation_layout.setVisibility(GONE);
            animationView.setVisibility(GONE);
        }
    }

    public void showInfo(){
        //다이얼로그 객체 생성
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        //속성 지정
        builder.setTitle("추천 아이템");
        builder.setMessage("- 카드를 누르면 스마트스토어 추천 아이템이 제시됩니다\n- 조회한 모든 아이템은 아이템DB에 수집됩니다\n- 모든 데이터는 예측 알고리즘을 바탕으로 한 추정치입니다\n- 조회할 때 포인트가 차감됩니다");
        //아이콘
        builder.setIcon(android.R.drawable.ic_menu_help);

        //예 버튼 눌렀을 때
        builder.setPositiveButton("확인했습니다", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //텍스트 뷰 객체를 넣어줌..
                // Snackbar.make(textView ,"확인되었습니다",Snackbar.LENGTH_SHORT).show();
            }
        });

        //만들어주기
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView msg = (TextView)dialog.findViewById(android.R.id.message);
        msg.setTextSize(12);
    }

    public void alert(){

        //다이얼로그 객체 생성
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        //속성 지정
        builder.setTitle("잠깐");
        builder.setMessage("너무 많은 카드를 로딩하였습니다\n초기화 해주세요");
        //아이콘
        builder.setIcon(android.R.drawable.ic_menu_help);


        //예 버튼 눌렀을 때
        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //텍스트 뷰 객체를 넣어줌..
                // Snackbar.make(textView ,"확인되었습니다",Snackbar.LENGTH_SHORT).show();
            }
        });

        //만들어주기
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView msg = (TextView)dialog.findViewById(android.R.id.message);
        msg.setTextSize(12);
    }

    public void getCardCount(View root) {
        //데이터 불러오기(임시)
        String db_url = Constant.BASE_URL + "/app/card_count";
        HashMap<String, String> hm = new HashMap<>();

        com.sendOptions(getContext(), db_url, hm, new Callback() {
            @Override
            public void callbackString(String str) {

            }

            @Override
            public void callbackInteger(int integer) {
//                ((TextView)getView().findViewById(R.id.card_count)).setText("수집아이템 " + String.valueOf(integer) + "개");
            }

            @Override
            public void callbackJSONArray(JSONArray array) {

            }
        });
    }


    public void getData(View roots,String is_open) {

        if(stop_repeat == (productList.size()) ) {

            root = roots;

            stop_repeat+= card_leng;

            repeat_count++;

            loadingImage(root,1);

            Log.v("volley","request");

            //데이터 불러오기(임시)
            String db_url = Constant.BASE_URL + "/app/today2";

            HashMap<String, String> hm = pm.getTodayFilterPref(getContext());
            hm.put("is_open", is_open);
            hm.put("data_counter", String.valueOf(data_counter));
            hm.put("card_leng", String.valueOf(card_leng));


            RelativeLayout noitem_layout = root.findViewById(R.id.noitem_text);

            com.sendOptions(getContext(), db_url, hm, new Callback() {
                @Override
                public void callbackString(String str) {

                }

                @Override
                public void callbackInteger(int integer) {

                }

                @Override
                public void callbackJSONArray(JSONArray array) {
                    if (array.length() > 0) {

                        noitem_layout.setVisibility(GONE);

                        Log.v("volley","response");

                        loadingImage(root,0);

                        data_counter += card_leng;

                        //리스츠 투가하기
                        for (int i = 0; i < array.length(); i++) {
                            try {
                                String obj = (String) array.get(i);
                                productList.add(new Product(new JSONObject(obj)));
                            } catch (Exception ex) {

                            }

                        } //


                        //어댑터연결
                        if (getActivity()!=null) {
                            gridViewAdapter = new GridViewTodayAdapter(getActivity(), R.layout.grid_item_today, productList);
                            gridView.setAdapter(gridViewAdapter);
                            if (productList != null) {
                                gridViewAdapter.notifyDataSetChanged();
                            }
                        }


                        //그리드뷰 셋팅
                        gridView.setOnItemClickListener(onItemClick);


                        gridView.setSelection(productList.size()-card_leng-getVisibleItem_gv +2);


                        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {

                            private int visibleThreshold = card_leng;
                            private int currentPage = 0;
                            private int previousTotal = 0;
                            private boolean loading = true;

                            @Override
                            public void onScrollStateChanged(AbsListView view, int scrollState) {

                            }

                            @Override
                            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                                if (loading) {
                                    if (totalItemCount > previousTotal) {
                                        loading = false;
                                        previousTotal = totalItemCount;
                                        currentPage++;
                                    }
                                }
                                if (!loading && ( (firstVisibleItem+visibleItemCount) == totalItemCount)) {
                                    getVisibleItem_gv = visibleItemCount;
                                    getData(root, "0");
                                    loading = true;
                                }
                            }
                        });


                    } else {
                        noitem_layout.setVisibility(VISIBLE);
                        loadingImage(root, 0);
                    }
                }
            });
        }
    }




    AdapterView.OnItemClickListener onItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            try {
                String data =  productList.get(position).getData().toString();
                pm.setString(getContext(), "large-data", data);
                Intent intent = new Intent(getContext(), CardDetailActivity.class);
                startActivity(intent);
            }catch(Exception ex2) {
                //
            }

        }
    };

    //상세보기에서 다시 돌아온경우
    @Override
    public void onResume() {

        super.onResume();
        String filter_changed = pm.getString(context, "today:adjust");

        if(filter_changed.equals("1")) {
            pm.setString(context,"today:adjust","0");

            loadingImage(root, 0);

            //프래그먼트 리프레시
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.detach(this).attach(this).commit();

            //초기화
            stop_repeat = 0;
            productList = new ArrayList<>();
            getData(root, "1");
        }
    }
}

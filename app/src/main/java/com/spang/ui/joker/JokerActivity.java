package com.spang.ui.joker;

import android.animation.ArgbEvaluator;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.viewpager.widget.ViewPager;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.spang.ApplicationActivity;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.Communication.PreferenceManager;
import com.spang.R;
import com.spang.constants.Constant;
import com.spang.constants.SharedPreferencesConstant;
import com.spang.models.UserPoint;
import com.spang.utils.HttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.spang.constants.SharedPreferencesConstant.SELECTED_CATEGORY;

public class JokerActivity extends ApplicationActivity  implements JokerActivityAdapter.OnSharedListener{
    String TAG = "hell";
    ViewPager viewPager;
    JokerActivityAdapter cardAdapter;
    List<JokerActivityModel> cardActivityModels;
    Integer[] colors = null;
    ArgbEvaluator argbEvaluator = new ArgbEvaluator();
    Communication com = new Communication();

    PreferenceManager pm = new PreferenceManager();

    int data_counter =0;

    int stop_repeat = 0;

    int card_leng = 5;

    private TextView pointText;
    private TextView categoryText;
    private LinearLayout layoutInformation;

    @Override
    public void ShareThis() {
        setPoint();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String TAG = "JOKER";
        setContentView(R.layout.fragment_todaycard2);

        //pointText = findViewById(R.id.remain_point);
        categoryText = findViewById(R.id.category);
        layoutInformation = findViewById(R.id.layout_information);

        setInformation();
        setCategory();
        //setPoint();

        //데이터 모델 어레이
        cardActivityModels = new ArrayList<>();

        //이미 오픈된 카드 =1 , 미오픈카드= 0
        String is_open = "0";
        getData(is_open);


    }
    private void setInformation(){
        if(PreferenceManager.getBoolean(this, SharedPreferencesConstant.JOKER_ITEM_INFORMATION) != true){
            showInfo();
            PreferenceManager.setBoolean(this, SharedPreferencesConstant.JOKER_ITEM_INFORMATION, true);
        }
        layoutInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInfo();
            }
        });
    }

    private void setCategory(){
        String category = PreferenceManager.getString(this, SELECTED_CATEGORY);
        if (category != null){
            categoryText.setText(category);
        }else{
            categoryText.setText("전체 카테고리");
        }
    }

    private void setPoint(){
        HttpClient.get(this, "/user_points/me", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                UserPoint userPoint = gson.fromJson(response.toString(), UserPoint.class);
                Integer point = userPoint.getPoint();
                //PreferenceManager.setInt(JokerActivity.this, SharedPreferencesConstant.REMAIN_POINT, point);

                //pointText.setText(point.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
            }
        });
    }

    public void showInfo(){

        //다이얼로그 객체 생성
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //속성 지정
        builder.setTitle("조커 아이템");
        builder.setMessage("- 알고리즘이 추출한 상위 1% 대박 아이템입니다\n- 조커 아이템은 '즉시매출' 가능성이 매우 높습니다\n- (주의) 랜덤이며 일반아이템이 나올 수도 있습니다");
        //아이콘
        builder.setIcon(android.R.drawable.ic_menu_help);

        //예 버튼 눌렀을 때
        builder.setPositiveButton("확인했습니다", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //텍스트 뷰 객체를 넣어줌..
               // Snackbar.make(textView ,"확인되었습니다",Snackbar.LENGTH_SHORT).show();
            }
        });

        //만들어주기
        AlertDialog dialog = builder.create();
        dialog.show();

        TextView msg = (TextView)dialog.findViewById(android.R.id.message);
        msg.setTextSize(12);
    }

    public void getData(String is_open) {
        if(stop_repeat == (cardActivityModels.size()) ) {

            stop_repeat+= card_leng;

            ((TextView)findViewById(R.id.card_bread)).setVisibility(GONE);

            //데이터 불러오기(임시)
            String db_url = Constant.BASE_URL + "/app/joker";
            HashMap<String, String> hm = new HashMap<>();
            hm.put("is_open", is_open);
            hm.put("data_counter", String.valueOf(data_counter));
            hm.put("card_leng", String.valueOf(card_leng));

            //로딩 시작
            final LottieAnimationView animationView = (LottieAnimationView) findViewById(R.id.lottie_loading2);
            animationView.setVisibility(VISIBLE);
            animationView.setAnimation("19451-blue-preloader.json");
            animationView.loop(true);
            animationView.playAnimation();


            com.sendOptions(getApplicationContext(), db_url, hm, new Callback() {
                @Override
                public void callbackString(String str) {

                }

                @Override
                public void callbackInteger(int integer) {

                }

                @Override
                public void callbackJSONArray(JSONArray array) {
                    if (array.length() > 0) {
                        Log.v("volley","response");

                        animationView.setVisibility(GONE);

                        ((TextView)findViewById(R.id.card_bread)).setVisibility(VISIBLE);

                        data_counter += card_leng;

                        findViewById(R.id.no_item_text).setVisibility(GONE);

                        TextView bread = findViewById(R.id.card_bread);

                        int current_position = cardActivityModels.size() - 1;


                        for (int i = 0; i < array.length(); i++) {
                            try {
                                cardActivityModels.add(new JokerActivityModel(new JSONObject((String) array.get(i))));
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }


                        cardAdapter = new JokerActivityAdapter(cardActivityModels, getApplicationContext());
                        cardAdapter.setOnSharedListener(JokerActivity.this);
                        viewPager = findViewById(R.id.viewPager);
                        viewPager.setAdapter(cardAdapter);
                        viewPager.setPadding(130, 0, 130, 0);
                        viewPager.getAdapter().notifyDataSetChanged();
                        viewPager.setCurrentItem(current_position);

                        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                super.onPageScrolled(position, positionOffset, positionOffsetPixels);

                                bread.setText((position + 1) + "/" + cardActivityModels.size());

                                if (position == cardAdapter.getCount() - 1) {
                                    getData("0");
                                }
                            }
                        });

                        viewPager.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                return false;
                            }
                        });


                    }

                }
            });
        }

    }



    //상세보기에서 다시 돌아온경우
    @Override
    public void onResume() {
        super.onResume();


    }

}

package com.spang.ui.joker;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.Communication.PreferenceManager;
import com.spang.R;
import com.spang.constants.Constant;
import com.spang.ui.analysis.CardDetailActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class JokerActivityAdapter extends PagerAdapter {
    String TAG = "hell";
    public List<JokerActivityModel> cardActivityModels;
    public LayoutInflater layoutInflater;
    public Context context;
    Communication com = new Communication();
    OnSharedListener mCallback;

    public MediaPlayer mp;
    PreferenceManager pm = new PreferenceManager();

    public JokerActivityAdapter(List<JokerActivityModel> cardActivityModels, Context context) {
        this.cardActivityModels = cardActivityModels;
        this.context = context;
        this.mp= MediaPlayer.create(context, R.raw.cardslide1);
    }

    public void setOnSharedListener(OnSharedListener mCallback) {
        this.mCallback = mCallback;
    }

    public interface OnSharedListener {
        public void ShareThis();
    }


    @Override
    public int getItemPosition(Object object) {

        return POSITION_NONE;
    }



    @Override
    public int getCount() {
        return cardActivityModels.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {


        View resultView = null;

        layoutInflater = LayoutInflater.from(context);



        //넘겨받은 카드데이터 오브젝트
        JSONObject card_data = cardActivityModels.get(position).getData();


        try {
            //카트 템플릿 먼저 확인
            String template = (String) card_data.get("template");

            //오픈여부
            final String is_open = String.valueOf(card_data.get("is_open"));

            //카드포인트
            final String card_value = String.valueOf(card_data.get("card_value"));

            //카드디자인
            final String card_design = "1";


            //베이직템플릿

            if (template.equals("basic")) {
                final View view = layoutInflater.inflate(R.layout.card_joker, container, false);

                ImageView imageView = view.findViewById(R.id.image);
                final TextView title = view.findViewById(R.id.title);
                final TextView category = view.findViewById(R.id.category);
                final TextView price = view.findViewById(R.id.price);
                final TextView revenue_total = view.findViewById(R.id.revenueTotal);
                //final TextView growth = view.findViewById(R.id.growth);
                final TextView season = view.findViewById(R.id.season);
                final LottieAnimationView animationView = (LottieAnimationView) view.findViewById(R.id.lottie_cardimage);
                final TextView card_text = view.findViewById(R.id.card_text);
                //final TextView card_desc = view.findViewById(R.id.card_desc);
                final TextView card_point = view.findViewById(R.id.card_point);
                final RelativeLayout card_cover = view.findViewById(R.id.card_cover);
                final ImageView card_cover_image = view.findViewById(R.id.card_cover_image);



                //상품명 (전광판)
                title.setText((String) card_data.get("타이틀"));
                title.setSelected(true);

                //카테고리
                category.setText((String) card_data.get("category"));

                //가격
                price.setText((String) card_data.get("price_short"));

                //매출(전체)
                revenue_total.setText((String) card_data.get("누적매출액_쇼트"));

                //성장률
                //growth.setText((String) String.valueOf(card_data.get("월평균성장률")));

                //계절
                season.setText((String) String.valueOf(card_data.get("계절")));


                //카드포인트
                card_point.setText(card_value);


                //카드 안깐경우 위장시키기
                if(is_open.equals("0")){

                    //설명부분 안보이게
                    LinearLayout item_info = view.findViewById(R.id.item_info);
                    item_info.setVisibility(View.INVISIBLE);
                    imageView.setVisibility(GONE);
                    card_cover.setVisibility(VISIBLE);


                    //카드 등급별 이미지
                    switch (card_design) {
                        case "1":
                            //animationView.setAnimation("4746-welcome-screen.json");
                            //animationView.setAnimation("2086-wow.json");
                            //animationView.playAnimation();
                            card_cover_image.setBackgroundResource(R.drawable.joker);
                            break;

                    }
                }
                if(is_open.equals("1")){

                    //이미지
                    imageView.setVisibility(VISIBLE);
                    card_cover.setVisibility(GONE);
                    Glide.with(context).load(card_data.get("image_url")).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
                }

                //클릭이벤트 등록
                imageClickListener(view, position);
                //saveClickListener(context,view,position);
                //deleteClickListener(context,view,position);
                container.addView(view, 0);
                resultView = view;
            }

        }catch(Exception ex){
            ex.printStackTrace();
        }


        return resultView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }



    public void deleteItem (Context context,  int position) {

        cardActivityModels.remove(position);
        notifyDataSetChanged();

    }

/*
    public void deleteClickListener (Context context, View view,  int position) {

        LinearLayout card_delete = view.findViewById(R.id.card_delete);
        JokerActivityModel card_model =  (JokerActivityModel) cardActivityModels.get(position);
        JSONObject card_data = card_model.getData();

        Communication com = new Communication();

        card_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap hm = new HashMap();
                try {
                    hm.put("product_no", card_data.get("상품번호"));
                    com.sendOptions(context, Constant.BASE_URL + "/app/delete", hm, new Callback() {
                        @Override
                        public void callbackString(String str) {

                        }

                        @Override
                        public void callbackInteger(int integer) {

                        }

                        @Override
                        public void callbackJSONArray(JSONArray array) {

                            cardActivityModels.remove(position);
                            notifyDataSetChanged();
                            Toast myToast = Toast.makeText(context, "삭제완료", Toast.LENGTH_SHORT);
                            myToast.show();
                            //success

                        }
                    });
                }catch(Exception ex){

                }
            }
        });
    }


    public void saveClickListener (Context context, View view,  int position) {

        LinearLayout card_save = view.findViewById(R.id.card_save);
        JokerActivityModel card_model =  (JokerActivityModel) cardActivityModels.get(position);
        JSONObject card_data = card_model.getData();

        card_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap hm = new HashMap();
                try {
                    hm.put("product_no", card_data.get("상품번호"));
                    com.sendOptions(context, Constant.BASE_URL + "/app/save", hm, new Callback() {
                        @Override
                        public void callbackString(String str) {

                        }

                        @Override
                        public void callbackInteger(int integer) {

                        }

                        @Override
                        public void callbackJSONArray(JSONArray array) {

                            cardActivityModels.remove(position);
                            notifyDataSetChanged();
                            Toast myToast = Toast.makeText(context, "저장완료", Toast.LENGTH_SHORT);
                            myToast.show();


                        }
                    });
                }catch(Exception ex){

                }
            }
        });
    }
*/



    public void imageClickListener (View view,  int position){
        try {


            JokerActivityModel card_model =  (JokerActivityModel) cardActivityModels.get(position);
            JSONObject card_data = card_model.getData();

            LottieAnimationView lottieView = view.findViewById(R.id.lottie_cardimage);
            ImageView cardCoverImage = view.findViewById(R.id.card_cover_image);
            RelativeLayout card_cover = view.findViewById(R.id.card_cover);
            ImageView imageView = view.findViewById(R.id.image);
            CardView cardView = view.findViewById(R.id.item_card);
            RelativeLayout card_container = view.findViewById(R.id.card_container);

            //오픈여부


            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        String is_open = String.valueOf(card_data.get("is_open"));

                        if(is_open.equals("1")) {

                            /*
                            Intent intent = new Intent(view.getContext(), CardDetailActivity.class);
                            intent.putExtra("data", card_data.toString());
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            view.getContext().startActivity(intent);

                             */

                            Intent intent = new Intent(view.getContext(), CardDetailActivity.class);
                            String data = card_data.toString();
                            pm.setString(context, "large-data", data);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            view.getContext().startActivity(intent);
                        }


                    } catch (Exception ex) {
                        Log.v("hello", ex.toString());
                        ex.printStackTrace();
                    }
                }
            });

            cardCoverImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        String is_open = String.valueOf(card_data.get("is_open"));

                        //로딩 애니메이션
                        card_cover.setVisibility(View.INVISIBLE);
                        final LottieAnimationView animationView = (LottieAnimationView) view.findViewById(R.id.lottie_boxopen);
                        animationView.setVisibility(animationView.VISIBLE);
                        animationView.setAnimation("17626-gift-box-opening.json");
                        animationView.playAnimation();

                        if (is_open.equals("0")) {


                            HashMap hm = new HashMap();
                            try {
                                hm.put("product_no", card_data.get("상품번호"));
                                hm.put("is_joker", "1");
                                com.sendOptions(context, Constant.BASE_URL + "/app/open", hm, new Callback() {
                                    @Override
                                    public void callbackString(String str) {

                                    }

                                    @Override
                                    public void callbackInteger(int integer) {

                                    }

                                    @Override
                                    public void callbackJSONArray(JSONArray array) {

                                        try {
                                            int result_code = (int) array.get(0);
                                            int current_cards = (int) array.get(1);
                                            int current_points = (int) array.get(2);

                                            //포인트가 부족합니다
                                            if (result_code == 0) {
                                                Toast.makeText(context, "포인트가 부족합니다\n하단 더보기>충전하기에서 충전해주세요", Toast.LENGTH_SHORT).show();
                                                animationView.cancelAnimation();
                                                animationView.setVisibility(GONE);
                                                card_cover.setVisibility(VISIBLE);
                                            }

                                            //결제됬습니다.
                                            if (result_code == 1) {


                                                //애니메이션 종료
                                                animationView.setVisibility(GONE);
                                                card_cover.setVisibility(GONE);

                                                mp.start();


                                                final Animation fadeIn = new AlphaAnimation(0, 1);
                                                fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
                                                fadeIn.setDuration(1000);

                                                final Animation fadeOut = new AlphaAnimation(1, 0);
                                                fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
                                                fadeOut.setStartOffset(1000);
                                                fadeOut.setDuration(1000);


                                                AnimationSet animationFF = new AnimationSet(false); //change to false
                                                //animationFF.addAnimation(fadeOut);
                                                animationFF.addAnimation(fadeIn);
                                                view.setAnimation(animationFF);


                                                //정보 보이기 (이미지, 정보)
                                                ImageView imageView = view.findViewById(R.id.image);
                                                imageView.setVisibility(VISIBLE);
                                                try {
                                                    //오픈으로 변경
                                                    card_data.put("is_open", "1");
                                                    cardActivityModels.get(position).setData(card_data);

                                                    //이미지지정
                                                    Glide.with(context).load(card_data.get("image_url")).into(imageView);

                                                } catch (Exception EX) {
                                                    EX.printStackTrace();
                                                }
                                                LinearLayout item_info = view.findViewById(R.id.item_info);
                                                item_info.setVisibility(VISIBLE);

                                                cardActivityModels.get(position).setData(card_data);

                                            }
                                        } catch (Exception ex) {
                                            //ex.printStackTrace();
                                        }

                                    }
                                });
                            } catch (Exception ex) {

                            }
                        }


                    }catch(Exception ee){

                        ee.printStackTrace();
                        //
                    }


                }// end
            });
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }


}

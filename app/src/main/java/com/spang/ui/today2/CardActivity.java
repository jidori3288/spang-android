package com.spang.ui.today2;

import android.animation.ArgbEvaluator;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.viewpager.widget.ViewPager;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.spang.ApplicationFragment;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.Communication.PreferenceManager;
import com.spang.R;
import com.spang.constants.Constant;
import com.spang.constants.SharedPreferencesConstant;
import com.spang.models.UserPoint;
import com.spang.ui.joker.JokerActivity;
import com.spang.utils.HttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.spang.constants.SharedPreferencesConstant.SELECTED_CATEGORY;

public class CardActivity extends ApplicationFragment implements CardActivityAdapter.OnSharedListener {
    String TAG = "hell";
    ViewPager viewPager;
    CardActivityAdapter cardAdapter;
    List<CardActivityModel> cardActivityModels;
    Integer[] colors = null;
    ArgbEvaluator argbEvaluator = new ArgbEvaluator();
    Communication com = new Communication();
    PreferenceManager pm = new PreferenceManager();

    int data_counter =0;

    int stop_repeat = 0;

    int card_leng = 5;

    int repeat_count = 0;

    private TextView pointText;
    private TextView categoryText;
    private LinearLayout layoutJoker;
    private LinearLayout layoutInformation;

    @Override
    public void ShareThis() {
        //setPoint();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_todaycard, container, false);

        //데이터 모델 어레이
        cardActivityModels = new ArrayList<>();

        //pointText = root.findViewById(R.id.remain_point);
        categoryText = root.findViewById(R.id.category);
        layoutJoker = root.findViewById(R.id.layout_joker);
        layoutInformation = root.findViewById(R.id.layout_information);

        setJoker();
        setInformation();
        setCategory();
        //setPoint();

        //이미 오픈된 카드 =1 , 미오픈카드= 0
        String is_open = "1";
        getCardCount(root);
        getData(root, is_open);

        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //setPoint();
    }

    private void setJoker() {
        layoutJoker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), JokerActivity.class);
                intent.putExtra("name","데이터");
                startActivityForResult(intent, 0);
            }
        });
    }

    private void setInformation(){
        if(PreferenceManager.getBoolean(getContext(), SharedPreferencesConstant.RANDOM_ITEM_INFORMATION) != true){
            showInfo();
            PreferenceManager.setBoolean(getContext(), SharedPreferencesConstant.RANDOM_ITEM_INFORMATION, true);
        }

        layoutInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInfo();
            }
        });
    }

    private void setCategory(){
        String category = PreferenceManager.getString(getContext(), SELECTED_CATEGORY);
        if (category != null){
            categoryText.setText(category);
        }else{
            categoryText.setText("전체 카테고리");
        }
    }

    private void setPoint(){
        HttpClient.get(getContext(), "/user_points/me", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                UserPoint userPoint = gson.fromJson(response.toString(), UserPoint.class);
                Integer point = userPoint.getPoint();
                PreferenceManager.setInt(getContext(), SharedPreferencesConstant.REMAIN_POINT, point);

                //포인트 미노출로 주석처리
                //pointText.setText(point.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
            }
        });
    }


    public void loadingImage(View root, int onoff){
        final LottieAnimationView animationView = (LottieAnimationView) root.findViewById(R.id.lottie_loading);
        if(onoff == 1) {
            //로딩 시작
            ((TextView)root.findViewById(R.id.card_bread)).setVisibility(GONE);
            animationView.setVisibility(VISIBLE);
            animationView.setAnimation("19451-blue-preloader.json");
            animationView.loop(true);
            animationView.playAnimation();
        }
        else {
            animationView.setVisibility(GONE);
        }
    }

    public void showInfo(){
        //다이얼로그 객체 생성
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        //속성 지정
        builder.setTitle("추천 아이템");
        builder.setMessage("- 카드를 누르면 스마트스토어 추천 아이템이 제시됩니다\n- 조회한 모든 아이템은 아이템DB에 수집됩니다\n- 모든 데이터는 예측 알고리즘을 바탕으로 한 추정치입니다\n- 조회할 때 포인트가 차감됩니다");
        //아이콘
        builder.setIcon(android.R.drawable.ic_menu_help);

        //예 버튼 눌렀을 때
        builder.setPositiveButton("확인했습니다", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //텍스트 뷰 객체를 넣어줌..
                // Snackbar.make(textView ,"확인되었습니다",Snackbar.LENGTH_SHORT).show();
            }
        });

        //만들어주기
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView msg = (TextView)dialog.findViewById(android.R.id.message);
        msg.setTextSize(12);
    }

    public void alert(){

        //다이얼로그 객체 생성
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        //속성 지정
        builder.setTitle("잠깐");
        builder.setMessage("너무 많은 카드를 로딩하였습니다\n초기화 해주세요");
        //아이콘
        builder.setIcon(android.R.drawable.ic_menu_help);


        //예 버튼 눌렀을 때
        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //텍스트 뷰 객체를 넣어줌..
                // Snackbar.make(textView ,"확인되었습니다",Snackbar.LENGTH_SHORT).show();
            }
        });

        //만들어주기
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView msg = (TextView)dialog.findViewById(android.R.id.message);
        msg.setTextSize(12);
    }

    public void getCardCount(View root) {
        //데이터 불러오기(임시)
        String db_url = Constant.BASE_URL + "/app/card_count";
        HashMap<String, String> hm = new HashMap<>();

        com.sendOptions(getContext(), db_url, hm, new Callback() {
            @Override
            public void callbackString(String str) {

            }

            @Override
            public void callbackInteger(int integer) {
//                ((TextView)getView().findViewById(R.id.card_count)).setText("수집아이템 " + String.valueOf(integer) + "개");
            }

            @Override
            public void callbackJSONArray(JSONArray array) {

            }
        });
    }


    public void getData(View root,String is_open) {

        if(stop_repeat == (cardActivityModels.size()) ) {

            stop_repeat+= card_leng;

            repeat_count++;

            TextView bread = root.findViewById(R.id.card_bread);

            loadingImage(root,1);

            //1000개이상 롤링되지 못하게 제한
            if(repeat_count * card_leng > 1000) {
                alert();
                loadingImage(root,0);
                bread.setText(cardActivityModels.size() + "/" + cardActivityModels.size());
                return;
            }


            Log.v("volley","request");

            //데이터 불러오기(임시)
            String db_url = Constant.BASE_URL + "/app/today";
            HashMap<String, String> hm = new HashMap<>();
            hm.put("is_open", is_open);
            hm.put("data_counter", String.valueOf(data_counter));
            hm.put("card_leng", String.valueOf(card_leng));


            com.sendOptions(getContext(), db_url, hm, new Callback() {
                @Override
                public void callbackString(String str) {

                }

                @Override
                public void callbackInteger(int integer) {

                }

                @Override
                public void callbackJSONArray(JSONArray array) {
                    if (array.length() > 0) {

                        Log.v("volley","response");

                        loadingImage(root,0);

                        ((TextView)root.findViewById(R.id.card_bread)).setVisibility(VISIBLE);

                        data_counter += card_leng;

                        root.findViewById(R.id.no_item_text).setVisibility(GONE);


                        int current_position = cardActivityModels.size() - 1;


                        for (int i = 0; i < array.length(); i++) {
                            try {
                                cardActivityModels.add(new CardActivityModel(new JSONObject((String) array.get(i))));
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }


                        cardAdapter = new CardActivityAdapter(cardActivityModels, getContext());

                        //수집데이터 텍스트 연동부분(어답터에서 콜백받음)
                        cardAdapter.setOnSharedListener(CardActivity.this);
                        viewPager = root.findViewById(R.id.viewPager);
                        viewPager.setAdapter(cardAdapter);
                        viewPager.setPadding(130, 0, 130, 0);
                        viewPager.getAdapter().notifyDataSetChanged();
                        viewPager.setCurrentItem(current_position);

                        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                super.onPageScrolled(position, positionOffset, positionOffsetPixels);

                                bread.setText((position + 1) + "/" + cardActivityModels.size());

                                if (position == cardAdapter.getCount() - 1) {
                                    getData(root, "0");
                                    loadingImage(root,1);
                                }
                            }
                        });
                        viewPager.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                return false;
                            }
                        });
                    }
                }
            });
        }
    }

    //상세보기에서 다시 돌아온경우
    @Override
    public void onResume() {
        super.onResume();
    }
}

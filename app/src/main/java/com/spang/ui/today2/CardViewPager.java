package com.spang.ui.today2;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

public class CardViewPager extends ViewPager {
    String TAG = "hell";
    public CardViewPager(Context context) {
        super(context);
    }

    public CardViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    //카드를 빠르게 넘길때 crash 생기는 viewpager 버그 해결용
    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        Boolean val = false;

        try {

            if (getCurrentItem() == 0 && getChildCount() == 0) {
                return false;
            }
            val = super.onTouchEvent(ev);
        }catch(Exception ex){
            ex.printStackTrace();
        }

        return  val;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (getCurrentItem() == 0 && getChildCount() == 0) {
            return false;
        }

        return super.onInterceptTouchEvent(ev);
    }
}
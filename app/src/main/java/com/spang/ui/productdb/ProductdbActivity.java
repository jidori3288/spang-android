package com.spang.ui.productdb;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.spang.ApplicationFragment;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.Communication.PreferenceManager;
import com.spang.R;
import com.spang.constants.Constant;
import com.spang.ui.analysis.CardDetailActivity;
import com.spang.ui.filter.Filter1;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import static android.content.Context.MODE_PRIVATE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ProductdbActivity extends ApplicationFragment {
    String TAG = "hell";
    private ViewStub stubGrid;
    private ViewStub stubList;
    private ListView listView;
    private GridView gridView;
    private ListViewAdapter listViewAdapter;
    private GridViewAdapter gridViewAdapter;
    private List<Product> productList;
    private int currentViewMode = 0;
    private LinearLayout activity_main;

    static final int VIEW_MODE_LISTVIEW = 0;
    static final int VIEW_MODE_GRIDVIEW = 1;

    private SlidingUpPanelLayout mLayout;
    private RelativeLayout loading_layout;
    public LottieAnimationView animationView;

    TextView product_leng;

    int isCollapsed = 0;

    Communication com = new Communication();
    PreferenceManager pm = new PreferenceManager();

    int stop_repeat = 0;
    int data_counter = 0;
    int card_leng = 10;
    int visibleItem_lv = 0;
    int getVisibleItem_gv = 0;

    TextView no_item_text;
    SharedPreferences sharedPreferences = null;
    Context context;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_productdb, container, false);

        context = getContext();

        sharedPreferences = getContext().getSharedPreferences("ViewMode", MODE_PRIVATE);

        stubList = (ViewStub) root.findViewById(R.id.stub_list);
        stubGrid = (ViewStub) root.findViewById(R.id.stub_grid);

        stubList.inflate();
        stubGrid.inflate();

        listView = (ListView) root.findViewById(R.id.mylistview);
        gridView = (GridView) root.findViewById(R.id.mygridview);

        productList = new ArrayList<>();

        product_leng = root.findViewById(R.id.product_leng);
        activity_main = root.findViewById(R.id.activity_main);

        //로딩 시작
        loading_layout = root.findViewById(R.id.lottie_loading_layout);
        loading_layout.setVisibility(VISIBLE);
        animationView = (LottieAnimationView) root.findViewById(R.id.lottie_loading);
        animationView.setVisibility(VISIBLE);
        animationView.setAnimation("19451-blue-preloader.json");
        animationView.loop(true);
        animationView.playAnimation();

        //옵션전부삭제
        pm.removePrefs(getContext());
        no_item_text = root.findViewById(R.id.no_item_text);

        //초기
        getData();


        mLayout = (SlidingUpPanelLayout) root.findViewById(R.id.sliding_layout);
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        sortClickListener(root);
        viewChangeListener(root);
        filterChangeListener(root);
        sortChangeListener(root);
        keywordListener(root);





        return root;

    }




    public void getData() {

        if(stop_repeat == productList.size()) {

            stop_repeat += card_leng;
            loading_layout.setVisibility(VISIBLE);

            HashMap<String, String> hm = pm.getAllPref(getContext());
            hm.put("data_counter", String.valueOf(data_counter));
            hm.put("card_leng", String.valueOf(card_leng));
            com.sendOptions(getContext(), Constant.BASE_URL + "/app/myproductdb", hm, new Callback() {
                @Override
                public void callbackString(String str) {

                }

                @Override
                public void callbackInteger(int integer) {

                }

                @Override
                public void callbackJSONArray(JSONArray array) {

                    data_counter += card_leng;

                    //로딩제거
                    loading_layout.setVisibility(GONE);

                    if(array.length() == 0){
                        no_item_text.setVisibility(VISIBLE);
                    }

                    //뷰모드 체크
                    if(sharedPreferences != null) {
                        currentViewMode = sharedPreferences.getInt("currentViewMode", VIEW_MODE_LISTVIEW); //Default is view listview
                    }else {
                        currentViewMode = VIEW_MODE_LISTVIEW;
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt("currentViewMode", currentViewMode);
                        editor.commit();
                    }


                    //리스츠 투가하기
                    for (int i = 0; i < array.length(); i++) {
                        try {
                            String obj = (String) array.get(i);
                            productList.add(new Product(new JSONObject(obj)));
                        } catch (Exception ex) {

                        }

                    } //


                    //갯수
                    if(productList.size() != 0) {
                        try {
                            if(pm.getString(getContext(), "filter_num") != "") {
                                product_leng.setText("검색결과 " + pm.getString(getContext(), "filter_num") + "개");
                            }
                            else {
                                product_leng.setText("검색결과 " + productList.get(0).getData().get("totalCnt") + "개");
                            }
                        }catch(Exception EX){
                            product_leng.setText("");
                        }
                    }
                    if(productList.size() == 0){
                        product_leng.setText("검색결과 0개");
                    }

                    //뷰모드 세팅
                    listView.setOnItemClickListener(onItemClick);
                    gridView.setOnItemClickListener(onItemClick);

                    //어댑터연결
                    switchView();

                    listView.setSelection(productList.size()-card_leng-visibleItem_lv+1);
                    gridView.setSelection(productList.size()-card_leng-getVisibleItem_gv+1);

                    //스크롤 리스너
                    listView.setOnScrollListener(new AbsListView.OnScrollListener() {

                        private int visibleThreshold = 0;
                        private int currentPage = 0;
                        private int previousTotal = 0;
                        private boolean loading = true;

                        @Override
                        public void onScrollStateChanged(AbsListView view, int scrollState) {

                        }

                        @Override
                        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                            if (loading) {
                                if (totalItemCount > previousTotal) {
                                    loading = false;
                                    previousTotal = totalItemCount;
                                    currentPage++;
                                }
                            }
                            if (!loading && ( (firstVisibleItem+visibleItemCount) == totalItemCount)) {
                                visibleItem_lv =visibleItemCount;
                                getData();
                                loading = true;
                            }
                        }
                    });
                    gridView.setOnScrollListener(new AbsListView.OnScrollListener() {

                        private int visibleThreshold = card_leng;
                        private int currentPage = 0;
                        private int previousTotal = 0;
                        private boolean loading = true;

                        @Override
                        public void onScrollStateChanged(AbsListView view, int scrollState) {

                        }

                        @Override
                        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                            if (loading) {
                                if (totalItemCount > previousTotal) {
                                    loading = false;
                                    previousTotal = totalItemCount;
                                    currentPage++;
                                }
                            }
                            if (!loading && ( (firstVisibleItem+visibleItemCount) == totalItemCount)) {
                                getVisibleItem_gv = visibleItemCount;
                                getData();
                                loading = true;
                            }
                        }
                    });




                }
            });
        }
    }


    @Override
    public void onResume() {
        super.onResume();


        //필터메뉴에서 적용하기 눌렀을떄 여기로옴
        String adjust = pm.getString(getContext(),"myproductdb:adjust");
        if(adjust != null && adjust != ""){

            try {

                //초기화
                stop_repeat = 0;
                data_counter = 0;
                card_leng = 10;
                visibleItem_lv = 0;
                getVisibleItem_gv = 0;
                productList = new ArrayList<>();

                //데이터불러오기
                getData();

                //삭제하기
                pm.removeKey(getContext(),"myproductdb:adjust");


            }catch(Exception ex){
                ex.printStackTrace();
            }


        }
    }

    private void switchView() {

        if(VIEW_MODE_LISTVIEW == currentViewMode) {
            //Display listview
            stubList.setVisibility(VISIBLE);
            //Hide gridview
            stubGrid.setVisibility(GONE);
        } else {
            //Hide listview
            stubList.setVisibility(GONE);
            //Display gridview
            stubGrid.setVisibility(VISIBLE);
        }
        setAdapters();
    }

    private void setAdapters() {

            if(context == null){
                context = getContext();
            }

            if (VIEW_MODE_LISTVIEW == currentViewMode) {
                if(context != null) {
                    listViewAdapter = new ListViewAdapter(context, R.layout.list_item, productList);
                    listView.setAdapter(listViewAdapter);
                    listViewAdapter.notifyDataSetChanged();
                }


            } else {
                if(context != null) {
                    gridViewAdapter = new GridViewAdapter(context, R.layout.grid_item, productList);
                    gridView.setAdapter(gridViewAdapter);
                    gridViewAdapter.notifyDataSetChanged();
                }
            }



    }






    AdapterView.OnItemClickListener onItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //Do any thing when user click to item
            /*
            Intent intent = new Intent(getContext(), CardDetailActivity.class);
            String data =  productList.get(position).getData().toString();
            intent.putExtra("data", productList.get(position).getData().toString());
            startActivity(intent);

             */


            try {
                String data =  productList.get(position).getData().toString();
                pm.setString(getContext(), "large-data", data);
                Intent intent = new Intent(getContext(), CardDetailActivity.class);
                startActivity(intent);
            }catch(Exception ex2) {
                //
            }

        }
    };


    public void keywordListener(View root){
        EditText editText = root.findViewById(R.id.keyword);

        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // 입력되는 텍스트에 변화가 있을 때

                pm.setString(getContext(),"keyword", (String)editText.getText().toString());

                //초기화
                stop_repeat = 0;
                data_counter = 0;
                card_leng = 10;
                visibleItem_lv = 0;
                getVisibleItem_gv = 0;
                productList = new ArrayList<>();
                getData();
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // 입력이 끝났을 때
            }


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

        });



    }


    public void viewChangeListener(View root){
        RelativeLayout viewChange = root.findViewById(R.id.product_viewchange);
        ImageView viewChangeImage = root.findViewById(R.id.product_viewchange_image);
        //TextView viewChangeText = root.findViewById(R.id.product_viewchange_text);
        viewChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(VIEW_MODE_LISTVIEW == currentViewMode) {
                    currentViewMode = VIEW_MODE_GRIDVIEW;
                    viewChangeImage.setImageResource(R.drawable.ic_view_module_24px);
                    //viewChangeText.setText("그리드");
                } else {
                    currentViewMode = VIEW_MODE_LISTVIEW;
                    viewChangeImage.setImageResource(R.drawable.ic_view_list_24px);
                    //viewChangeText.setText("리스트");
                }
                //Switch view
                switchView();
                //Save view mode in share reference

                sharedPreferences = getContext().getSharedPreferences("ViewMode", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("currentViewMode", currentViewMode);
                editor.commit();

            }
        });
    }

    public void filterChangeListener(View root){
        RelativeLayout filter_btn = root.findViewById(R.id.product_filter);
        filter_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Filter1.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });

    }

    public void sortChangeListener(View root){
        RelativeLayout filter_btn = root.findViewById(R.id.product_sort);
        filter_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    //열려저있을떄
                    if (mLayout.getPanelState() != SlidingUpPanelLayout.PanelState.COLLAPSED) {
                        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

                    } else {
                        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                        mLayout.setAnchorPoint(1f);
                        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
                    }
            }
        });

    }
    public void sortResultsShow () {


        //getData 방식으로 바꿈
        //초기화
        stop_repeat = 0;
        data_counter = 0;
        card_leng = 10;
        visibleItem_lv = 0;
        getVisibleItem_gv = 0;
        productList = new ArrayList<>();

        getData();

        /*
        pm.confirmAndAction(getContext(), null, new Callback() {
            @Override
            public void callbackString(String str) {
                try {
                    PreferenceManager pm = new PreferenceManager();
                    String data_str = pm.getString(getContext(), "myproductdb:data");
                    JSONArray array = new JSONArray(data_str);

                    productList = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        try {
                            String obj = (String) array.get(i);
                            productList.add(new Product(new JSONObject(obj)));
                        } catch (Exception ex) {

                        }

                    }

                    SharedPreferences sharedPreferences = getContext().getSharedPreferences("ViewMode", MODE_PRIVATE);
                    currentViewMode = sharedPreferences.getInt("currentViewMode", VIEW_MODE_LISTVIEW);//Default is view listview
                    listView.setOnItemClickListener(onItemClick);
                    gridView.setOnItemClickListener(onItemClick);
                    switchView();
                }catch(Exception ex){
                    //처리
                }
            }

            @Override
            public void callbackInteger(int integer) {

            }

            @Override
            public void callbackJSONArray(JSONArray array) {

            }
        });
        */
    }

    public void sortClickListener(View root){

        //초기세팅sortClickListener
        pm.setString(getContext(), "sort","insert_time/DESC");


        TextView inserttime = root.findViewById(R.id.inserttime);
        TextView revenue_high = root.findViewById(R.id.revenue_high);
        TextView revenue_low = root.findViewById(R.id.revenue_low);
        TextView growth_high = root.findViewById(R.id.growth_high);
        TextView growth_low = root.findViewById(R.id.growth_low);
        TextView price_high = root.findViewById(R.id.price_high);
        TextView price_low = root.findViewById(R.id.price_low);

        TextView sort_text = root.findViewById(R.id.tt1);

        TextView[] tvs = {inserttime,revenue_high,revenue_low,growth_high,growth_low,price_high,price_low};


        pm.setString(getContext(), "sort","insert_time/DESC");
        inserttime.setTextColor(getResources().getColor(R.color.naver));
        sort_text.setText("최신저장순");




        inserttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(int i=0; i < tvs.length; i++){
                    tvs[i].setTextColor(getResources().getColor(R.color.text1));
                }
                pm.setString(getContext(),"sort","insert_time/DESC");
                sortResultsShow();
                inserttime.setTextColor(getResources().getColor(R.color.naver));
                sort_text.setText("최신저장순");
            }
        });
        revenue_high.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(int i=0; i < tvs.length; i++){
                    tvs[i].setTextColor(getResources().getColor(R.color.text1));
                }

                pm.setString(getContext(),"sort","revenue/DESC");
                sortResultsShow();
                revenue_high.setTextColor(getResources().getColor(R.color.naver));
                sort_text.setText("높은매출순");

            }
        });
        revenue_low.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0; i < tvs.length; i++){
                    tvs[i].setTextColor(getResources().getColor(R.color.text1));
                }

                pm.setString(getContext(),"sort","revenue/ASC");
                sortResultsShow();
                revenue_low.setTextColor(getResources().getColor(R.color.naver));
                sort_text.setText("낮은매출순");
            }
        });
        growth_high.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0; i < tvs.length; i++){
                    tvs[i].setTextColor(getResources().getColor(R.color.text1));
                }

                pm.setString(getContext(),"sort","growth/DESC");
                sortResultsShow();
                growth_high.setTextColor(getResources().getColor(R.color.naver));
                sort_text.setText("높은성장률순");
            }
        });
        growth_low.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0; i < tvs.length; i++){
                    tvs[i].setTextColor(getResources().getColor(R.color.text1));
                }

                pm.setString(getContext(),"sort","growth/ASC");
                pm.confirm(getContext(),null);
                growth_low.setTextColor(getResources().getColor(R.color.naver));
                sort_text.setText("낮은성장률순");
            }
        });
        price_high.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0; i < tvs.length; i++){
                    tvs[i].setTextColor(getResources().getColor(R.color.text1));
                }

                pm.setString(getContext(),"sort","price/DESC");
                sortResultsShow();
                price_high.setTextColor(getResources().getColor(R.color.naver));
                sort_text.setText("높은가격순");
            }
        });
        price_low.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0; i < tvs.length; i++){
                    tvs[i].setTextColor(getResources().getColor(R.color.text1));
                }

                pm.setString(getContext(),"sort","price/ASC");
                sortResultsShow();
                price_low.setTextColor(getResources().getColor(R.color.naver));
                sort_text.setText("낮은가격순");
            }
        });


    }



}

package com.spang.ui.productdb;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.spang.R;

import org.json.JSONObject;

import java.util.List;

public class GridViewAdapter extends ArrayAdapter<Product> {
    String TAG = "hell";
    public GridViewAdapter(Context context, int resource, List<Product> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(null == v) {
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.grid_item, null);
        }
        Product product = getItem(position);
        ImageView img = (ImageView) v.findViewById(R.id.image);
        TextView title = (TextView) v.findViewById(R.id.title);
        TextView price =(TextView) v.findViewById(R.id.price);
        TextView category  = (TextView) v.findViewById(R.id.category);

        TextView revenue = (TextView) v.findViewById(R.id.revenueTotal);
        //TextView growth = (TextView) v.findViewById(R.id.growth);
        TextView season = (TextView) v.findViewById(R.id.season);

        JSONObject obj = product.getData();

        try {

            if(position < 50) {
                Glide.with(getContext()).load(obj.get("image_url")).thumbnail(/*sizeMultiplier=*/ 0.5f).diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.HIGH).into(img);
            }
            else {
                Glide.with(getContext()).load(obj.get("image_url")).thumbnail(/*sizeMultiplier=*/ 0.5f).diskCacheStrategy(DiskCacheStrategy.ALL).priority(Priority.LOW).into(img);
            }
            title.setText((String)obj.get("타이틀"));
            price.setText((String)obj.get("price_short"));
            category.setText((String)obj.get("category"));
            revenue.setText((String)obj.get("누적매출액_쇼트"));
            //growth.setText((String)obj.get("월평균성장률"));
            season.setText((String)obj.get("계절"));
        }catch(Exception ex){

        }
        return v;
    }
}

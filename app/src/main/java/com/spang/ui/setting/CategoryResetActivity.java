package com.spang.ui.setting;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.spang.ApplicationActivity;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.spang.ApplicationActivity;
import com.spang.Communication.PreferenceManager;
import com.spang.R;
import com.spang.constants.CategoryConstants;
import com.spang.models.User;
import com.spang.ui.intro.IntroActivity;
import com.spang.utils.HttpClient;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.spang.constants.SharedPreferencesConstant.SELECTED_CATEGORY;

public class CategoryResetActivity extends ApplicationActivity {
    String TAG = "CATEGORY_RESET_FRAGMENT";
    String selectedCategoryName = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_reset);


        ImageView backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button confirmButton = (Button)findViewById(R.id.confirmButton);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selectedCategoryName != null){
                    updateCategory();
                }
            }
        });
        Button cancelButton = (Button)findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        createCategoryRadioButton(radioGroup);
    }

    private void createCategoryRadioButton(RadioGroup radioGroup){
        RadioButton defaultCheckedRadioButton = null;
        String current_category = User.getCategoryFromSharedPreference(getApplicationContext());

        for(int i = 0; i< CategoryConstants.CATEGORY_NAMES.length; i++){
            final String categoryName = CategoryConstants.CATEGORY_NAMES[i];
            RadioButton radioButton = new RadioButton(this);
            ViewGroup.LayoutParams param = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            radioButton.setLayoutParams(param);
            radioButton.setText(categoryName);
            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedCategoryName = categoryName;


                    int count = radioGroup.getChildCount();
                    ArrayList<RadioButton> listOfRadioButtons = new ArrayList<RadioButton>();
                    for (int i=0;i<count;i++) {
                        View o = radioGroup.getChildAt(i);
                        if (o instanceof RadioButton) {
                            ((RadioButton)o).setTextColor(getResources().getColor(R.color.text2));
                        }
                    }


                    radioButton.setTextColor(getResources().getColor(R.color.naver));
                }
            });

            if(current_category.equals(categoryName)) {
                defaultCheckedRadioButton = radioButton;
            }

            radioGroup.addView(radioButton);
        }

        if(defaultCheckedRadioButton != null){
            defaultCheckedRadioButton.setChecked(true);
            defaultCheckedRadioButton.setTextColor(getResources().getColor(R.color.naver));
        }

    }

    private void updateCategory(){
        HashMap<String, String> data = new HashMap<>();
        data.put("category", selectedCategoryName);

        HttpClient.put(this, "/users/me", data, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                PreferenceManager.setString(CategoryResetActivity.this, SELECTED_CATEGORY, selectedCategoryName);

                Toast.makeText(CategoryResetActivity.this, "관심 카테고리가 변경되었습니다.", Toast.LENGTH_LONG).show();
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
            }
        });
    }
}

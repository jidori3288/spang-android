package com.spang.ui.setting;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.spang.ApplicationFragment;
import com.spang.R;
import com.spang.SplashActivity;
import com.spang.constants.Constant;
import com.spang.models.PaymentTransaction;
import com.spang.models.User;
import com.spang.models.UserPoint;
import com.spang.ui.analysis.WebsiteViewActivity;
import com.spang.ui.payment.PaymentActivity;
import com.spang.ui.payment.PaymentListActivity;
import com.spang.utils.HttpClient;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import org.json.JSONObject;

import java.text.DecimalFormat;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class SettingFragment extends ApplicationFragment {
    String TAG = "hell";
    private TextView pointText;

    @Override
    public void onResume() {
        super.onResume();

        //fetchUserPoint();
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_setting, container, false);

        RelativeLayout btnSignOut = root.findViewById(R.id.btnSignOut);
        pointText = root.findViewById(R.id.point_text);
        //TextView payment_history = root.findViewById(R.id.payment_history);
        //TextView payment_letsgo = root.findViewById(R.id.payment_letsgo);
        RelativeLayout btnYakwan = root.findViewById(R.id.btnYakwan);
        RelativeLayout btnInfo = root.findViewById(R.id.btnInfo);

        //로그아웃
        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User.removeFromSharedPreference(root.getContext());

                Intent intent= new Intent(root.getContext(), SplashActivity.class);
                startActivity(intent);
            }
        });

        /*
        //결제하기
        payment_letsgo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(root.getContext(), PaymentActivity.class);
                startActivity(intent);
            }
        });
        //결제목록
        payment_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(root.getContext(), PaymentListActivity.class);
                startActivity(intent);
            }
        });
        */

        //약관
        btnYakwan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    Intent intent = new Intent(getContext(), WebsiteViewActivity.class);
                    intent.putExtra("url",  Constant.BASE_URL +"/app/yakwan");
                    startActivity(intent.addFlags(FLAG_ACTIVITY_NEW_TASK));

            }
        });

        //앱 정보
        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), WebsiteViewActivity.class);
                intent.putExtra("url", Constant.BASE_URL +"/app/info");
                startActivity(intent.addFlags(FLAG_ACTIVITY_NEW_TASK));
            }
        });


        /*
        //카테고리재설정
        RelativeLayout btnCategoryReset = root.findViewById(R.id.btnCategoryReset);
        btnCategoryReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(root.getContext(), CategoryResetActivity.class);
                startActivity(intent);
            }
        });

         */

        return root;
    }



    private void fetchUserPoint() {
        HttpClient.get(getContext(), "/user_points/me", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                UserPoint userPoint = gson.fromJson(response.toString(), UserPoint.class);
                String point = toNumFormat(userPoint.getPoint()) + " P";

                pointText.setText(point);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
            }
        });
    }

    public static String toNumFormat(int num) {
        DecimalFormat df = new DecimalFormat("#,###");
        return df.format(num);
    }
}

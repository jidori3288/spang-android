package com.spang.ui.analysis;

import android.animation.Animator;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.text.Spannable;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.spang.ApplicationActivity;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.Communication.PreferenceManager;
import com.spang.R;
import com.spang.constants.Constant;
import com.spang.ui.cardbuy.CardBuyActivity;
import com.spang.ui.productdb.ProductdbActivity;
import com.spang.ui.today.CardActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.view.View.GONE;


public class CardDetailActivity extends ApplicationActivity implements ImageAdapter.OnListItemSelectedInterface {

    String TAG = "CardDetailActivity";

    private RecyclerView thumbnailView;
    List<ImageModel> thumbnailList;
    private ImageAdapter thumbnailAdapter;

    private RecyclerView relateView;
    List<ImageModel> relateList;
    private GridAdapter relateAdapter;

    public Animator currentAnimator;
    public int shortAnimationDuration;

    private Communication com = new Communication();

    public PreferenceManager pm = new PreferenceManager();

    private String product_no = "";
    String is_save = "0";
    String is_store = "0";

    private String url = "";
    String thumbnail_str = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        String data_str;


        String large_data = pm.getString(getApplicationContext(), "large-data");
        if(large_data != null && !large_data.equals("")){
            data_str = large_data;
            pm.setString(getApplicationContext(), "large-data", null);
        }
        else {
            data_str = getIntent().getStringExtra("data");
        }
        try {
            JSONObject data_obj = new JSONObject(data_str);

            product_no = (String) data_obj.get("상품번호");

            url = (String) data_obj.get("url");

            //썸네일
            //thumbnail_str = (String)data_obj.get("썸네일");

            //해시태그
            TextView item_hashtag = findViewById(R.id.item_hashtag);
            String hashtag_str = (String)data_obj.get("hashtag");
            String[] hashtag_arr = hashtag_str.split(",");
            String hashtag_pick = "";
            for(int t=0; t <hashtag_arr.length; t++){
                    hashtag_pick += hashtag_arr[t];
                    hashtag_pick += " ";
            }
            item_hashtag.setText(hashtag_pick);

            //메인 이미지 셋팅
            ImageView item_photo = findViewById(R.id.item_photo);
            Glide.with(getApplicationContext()).load(data_obj.get("image_url")).diskCacheStrategy(DiskCacheStrategy.ALL).into(item_photo);

            //타이틀
            TextView item_title = findViewById(R.id.item_title);
            item_title.setText((String)data_obj.get("타이틀"));

            //가격
            TextView item_price = findViewById(R.id.item_price);
            item_price.setText((String)data_obj.get("price") + "원");

            //카테고리
            TextView item_category = findViewById(R.id.item_category);
            item_category.setText((String)data_obj.get("category"));

            //누적매출
            TextView item_revenueTotal = findViewById(R.id.item_revenueTotal);
            item_revenueTotal.setText(((String) data_obj.get("누적매출액_쇼트")) );

            //최근6개월매출
            TextView item_revenue6m = findViewById(R.id.item_revenue6m);
            item_revenue6m.setText(((String) data_obj.get("누적매출액6개월_쇼트")) );

            //최근3개월매출
            TextView item_revenue3m = findViewById(R.id.item_revenue3m);
            item_revenue3m.setText(((String) data_obj.get("누적매출액3개월_쇼트")));

            //봄여름비율
            TextView item_revenueSS = findViewById(R.id.item_revenueSS);
            item_revenueSS.setText((String) data_obj.get("봄여름"));

            //가을겨울비율
            TextView item_revenueFW = findViewById(R.id.item_revenueFW);
            item_revenueFW.setText((String) data_obj.get("가을겨울"));

            //분야매출순위
            //TextView item_catrank = findViewById(R.id.item_catrank);
            //item_catrank.setText((String) data_obj.get("분야매출순위"));

            //성장률
            TextView item_growth = findViewById(R.id.item_growth);
            item_growth.setText((String) data_obj.get("월평균성장률"));

            //제조사
            try {
                TextView item_company = findViewById(R.id.item_company);
                item_company.setText((String) data_obj.get("제조사"));
            }catch(Exception noComp){

            }

            //브랜드
            try {
                TextView item_brand = findViewById(R.id.item_brand);
                item_brand.setText((String) data_obj.get("브랜드"));
            }catch(Exception noData){

            }

            //원산지
            try {
                TextView item_makecountry = findViewById(R.id.item_makecountry);
                item_makecountry.setText((String) data_obj.get("원산지"));
            }catch(Exception ex){

            }

            //키워드
            try {
                TextView item_keyword = findViewById(R.id.item_keyword);
                String keyword = (String) data_obj.get("firstpage_keyword");
                if(!keyword.equals("-")) {
                    item_keyword.setText((String) data_obj.get("firstpage_keyword"));
                } else {
                    item_keyword.setVisibility(GONE);
                    findViewById(R.id.title_keyword).setVisibility(GONE);
                }
            }catch(Exception ex){

            }
            //백버튼
            try {
                RelativeLayout backBtn = findViewById(R.id.back_container);
                backBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //pm.setString(getApplicationContext(),"delete:card", product_no);
                        finish();


                    }
                });
            }catch(Exception ex){

            }

            ImageView revnue_question = (ImageView) findViewById(R.id.revenue_q);
            revnue_question.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });



            //저장,스토어,웹사이트, 삭제 버튼
            Button storeBtn = findViewById(R.id.storeBtn);
            Button saveBtn = findViewById(R.id.saveBtn);
            Button gotoBtn = findViewById(R.id.gotoBtn);
            //ImageView deleteBtn = findViewById(R.id.deleteBtn);

            gotoBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), WebsiteViewActivity.class);
                    intent.putExtra("url", url);
                    startActivity(intent.addFlags(FLAG_ACTIVITY_NEW_TASK));


                }
            });

            //삭제
            /*
            deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> hm = new HashMap();
                    hm.put("product_no", product_no);
                    com.sendOptions(getApplicationContext(), Constant.BASE_URL + "/app/delete", hm, new Callback() {
                        @Override
                        public void callbackString(String str) {

                        }

                        @Override
                        public void callbackInteger(int integer) {

                              showDelete(integer);

                        }

                        @Override
                        public void callbackJSONArray(JSONArray array) {

                        }

                });
                }
            });

             */


            //저장버튼
            try {
                is_save = String.valueOf(data_obj.get("is_save"));
                Log.v("is_save", is_save);
                if(is_save.equals("1")){
                    saveBtn.setBackgroundColor(getResources().getColor(R.color.text1));
                    saveBtn.setText("아이템DB 저장됨");
                }
            }catch(Exception ex){
                //저장이 안되어있음
                is_save = "0";
            }


            //스토어버튼
            try {
                is_store = String.valueOf(data_obj.get("is_store"));
                if(is_store.equals("1")){
                    storeBtn.setBackgroundColor(getResources().getColor(R.color.text1));
                    storeBtn.setText("모의판매중");
                }
            }catch(Exception ex){
                //저장이 안되어있음
                is_store = "0";
            }


            ImageView revenue_q = (ImageView) findViewById(R.id.revenue_q);
            revenue_q.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showInfo();
                }
            });


            storeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try{
                    is_store = String.valueOf(data_obj.get("is_store"));

                    if(is_store.equals("1")) {

                            HashMap<String, String> hm = new HashMap();
                            hm.put("product_no", (String) data_obj.get("상품번호"));
                            storeBtn.setText("등록 해제중...");
                            com.sendOptions(getApplicationContext(), Constant.BASE_URL + "/app/store-remove", hm, new Callback() {
                                @Override
                                public void callbackString(String str) {

                                }

                                @Override
                                public void callbackInteger(int integer) {

                                }

                                @Override
                                public void callbackJSONArray(JSONArray array) {

                                    try {
                                        data_obj.put("is_store", "0");
                                        storeBtn.setBackgroundColor(getResources().getColor(R.color.naver));
                                        storeBtn.setText("모의판매 등록");

                                    } catch (Exception ex) {

                                    }


                                }
                            });


                    }

                    if(is_store.equals("0")) {

                            HashMap<String, String> hm = new HashMap();
                            hm.put("product_no", (String) data_obj.get("상품번호"));
                            storeBtn.setText("등록중...");
                            com.sendOptions(getApplicationContext(), Constant.BASE_URL + "/app/store", hm, new Callback() {
                                @Override
                                public void callbackString(String str) {

                                }

                                @Override
                                public void callbackInteger(int integer) {

                                }

                                @Override
                                public void callbackJSONArray(JSONArray array) {

                                    try {
                                        data_obj.put("is_save", "1");
                                        data_obj.put("is_store", "1");
                                        storeBtn.setText("모의판매중");
                                        storeBtn.setBackgroundColor(getResources().getColor(R.color.text1));

                                        saveBtn.setText("아이템DB 저장됨");
                                        saveBtn.setBackgroundColor(getResources().getColor(R.color.text1));
                                        Toast myToast = Toast.makeText(getApplicationContext(), "모의판매 등록 성공", Toast.LENGTH_SHORT);
                                        myToast.show();
                                    } catch (Exception ex) {

                                    }


                                }
                            });


                    }


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                }
            });




                    saveBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            try{
                                is_save = String.valueOf(data_obj.get("is_save"));
                            if(is_save.equals("1")) {
                                saveBtn.setText("저장 해제중...");
                                    HashMap<String, String> hm = new HashMap();
                                    hm.put("product_no", (String) data_obj.get("상품번호"));
                                    com.sendOptions(getApplicationContext(), Constant.BASE_URL + "/app/save-remove", hm, new Callback() {
                                        @Override
                                        public void callbackString(String str) {

                                        }

                                        @Override
                                        public void callbackInteger(int integer) {

                                            try {
                                                data_obj.put("is_save", "0");
                                                saveBtn.setBackgroundColor(getResources().getColor(R.color.naver));
                                                saveBtn.setText("아이템DB 저장");
                                            } catch (Exception ex) {

                                            }
                                        }

                                        @Override
                                        public void callbackJSONArray(JSONArray array) {



                                        }
                                    });


                            }

                            if (is_save.equals("0")) {

                                    HashMap<String, String> hm = new HashMap();
                                    hm.put("product_no", (String) data_obj.get("상품번호"));
                                    saveBtn.setText("저장중 ...");
                                    com.sendOptions(getApplicationContext(), Constant.BASE_URL + "/app/save", hm, new Callback() {
                                        @Override
                                        public void callbackString(String str) {

                                        }

                                        @Override
                                        public void callbackInteger(int integer) {
                                            try {
                                                data_obj.put("is_save", "1");
                                                saveBtn.setText("아이템DB 저장됨");
                                                saveBtn.setBackgroundColor(getResources().getColor(R.color.text1));
                                                Toast myToast = Toast.makeText(getApplicationContext(), "저장완료", Toast.LENGTH_SHORT);
                                                myToast.show();
                                            } catch (Exception ex) {

                                            }
                                        }

                                        @Override
                                        public void callbackJSONArray(JSONArray array) {




                                        }
                                    });

                            }



                        }catch(Exception ex){
                            ex.printStackTrace();
                        }
                        }
                    });





            //썸네일들 보기
            //setUpThumbnails();


            JSONArray revenues= (JSONArray) data_obj.get("월별매출그래프");
            float spring = (Integer) data_obj.get("봄");
            float summer = (Integer) data_obj.get("여름");
            float fall = (Integer) data_obj.get("가을");
            float winter = (Integer) data_obj.get("겨울");
            float[] seasons = {winter, fall,summer,spring};
            String best_season = "사계절";
            if(spring+ summer >= 60) {
                best_season = "봄/여름";
            }
            else if(fall+winter >= 60) {
                best_season  ="가을/겨울";
            }



            if(revenues.length() > 0) {
                new ChartRevenue((CardView) findViewById(R.id.chartRevenueView), this, revenues).init();
            } else {
                findViewById(R.id.chartRevenueView).setVisibility(GONE);
            }
            if(best_season.length() > 0) {
                new ChartSeason((CardView) findViewById(R.id.chartSeasonView), this, seasons, best_season).init();
            } else {
                findViewById(R.id.chartSeasonView).setVisibility(GONE);
            }



            JSONObject buy_opt = (JSONObject)data_obj.get("구매옵션");

            if(buy_opt.length() > 0) {
                new ChartInventory((CardView) findViewById(R.id.chartInventoryView), this, buy_opt).init();
                TextView inventory_detail = (TextView) findViewById(R.id.chartInventoryView_detail);

                String buy_opt_str = "";


                //옵션 텍스트 만들기
                Iterator<String> keys = buy_opt.keys();
                class Inventory { String product_name; int product_ratio; Inventory(String product_name, int product_ratio){this.product_name= product_name; this.product_ratio = product_ratio;} };
                Inventory ivt;
                List<Inventory>list = new ArrayList<>();
                while(keys.hasNext()) {
                    String key = keys.next();
                    String value_str = (String) buy_opt.get(key).toString();
                    float value = Float.valueOf(value_str);
                    int real_value = Math.round(value * 100);
                    ivt = new Inventory(key, real_value);
                    list.add(ivt);
                }

                //소팅
                Collections.sort(list, new Comparator<Inventory>() {
                    @Override
                    public int compare(Inventory s1, Inventory s2) {
                        return Integer.compare(s2.product_ratio, s1.product_ratio);
                    }
                });

                //텍스트 만들기
                for(int v =0 ; v < list.size(); v++){
                    buy_opt_str += 	(list.get(v).product_name + " (" + list.get(v).product_ratio + "%)");
                    buy_opt_str += "\n";
                }

                //텍스트 적용
                inventory_detail.setText(buy_opt_str);


            } else{
                findViewById(R.id.chartInventoryView_text).setVisibility(GONE);
                findViewById(R.id.chartInventoryView).setVisibility(GONE);
                findViewById(R.id.chartInventoryView_detail_text).setVisibility(GONE);
            }


            //연관상품 보기
            setUpRelateItems(data_obj);


        }catch(Exception ex){
            ex.printStackTrace();
        }




    }

    @Override

    public void onEnterAnimationComplete() {

        super.onEnterAnimationComplete();

    }



    @Override
    public void onItemSelected(View v, int position, String src) {
        ImageAdapter.ViewHolder viewHolder = (ImageAdapter.ViewHolder)thumbnailView.findViewHolderForAdapterPosition(position);



    }



    //상품 썸네일 스냅헬퍼 뷰
    private void setUpThumbnails() {






        //썸네일들 보기

        /*
        thumbnailView =findViewById(R.id.itemdetail_images_holder);
        LinearLayoutManager layoutRecommend = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        thumbnailView.setLayoutManager(layoutRecommend);
        thumbnailList = getThumbnails(thumbnail_str);
        thumbnailAdapter = new ImageAdapter(this,this,thumbnailList);
        thumbnailView.setAdapter(thumbnailAdapter);
        SnapHelper snapHelper1 = new LinearSnapHelper();
        snapHelper1.attachToRecyclerView(thumbnailView);
        thumbnailAdapter.updateList(thumbnailList);
        */




    }


    //연관상품 뷰
    private void setUpRelateItems(JSONObject data_obj) {

        relateView =findViewById(R.id.item_relate);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        relateView.setLayoutManager(layoutManager);
        relateAdapter = new GridAdapter(this);
        relateView.setAdapter(relateAdapter);
        relateList = getRelateItems(data_obj);
        relateAdapter.updateList(relateList);

    }

    public void showInfo(){

        //다이얼로그 객체 생성
        AlertDialog.Builder builder = new AlertDialog.Builder(CardDetailActivity.this);
        //속성 지정
        builder.setTitle("월별 누적 매출 추이");
        builder.setMessage("월별 매출을 추정 하여 합산한 값의 그래프입니다\n추정치임으로 참고 자료로만 활용 하시기 바랍니다");
        //아이콘
        builder.setIcon(android.R.drawable.ic_menu_help);


        //예 버튼 눌렀을 때
        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //텍스트 뷰 객체를 넣어줌..
                // Snackbar.make(textView ,"확인되었습니다",Snackbar.LENGTH_SHORT).show();
            }
        });

        //만들어주기
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView msg = (TextView)dialog.findViewById(android.R.id.message);
        msg.setTextSize(12);
    }

    public void showDelete(int isSuccess){

        //다이얼로그 객체 생성
        AlertDialog.Builder builder = new AlertDialog.Builder(CardDetailActivity.this);
        String msg_del = "";
        if(isSuccess == 1){
            msg_del = "삭제 성공\n리프레시 해주세요";
        }
        if(isSuccess == 0){
            msg_del ="이미 삭제되었습니다\n리프레시 해주세요";
        }

        //속성 지정
        builder.setTitle("알림");
        builder.setMessage(msg_del);


        //예 버튼 눌렀을 때
        builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();

                //텍스트 뷰 객체를 넣어줌..
                // Snackbar.make(textView ,"확인되었습니다",Snackbar.LENGTH_SHORT).show();
            }
        });

        //만들어주기
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView msg = (TextView)dialog.findViewById(android.R.id.message);
        msg.setTextSize(15);
    }



    public List<ImageModel> getThumbnails(String thumbnails) {

        List<ImageModel> thumbnail_list = new ArrayList<>();

        try {
            String str = thumbnails.split("\\[")[1].split("\\]")[0];
            String[] data_obj = str.split(",");
            for(int i = 0; i < data_obj.length; i++){
                ImageModel im = new ImageModel(null, data_obj[i] );
                thumbnail_list.add(im);
            }
        }catch(Exception ex){
            //
            //findViewById(R.id.itemdetail_images_holder_text).setVisibility(GONE);
            //findViewById(R.id.itemdetail_images_holder).setVisibility(GONE);
        }

        return thumbnail_list;
    }



    public List<ImageModel> getRelateItems(JSONObject data_obj) {

        List<ImageModel> enemy_list = new ArrayList<>();

        try {
            int limit =0;
            JSONArray enemy_list_data =(JSONArray)data_obj.get("경쟁제품상세분석");

            for(int i =0; i < enemy_list_data.length(); i++){
                try {
                    String product_url = (String) ((JSONObject) enemy_list_data.get(i)).get("product_url");
                    String image_url = (String) ((JSONObject) enemy_list_data.get(i)).get("image_url");
                    if(image_url != null && product_url != null) {
                        if (limit < 12) {
                            enemy_list.add(new ImageModel(product_url, image_url));
                            limit++;
                        }
                    }
                }catch(Exception ex){
                    //데이터 null;
                }
            }
            if(enemy_list_data.length() == 0 || limit == 0){
                findViewById(R.id.relate_text).setVisibility(GONE);
            }


        }catch(Exception ex){
            ex.printStackTrace();
        }
        return enemy_list;

    }







}

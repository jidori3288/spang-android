package com.spang.ui.analysis;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.spang.R;

import java.util.ArrayList;
import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.ViewHolder> {
    String TAG = "hell";
    private List<ImageModel> cardImgesDeck;
    private Context context;
    private int cellSize;

    public GridAdapter(Context context) {
        this.context = context;
        cardImgesDeck = new ArrayList<>();
        this.cellSize = Util.getScreenWidth(context) / 3;


    }

    public void updateList(List<ImageModel> cardImageList) {
        this.cardImgesDeck = cardImageList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view2, parent, false);
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
        layoutParams.height = cellSize;
        layoutParams.width = cellSize;
        layoutParams.setFullSpan(false);
        view.setLayoutParams(layoutParams);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return cardImgesDeck.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imagePlace;
        TextView textViewName;

        public ViewHolder(View itemView) {
            super(itemView);
            imagePlace = itemView.findViewById(R.id.itemdetail_images);
        }

        public void onBind(final int position) {
            final ImageModel imageModel = cardImgesDeck.get(position);
            Glide.with(context).load(imageModel.getImageSrc()).override(cellSize,cellSize).thumbnail(0.5f).diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop().into(imagePlace);
            //Picasso.get().load(imageModel.getImageSrc()).into(imagePlace);
            imagePlace.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, WebsiteViewActivity.class);
                    intent.putExtra("url", imageModel.getProduct_no());
                    context.startActivity(intent.addFlags(FLAG_ACTIVITY_NEW_TASK));
                }
            });

        }
    }

}

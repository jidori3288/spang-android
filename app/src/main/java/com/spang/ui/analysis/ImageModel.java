package com.spang.ui.analysis;

public class ImageModel {
    String TAG = "hell";
    private String product_no;

    private String image_src;

    public ImageModel(String product_no,  String image_src) {
        this.product_no = product_no;
        this.image_src = image_src;
    }

    public String getProduct_no() {
        return product_no;
    }

    public void setProduct_no(String product_no) {
        this.product_no = product_no;
    }


    public String getImageSrc() {
        return image_src;
    }

    public void setImageSrc(String image_src) {
        this.image_src = image_src;
    }


}

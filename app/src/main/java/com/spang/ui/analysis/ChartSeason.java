package com.spang.ui.analysis;

import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.view.View;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.db.chart.Tools;
import com.db.chart.animation.Animation;
import com.db.chart.listener.OnEntryClickListener;
import com.db.chart.model.Bar;
import com.db.chart.model.BarSet;
import com.db.chart.renderer.XRenderer;
import com.db.chart.tooltip.Tooltip;
import com.db.chart.view.HorizontalBarChartView;
import com.spang.R;


public class ChartSeason extends CardController {

    String TAG = "hell";

    private final Context mContext;


    private final TextView mTextViewValue;


    private final HorizontalBarChartView mChart;

    private final String[] mLabels = {"겨울","가을","여름","봄"};

    public float[] mValues;


    public ChartSeason(CardView card, Context context, float[] seasons, String best_sea) {

        super(card);

        mContext = context;
        mChart = (HorizontalBarChartView) card.findViewById(R.id.chart_season);
        TextView best_season = (TextView) card.findViewById(R.id.chart_seson_legend_value);
        best_season.setText(best_sea);



        mValues = seasons;

        mTextViewValue = (TextView) card.findViewById(R.id.season_value);

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "OpenSans-Semibold.ttf");
        mTextViewValue.setTypeface(typeface);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
            mTextViewValue.animate().alpha(0).setDuration(100);
        }
        else { mTextViewValue.setVisibility(View.INVISIBLE);}
    }


    @Override
    public void show(Runnable action) {

        super.show(action);

        Tooltip tip = new Tooltip(mContext);
        tip.setBackgroundColor(Color.parseColor("#ffffff"));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            tip.setEnterAnimation(PropertyValuesHolder.ofFloat(View.ALPHA, 1)).setDuration(150);
            tip.setExitAnimation(PropertyValuesHolder.ofFloat(View.ALPHA, 0)).setDuration(150);
        }

        mChart.setTooltips(tip);

        mChart.setOnEntryClickListener(new OnEntryClickListener() {
            @Override
            public void onClick(int setIndex, int entryIndex, Rect rect) {

                mTextViewValue.setText(Integer.toString((int) mValues[entryIndex]) + "%");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
                    mTextViewValue.animate().alpha(1).setDuration(200);
                else mTextViewValue.setVisibility(View.VISIBLE);
            }
        });

        mChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
                    mTextViewValue.animate().alpha(0).setDuration(100);
                else mTextViewValue.setVisibility(View.INVISIBLE);
            }
        });


        BarSet barSet = new BarSet();
        Bar bar;
        for (int i = 0; i < mLabels.length; i++) {
            float val =mValues[i];
            if(val < 1) {
                val = 8;
            }
            bar = new Bar(mLabels[i], val);
            switch (i) {
                case 3:
                    bar.setColor(Color.parseColor("#f75e5e"));
                    break;
                case 2:
                    bar.setColor(Color.parseColor("#f5365c"));
                    break;
                case 1:
                    bar.setColor(Color.parseColor("#41d7f2"));
                    break;
                case 0:
                    bar.setColor(Color.parseColor("#4184f2"));
                    break;
                default:
                    break;
            }
            barSet.addBar(bar);
        }

        mChart.addData(barSet);
        mChart.setBarSpacing(Tools.fromDpToPx(4));

        mChart.setBorderSpacing(0)
                .setXAxis(false)
                .setYAxis(false)
                .setLabelsColor(Color.parseColor("#ffffff"))
                .setXLabels(XRenderer.LabelPosition.NONE);

        int[] order = {3, 2, 1,0};
        final Runnable auxAction = action;
        Runnable chartOneAction = new Runnable() {
            @Override
            public void run() {

                auxAction.run();
                showTooltip();
            }
        };
        mChart.show(new Animation().setOverlap(.7f, order).setEndAction(chartOneAction));
    }


    private void showTooltip() {



        for(int p=0; p < mValues.length; p++) {
            // Tooltip
            if(mValues[p] >= 10) {
                Tooltip tip = new Tooltip(mContext, R.layout.linechart_three_tooltip2, R.id.value);
                //tip.setBackgroundColor(Color.parseColor("#ffffff"));
                tip.setHorizontalAlignment(Tooltip.Alignment.RIGHT_RIGHT);
                tip.setDimensions((int) Tools.fromDpToPx(25), (int) Tools.fromDpToPx(15));
                tip.setMargins(0, 0, 5, (int) Tools.fromDpToPx(25));
                tip.prepare(mChart.getEntriesArea(0).get(p), mValues[p]);
                mChart.showTooltip(tip, true);
            }
            if(mValues[p] < 10) {
                Tooltip tip = new Tooltip(mContext, R.layout.linechart_three_tooltip2, R.id.value);
                //tip.setBackgroundColor(Color.parseColor("#ffffff"));
                tip.setHorizontalAlignment(Tooltip.Alignment.RIGHT_RIGHT);
                tip.setDimensions((int) Tools.fromDpToPx(25), (int) Tools.fromDpToPx(15));
                tip.setMargins(0, 0, -70, (int) Tools.fromDpToPx(25));
                tip.prepare(mChart.getEntriesArea(0).get(p), mValues[p]);
                mChart.showTooltip(tip, true);
            }

        }



    }

    @Override
    public void update() {

        super.update();

        mChart.dismissAllTooltips();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
            mTextViewValue.animate().alpha(0).setDuration(100);
        else mTextViewValue.setVisibility(View.INVISIBLE);

        if (firstStage) mChart.updateValues(0, mValues);
        else mChart.updateValues(0, mValues);
        mChart.notifyDataUpdate();
    }


    @Override
    public void dismiss(Runnable action) {

        super.dismiss(action);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
            mTextViewValue.animate().alpha(0).setDuration(100);
        else mTextViewValue.setVisibility(View.INVISIBLE);

        mChart.dismissAllTooltips();

        int[] order = {0, 1, 2, 3};
        mChart.dismiss(new Animation().setOverlap(.5f, order).setEndAction(action));
    }

}

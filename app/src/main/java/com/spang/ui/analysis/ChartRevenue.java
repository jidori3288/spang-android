package com.spang.ui.analysis;

import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.db.chart.Tools;
import com.db.chart.animation.Animation;
import com.db.chart.model.LineSet;
import com.db.chart.renderer.AxisRenderer;
import com.db.chart.tooltip.Tooltip;
import com.db.chart.view.LineChartView;
import com.spang.R;

import org.json.JSONArray;

import java.util.Calendar;


public class ChartRevenue extends CardController {


    String TAG = "ChartRevenue";

    private final LineChartView mChart;


    private final Context mContext;


    private final String[] mLabels = {"1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"};

    //앞에꺼 1년, 뒤에꺼 최근 3개월?
    public float[] mValues = new float[12];

    private Tooltip mTip;

    private Runnable mBaseAction;

    int current_month = Calendar.getInstance().get(Calendar.MONTH);


    public ChartRevenue(CardView card, Context context, JSONArray revenues) {

        super(card);

        mContext = context;
        mChart = (LineChartView) card.findViewById(R.id.chart_revenue);
        //TextView maxRevenue = card.findViewById(R.id.chart_revenue_legend_value);


        //그래프 그리기
        try {
            for (int i = 0; i < revenues.length(); i++) {
                String data = (revenues.get(i)).toString();
                mValues[i] = Math.round(Float.valueOf(data) / 10000);
            }

           // maxRevenue.setText(getMax(mValues) + "만");

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }


    @Override
    public void show(Runnable action) {

        super.show(action);

        // Tooltip
        mTip = new Tooltip(mContext, R.layout.linechart_three_tooltip, R.id.value);

        ((TextView) mTip.findViewById(R.id.value)).setTypeface(
                Typeface.createFromAsset(mContext.getAssets(), "OpenSans-Semibold.ttf"));


        //툴팁 자체 사이즈에 관한것 (건들지 말것)
        mTip.setVerticalAlignment(Tooltip.Alignment.BOTTOM_TOP);
        mTip.setDimensions((int) Tools.fromDpToPx(70), (int) Tools.fromDpToPx(25));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {

            mTip.setEnterAnimation(PropertyValuesHolder.ofFloat(View.ALPHA, 1),
                    PropertyValuesHolder.ofFloat(View.SCALE_Y, 1f),
                    PropertyValuesHolder.ofFloat(View.SCALE_X, 1f)).setDuration(200);

            mTip.setExitAnimation(PropertyValuesHolder.ofFloat(View.ALPHA, 0),
                    PropertyValuesHolder.ofFloat(View.SCALE_Y, 0f),
                    PropertyValuesHolder.ofFloat(View.SCALE_X, 0f)).setDuration(200);

            mTip.setPivotX(Tools.fromDpToPx(65) / 2);
            mTip.setPivotY(Tools.fromDpToPx(25));
        }

        mChart.setTooltips(mTip);

        // 회색 점선
        LineSet dataset = new LineSet(mLabels, mValues);
        dataset.setColor(Color.parseColor("#758cbb"))
                .setFill(Color.parseColor("#2d374c"))
                .setDotsColor(Color.parseColor("#758cbb"))
                .setThickness(4)
                .setDashed(new float[] {10f, 10f})
                .beginAt(current_month);
        mChart.addData(dataset);


        //노란색 실선
        dataset = new LineSet(mLabels, mValues);
        dataset.setColor(Color.parseColor("#b3b5bb"))
                .setFill(Color.parseColor("#2d374c"))
                .setDotsColor(Color.parseColor("#ffc755"))
                .setThickness(4)
                .endAt(current_month+1);
        mChart.addData(dataset);


        // Chart
        mChart.setBorderSpacing(Tools.fromDpToPx(15))
                .setAxisBorderValues(0, getMax(mValues) + 20)
                .setYLabels(AxisRenderer.LabelPosition.NONE)
                .setLabelsColor(Color.parseColor("#6a84c3"))
                .setXAxis(false)
                .setYAxis(false);



        mBaseAction = action;
        Runnable chartAction = new Runnable() {
            @Override
            public void run() {


                mBaseAction.run();
                mTip.prepare(mChart.getEntriesArea(0).get(current_month), mValues[current_month]);
                mChart.showTooltip(mTip, true);
            }
        };

        Animation anim = new Animation().setEasing(new BounceInterpolator()).setEndAction(chartAction);

        mChart.show(anim);
    }


    @Override
    public void update() {

        super.update();

        mChart.dismissAllTooltips();
        if (firstStage) {
            mChart.updateValues(0, mValues);
            mChart.updateValues(1, mValues);
        } else {
            mChart.updateValues(0, mValues);
            mChart.updateValues(1, mValues);
        }
        mChart.getChartAnimation().setEndAction(mBaseAction);
        mChart.notifyDataUpdate();
    }


    @Override
    public void dismiss(Runnable action) {

        super.dismiss(action);

        mChart.dismissAllTooltips();
        mChart.dismiss(new Animation().setEasing(new BounceInterpolator()).setEndAction(action));
    }

    // Method for getting the maximum value
    public static int getMax(float[] inputArray){
        float maxValue = inputArray[0];
        for(int i=1;i < inputArray.length;i++){
            if(inputArray[i] > maxValue){
                maxValue = inputArray[i];
            }
        }
        return Math.round(maxValue);
    }

    // Method for getting the minimum value
    public static int getMin(float[] inputArray){
        float minValue = inputArray[0];
        for(int i=1;i<inputArray.length;i++){
            if(inputArray[i] < minValue){
                minValue = inputArray[i];
            }
        }
        return Math.round(minValue);
    }


}

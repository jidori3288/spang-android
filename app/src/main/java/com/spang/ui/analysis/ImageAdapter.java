package com.spang.ui.analysis;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.spang.R;


import java.util.ArrayList;
import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
    String TAG = "hell";
    private List<ImageModel> cardImgesDeck;
    private Context context;



    public interface OnListItemSelectedInterface {
        void onItemSelected(View v, int position, String src);
    }
    private OnListItemSelectedInterface mListener;


    public ImageAdapter(Context context, OnListItemSelectedInterface listener,List<ImageModel> cardImageList){
        this.context = context;
        this.mListener = listener;
        this.cardImgesDeck = cardImageList;
    }

    public void updateList(List<ImageModel> cardImageList) {
        this.cardImgesDeck = cardImageList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.onBind(cardImgesDeck.get(position));
    }

    @Override
    public int getItemCount() {
        return cardImgesDeck.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imagePlace;
        TextView textViewName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //
            imagePlace = itemView.findViewById(R.id.itemdetail_images);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   //mListener.onItemSelected(v, getAdapterPosition(),  cardImgesDeck.get(getAdapterPosition()).getImageSrc());
                    Intent intent = new Intent(context, ImageZoomActivity.class);

                    String src_string = "";
                    for(ImageModel model : cardImgesDeck){
                        String src = model.getImageSrc();
                        src += ",";
                    }
                    intent.putExtra("image_array", src_string);
                    intent.putExtra("image_num",getAdapterPosition());
                    context.startActivity(intent);


                }
            });


        }

        public void onBind(ImageModel imageModel) {
            String url = imageModel.getImageSrc().split("type=")[0];
            url = url + "type=f200";
            Glide.with(context).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).thumbnail(/*sizeMultiplier=*/ 0.1f).into(imagePlace);


        }
    }




}

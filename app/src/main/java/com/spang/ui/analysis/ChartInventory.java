package com.spang.ui.analysis;

import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.db.chart.Tools;
import com.db.chart.animation.Animation;
import com.db.chart.listener.OnEntryClickListener;
import com.db.chart.model.Bar;
import com.db.chart.model.BarSet;
import com.db.chart.renderer.XRenderer;
import com.db.chart.tooltip.Tooltip;
import com.db.chart.view.HorizontalBarChartView;
import com.spang.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class ChartInventory extends CardController {

    String TAG = "ChartInventory";

    private final Context mContext;

    private final HorizontalBarChartView mChart;

    private final TextView mTextViewValue;

    public  String[] mLabels = {};

    public String[] mLabelsReal = {};

    public float[] mValues = {};

    public int[] order;


    public ArrayList<Integer> sorted_list = new ArrayList<>();

    Runnable mBaseAction;

    // Method for getting the maximum value
    public static int getMax(float[] inputArray){
        float maxValue = inputArray[0];
        for(int i=1;i < inputArray.length;i++){
            if(inputArray[i] > maxValue){
                maxValue = inputArray[i];
            }
        }
        return Math.round(maxValue);
    }



    public ChartInventory(CardView card, Context context, JSONObject buy_opt) {

        super(card);

        mContext = context;
        mChart = (HorizontalBarChartView) card.findViewById(R.id.chart_inventory);
        mTextViewValue = (TextView) card.findViewById(R.id.inventory_value);

        ViewGroup.LayoutParams layoutParams = card.getLayoutParams();

        if(buy_opt.length() == 1){
            layoutParams.height = 300;
        }



        String max_option = null;
        float max_value =0;


        Iterator<String> keys = buy_opt.keys();
        while(keys.hasNext())
        {
            try {
                String key = keys.next();
                String value_str = (String) buy_opt.get(key).toString();
                float value = Float.valueOf(value_str);
                if(value > 0){
                    if (value >= max_value) {
                        max_value = value;
                        max_option = key;
                    }
                    sorted_list.add(Math.round(value * 100));

                }
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }

        //소팅
        Collections.sort(sorted_list,new AscendingInteger());



        //5개+ 기타로 정리
        if(sorted_list.size() > 5) {

            order = new int[6];
            order[0]=5;
            order[1]=4;
            order[2]=3;
            order[3]=2;
            order[4]=1;
            order[5]=0;

            mLabels = new String[6];
            mValues = new float[6];
            int sum = 0;
            int etc = 0;
            for(int k=0; k < 5; k++){
                sum += sorted_list.get(k);
            }
            etc = 100-sum;
            mLabels[5] = "기타";
            mValues[5] = etc;

            int cnt = 0;
            for(int s =4; s >=0; s--){
                mLabels[cnt] = "옵션명" + (s+1);
                mValues[cnt] = sorted_list.get(s);
                cnt++;
            }
        } else {
            order = new int[sorted_list.size()];
            for(int t=0; t < sorted_list.size(); t++){
                order[t] = (sorted_list.size()) -t-1;
            }

            mLabels = new String[sorted_list.size()];
            mValues = new float[sorted_list.size()];

            int cnt = 0;
            for(int s =sorted_list.size()-1; s >=0; s--){
                mLabels[cnt] = "옵션명" + (s+1);
                mValues[cnt] = sorted_list.get(s);
                cnt++;
            }
        }





        TextView best_inven = card.findViewById(R.id.chart_inventory_legend_value);
        best_inven.setText(max_option);





        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "OpenSans-Semibold.ttf");
        mTextViewValue.setTypeface(typeface);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
            mTextViewValue.animate().alpha(0).setDuration(100);
        }
        else { mTextViewValue.setVisibility(View.INVISIBLE);}

    }

    private void showTooltip() {



        for(int p=0; p < mValues.length; p++) {
            // Tooltip
            if(mValues[p] >= 10) {
                Tooltip tip = new Tooltip(mContext, R.layout.linechart_three_tooltip2, R.id.value);
                //tip.setBackgroundColor(Color.parseColor("#ffffff"));
                tip.setHorizontalAlignment(Tooltip.Alignment.RIGHT_RIGHT);
                tip.setDimensions((int) Tools.fromDpToPx(25), (int) Tools.fromDpToPx(15));
                tip.setMargins(0, 0, 5, (int) Tools.fromDpToPx(25));
                tip.prepare(mChart.getEntriesArea(0).get(p), mValues[p]);
                mChart.showTooltip(tip, true);
            }
            if(mValues[p] < 10) {
                Tooltip tip = new Tooltip(mContext, R.layout.linechart_three_tooltip2, R.id.value);
                //tip.setBackgroundColor(Color.parseColor("#ffffff"));
                tip.setHorizontalAlignment(Tooltip.Alignment.RIGHT_RIGHT);
                tip.setDimensions((int) Tools.fromDpToPx(25), (int) Tools.fromDpToPx(15));
                tip.setMargins(0, 0, -70, (int) Tools.fromDpToPx(25));
                tip.prepare(mChart.getEntriesArea(0).get(p), mValues[p]);
                mChart.showTooltip(tip, true);
            }

        }


    }

    @Override
    public void show(Runnable action) {

        super.show(action);

        Tooltip tip = new Tooltip(mContext);
        tip.setBackgroundColor(Color.parseColor("#f39c12"));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            tip.setEnterAnimation(PropertyValuesHolder.ofFloat(View.ALPHA, 1)).setDuration(150);
            tip.setExitAnimation(PropertyValuesHolder.ofFloat(View.ALPHA, 0)).setDuration(150);
        }

        mChart.setTooltips(tip);

        mChart.setOnEntryClickListener(new OnEntryClickListener() {
            @Override
            public void onClick(int setIndex, int entryIndex, Rect rect) {

                mTextViewValue.setText(Integer.toString((int) mValues[entryIndex]) + "%");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
                    mTextViewValue.animate().alpha(1).setDuration(200);
                else mTextViewValue.setVisibility(View.VISIBLE);
            }
        });

        mChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
                    mTextViewValue.animate().alpha(0).setDuration(100);
                else mTextViewValue.setVisibility(View.INVISIBLE);
            }
        });


        BarSet barSet = new BarSet();
        Bar bar;
        for (int i = 0; i < mLabels.length; i++) {
            bar = new Bar(mLabels[i], mValues[i]);
            bar.setColor(Color.parseColor("#758cbb"));
            barSet.addBar(bar);
        }

        mChart.addData(barSet);
        mChart.setBarSpacing(Tools.fromDpToPx(4));

        mChart.setBorderSpacing(0)
                .setXAxis(false)
                .setYAxis(false)
                .setLabelsColor(Color.parseColor("#ffffff"))
                .setXLabels(XRenderer.LabelPosition.NONE);


        //mChart.show(new Animation().setOverlap(.5f, order).setEndAction(action));
        final Runnable auxAction = action;
        Runnable chartOneAction = new Runnable() {
            @Override
            public void run() {

                auxAction.run();
                showTooltip();
            }
        };
        mChart.show(new Animation().setOverlap(.7f, order).setEndAction(chartOneAction));
    }


    @Override
    public void update() {

        super.update();

        mChart.dismissAllTooltips();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
            mTextViewValue.animate().alpha(0).setDuration(100);
        else mTextViewValue.setVisibility(View.INVISIBLE);

        if (firstStage) mChart.updateValues(0, mValues);
        else mChart.updateValues(0, mValues);
        mChart.notifyDataUpdate();
    }

    @Override
    public void dismiss(Runnable action) {

        super.dismiss(action);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1)
            mTextViewValue.animate().alpha(0).setDuration(100);
        else mTextViewValue.setVisibility(View.INVISIBLE);

        mChart.dismissAllTooltips();

        mChart.dismiss(new Animation().setOverlap(.5f, order).setEndAction(action));
    }

    class AscendingInteger implements Comparator<Integer> { @Override public int compare(Integer a, Integer b) { return b.compareTo(a); } }



}

package com.spang.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.spang.constants.SharedPreferencesConstant;

import java.util.Date;

public class User {
    private Integer id;
    private String name;
    @SerializedName("profile_url")
    private String profileUrl;
    @SerializedName("access_token")
    private String accessToken;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public static User fromSharedPreference(Context context){
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences(SharedPreferencesConstant.NAME, 0); // 0 - for private mode
        String json = pref.getString(SharedPreferencesConstant.JSON_USER, null);

        if(json != null){
            Gson gson = new Gson();
            return gson.fromJson(json, User.class);
        }else{
            return null;
        }
    }

    public static void removeFromSharedPreference(Context context){
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences(SharedPreferencesConstant.NAME, 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(SharedPreferencesConstant.JSON_USER);
        editor.commit();
    }

    public void toSharedPreference(Context context) {
        Gson gson = new Gson();
        String json = gson.toJson(this);

        SharedPreferences pref = context.getApplicationContext().getSharedPreferences(SharedPreferencesConstant.NAME, 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(SharedPreferencesConstant.JSON_USER, json);
        editor.commit();
    }

    public static String getCategoryFromSharedPreference(Context context){
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences(SharedPreferencesConstant.NAME, 0); // 0 - for private mode
        return pref.getString(SharedPreferencesConstant.SELECTED_CATEGORY, null);
    }

    public static void setCategoryToSharedPreference(Context context, String category){
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences(SharedPreferencesConstant.NAME, 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(SharedPreferencesConstant.SELECTED_CATEGORY, category);
        editor.commit();
    }
}

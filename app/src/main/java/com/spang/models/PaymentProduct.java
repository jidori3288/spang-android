package com.spang.models;

import com.google.gson.annotations.SerializedName;

public class PaymentProduct {
    private Integer id;
    @SerializedName("product_id")
    private String productId;
    private String name;
    private Integer price;
    private Integer point;


    private String img;
    private String name_desc;
    private String name_desc2;
    private String price_btn;

    public Integer getId() {
        return id;
    }

    public String getProductId() {
        return productId;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getPoint() {
        return point;
    }

    public String getImg() {return img;}

    public String getNameDesc() { return name_desc;}

    public String getNameDesc2() { return name_desc2;}

    public String getPriceBtn(){return price_btn;}
}

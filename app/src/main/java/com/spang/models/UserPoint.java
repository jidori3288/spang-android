package com.spang.models;

import com.google.gson.annotations.SerializedName;

public class UserPoint {
    private Integer id;
    private Integer point;

    public Integer getId() {
        return id;
    }

    public Integer getPoint() {
        return point;
    }
}

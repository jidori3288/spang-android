package com.spang.models;

import com.anjlab.android.iab.v3.PurchaseState;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class PaymentTransaction {
    private Integer id;
    @SerializedName("purchase_token")
    private String purchaseToken;
    @SerializedName("purchase_time")
    public Date purchaseTime;
    @SerializedName("developer_payload")
    public String developerPayload;
    @SerializedName("payment_product")
    public PaymentProduct paymentProduct;

    public Integer getId() {
        return id;
    }

    public String getPurchaseToken() {
        return purchaseToken;
    }

    public Date getPurchaseTime() {
        return purchaseTime;
    }

    public String getDeveloperPayload() {
        return developerPayload;
    }

    public PaymentProduct getPaymentProduct() {
        return paymentProduct;
    }
}

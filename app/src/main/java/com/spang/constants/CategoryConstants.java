package com.spang.constants;

import android.widget.RadioButton;
import android.widget.RadioGroup;

public class CategoryConstants {
    public static final String[] CATEGORY_NAMES = {
            "전체",
            "패션의류",
            "패션잡화",
            "화장품/미용",
            "디지털/가전",
            "가구/인테리어",
            "출산/육아",
            "식품",
            "스포츠/레저",
            "생활/건강",
            "여가/생활편의"
    };
}

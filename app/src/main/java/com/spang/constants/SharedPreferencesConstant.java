package com.spang.constants;

public class SharedPreferencesConstant {
    public static final String NAME = "PREF_SPANG";
    public static final String JSON_USER = "JSON_USER";
    public static final String SELECTED_CATEGORY = "SELECTED_CATEGORY";
    public static final String RANDOM_ITEM_INFORMATION = "RANDOM_ITEM_INFORMATION";
    public static final String JOKER_ITEM_INFORMATION = "JOKER_ITEM_INFORMATION";
    public static final String REMAIN_POINT = "REMAIN_POINT";
}

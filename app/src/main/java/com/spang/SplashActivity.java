package com.spang;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.multidex.BuildConfig;

import com.google.android.play.core.appupdate.AppUpdateManager;
import com.spang.Communication.Callback;
import com.spang.Communication.Communication;
import com.spang.constants.Constant;
import com.spang.models.User;
import com.spang.ui.intro.IntroActivity;

import org.json.JSONArray;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import static android.view.View.VISIBLE;
import static com.kakao.util.helper.Utility.getPackageInfo;

public class SplashActivity extends AppCompatActivity {

    private final int MY_REQUEST_CODE = 100;
    private AppUpdateManager mAppUpdateManager;
    Communication com = new Communication();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String TAG = "SPLASH";
        setContentView(R.layout.activity_splash);
        getKeyHash(this);
        User user = User.fromSharedPreference(this);


        String db_url = Constant.BASE_URL + "/app/version";
        HashMap<String, String> hm = new HashMap<>();

        com.sendOptions(getApplicationContext(), db_url, hm, new Callback() {
            @Override
            public void callbackString(String str) {
                try{

                    Log.v("version", str);

                    //앱버전
                    String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                    Log.v("versionName", versionName);
                    versionName = versionName.replaceAll("\\.", "");
                    int versionName_int = Integer.parseInt(versionName);

                    //현재버전
                    String current_version = str.replaceAll("\\.", "");
                    int current_version_int = Integer.parseInt(current_version);

                    if(versionName_int < current_version_int) {

                        TextView splash_text = findViewById(R.id.splash_text);
                        Button just_pass =findViewById(R.id.just_pass);
                        splash_text.setVisibility(VISIBLE);
                        just_pass.setVisibility(VISIBLE);

                        Intent itn = new Intent(Intent.ACTION_VIEW);
                        itn.setData(Uri.parse("market://details?id=com.spang"));
                        startActivity(itn);


                        just_pass.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (user != null) {
                                    alreadySignIn();
                                } else {
                                    notSignedIn();
                                }
                            }
                        });
                    }

                    else {

                        if (user != null) {
                            alreadySignIn();
                        } else {
                            notSignedIn();
                        }

                    }




                }catch(PackageManager.NameNotFoundException nex) {

                    //에러일 경우 그냥 패스
                    if (user != null) {
                        alreadySignIn();
                    } else {
                        notSignedIn();
                    }
                }
            }

            @Override
            public void callbackInteger(int integer) {

            }

            @Override
            public void callbackJSONArray(JSONArray array) {

            }
        });



    }

    private void alreadySignIn() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void notSignedIn() {
        Intent intent = new Intent(this, IntroActivity.class);
//        Intent intent= new Intent(this, PaymentActivity.class);
        startActivity(intent);
        finish();
    }

    public static String getKeyHash(final Context context) {
        PackageInfo packageInfo = getPackageInfo(context, PackageManager.GET_SIGNATURES);
        if (packageInfo == null)
            return null;

        for (Signature signature : packageInfo.signatures) {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.v("해시키", Base64.encodeToString(md.digest(), Base64.NO_WRAP));
            } catch (NoSuchAlgorithmException e) {
                Log.e("d", "Unable to get MessageDigest. signature=" + signature, e);
            }
        }
        return null;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                Log.d("AppUpdate", "Update flow failed! Result code: " + resultCode); // 로그로 코드 확인
                //showCustomSnackbar(findViewById(R.id.activity_splash), "코로나 맵을 사용하기 위해서는 업데이트가 필요해요");  //snackbar로 사용자에게 알려주기
                finishAffinity(); // 앱 종료
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


    }
}


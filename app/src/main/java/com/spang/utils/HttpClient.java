package com.spang.utils;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.spang.constants.Constant;
import com.spang.models.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HttpClient  {
    private static RequestQueue requestQueue;

    public static RequestQueue getRequestQueue(Context context) {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return requestQueue;
    }

    public static void get(Context context, String url, Response.Listener<JSONObject> successCallback, Response.ErrorListener errorCallback) {
        JsonObjectRequest request = new JsonObjectRequest(
            Request.Method.GET, fullUrl(url), null, successCallback, errorCallback
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = customAuthorizationHeader(context);

                if(headers != null){
                    return headers;
                }
                return super.getHeaders();
            }
        };

        getRequestQueue(context).add(request);
    }

    public static void getWithArrayResponse(Context context, String url, Response.Listener<JSONArray> successCallback, Response.ErrorListener errorCallback) {
        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET, fullUrl(url), null, successCallback, errorCallback
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = customAuthorizationHeader(context);

                if(headers != null){
                    return headers;
                }
                return super.getHeaders();
            }
        };

        getRequestQueue(context).add(request);
    }

    public static void post(Context context, String url, Map<String, String> data, Response.Listener<JSONObject> successCallback, Response.ErrorListener errorCallback) {
        JSONObject params = new JSONObject(data);

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST, fullUrl(url), params, successCallback, errorCallback
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = customAuthorizationHeader(context);

                if(headers != null){
                    return headers;
                }
                return super.getHeaders();
            }
        };

        getRequestQueue(context).add(request);
    }

    public static void put(Context context, String url, Map<String, String> data, Response.Listener<JSONObject> successCallback, Response.ErrorListener errorCallback) {
        JSONObject params = new JSONObject(data);

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.PUT, fullUrl(url), params, successCallback, errorCallback
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = customAuthorizationHeader(context);

                if(headers != null){
                    return headers;
                }
                return super.getHeaders();
            }
        };

        getRequestQueue(context).add(request);
    }

    private static String fullUrl(String url){
        return Constant.BASE_URL + url;
    }

    private static Map<String, String> customAuthorizationHeader(Context context) {
        User user = User.fromSharedPreference(context);

        if(user != null){
            Map<String, String> headers = new HashMap<String, String>();
            headers.put("Custom-Authorization", user.getAccessToken());

            return headers;
        }
        return null;
    }

}
